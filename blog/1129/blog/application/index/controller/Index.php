<?php
namespace app\index\controller;
use app\common\model\Article; 
use app\common\model\Category; 
use app\common\model\Tag; 
use app\common\model\Webset; 

class Index
{
    public function index()
    {

        $articles = Article::get_data();
		//栏目
		$categories = Category::get_data();
        //标签云
        $tags = Tag::get_data();



    	
        return view('',compact('articles','categories','tags'));
    }
}
