<?php

namespace app\admin\controller;

use think\Request;

class Index extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        
        return view();
    }

    public function welcome(){
        echo 'welcome';
    }


    public function flush_cache(){
        cache('article', NULL);
        $str = <<<str
<script>
alert('更新成功');
setTimeout(function(){
    parent.location.href='/admin/index/index';
},1000)

</script>
str;
        echo $str;
    }
   
}
