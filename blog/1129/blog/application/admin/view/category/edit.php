<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">
	    <!-- Loading Bootstrap -->
	    <link href="/static/admin/Flat/dist/css/vendor/bootstrap.min.css" rel="stylesheet">
	    <!-- Loading Flat UI -->
	    <link href="/static/admin/Flat/dist/css/flat-ui.css" rel="stylesheet">
	    <link href="/static/admin/Flat/docs/assets/css/demo.css" rel="stylesheet">
	    <link rel="shortcut icon" href="/static/admin/Flat/img/favicon.ico">
	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	    <!--[if lt IE 9]>
	      <script src="dist/js/vendor/html5shiv.js"></script>
	      <script src="dist/js/vendor/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<form method="post" action="/admin/category/{$data.cid}">
			 <input type="hidden" name="_method" value="PUT" >
			<div class="alert alert-success">编辑分类</div>
			<div class="form-group">
		
	
			<div class="form-group">
				<label for="exampleInputEmail1">分类名字</label>
				<input id="exampleInputEmail1" class="form-control" type="text" placeholder="请输入分类标题" name="cname" value="{$data.cname}">
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">分类关键字</label>
				<textarea name="keywords" rows="5" cols=""  class="form-control" placeholder="请输入分类关键字">{$data.keywords}</textarea>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">分类描述</label>
				<textarea name="description" rows="5" cols=""  class="form-control" placeholder="请输入分类描述">{$data.description}</textarea>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">分类排序</label>
				<input id="exampleInputEmail1" class="form-control" type="text" placeholder="请输入分类排序" required="" name="sort" value="{$data.sort}">
			</div>
			
		
			<button class="btn btn-primary btn-block" type="submit"> 确定 </button>
		</form>
	</body>
</html>
