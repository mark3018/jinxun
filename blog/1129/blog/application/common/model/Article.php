<?php

namespace app\common\model;

use think\Model;
use think\Cache;

class Article extends Model
{
    //
    public function category()
    {
        return $this->belongsTo('category','cid','cid');
    }


    public static function get_data(){
    	//没有缓存读取数据库的数据，有缓存直接读取缓存
    	$articles = Cache::remember('article',function(){
			$articles = self::where('is_recycle',0)->order('pubtime desc')->limit(10)->select();
	        foreach ($articles as $article) {
	            //通过中间表查询文章对应的标签id
	        	$tids = ArcTag::where('aid',$article->aid)->column('tid');
	        	$strTids = implode(',', $tids);
	     		//通过tid得到对应的标签
	            $article->tags = Tag::all($strTids);
	        }

	        return $articles;
		});
        return $articles;

    	
    }
}
