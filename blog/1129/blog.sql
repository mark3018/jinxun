/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-11-29 20:24:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `arc_tag`
-- ----------------------------
DROP TABLE IF EXISTS `arc_tag`;
CREATE TABLE `arc_tag` (
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `tid` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of arc_tag
-- ----------------------------
INSERT INTO `arc_tag` VALUES ('1', '2');
INSERT INTO `arc_tag` VALUES ('1', '1');
INSERT INTO `arc_tag` VALUES ('19', '3');
INSERT INTO `arc_tag` VALUES ('19', '1');

-- ----------------------------
-- Table structure for `article`
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(120) NOT NULL DEFAULT '',
  `content` text,
  `author` varchar(10) NOT NULL DEFAULT '',
  `pubtime` int(11) NOT NULL DEFAULT '0',
  `click` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) DEFAULT '' COMMENT '缩略图地址',
  `digest` varchar(500) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_recycle` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '1章标题', '章正文', '作者', '1', '0', '1', '', '123摘要', '关键字123', '章描述', '1');
INSERT INTO `article` VALUES ('18', '2张‭波999', '<p>这里写你的初始化1111内容</p>', '', '2', '0', '4', 'uploads/20181122/cde72a2448734e91d07673bcac57e8f8.jpg', null, null, null, '0');
INSERT INTO `article` VALUES ('19', '3张‭波888', '<p>这里写你的初始化内容</p>', '', '3', '0', '4', 'uploads/20181122/d291a83fd9ada32984d67adbc04c0399.jpg', null, null, null, '0');
INSERT INTO `article` VALUES ('20', '4章标题', '<p>这里写你的初始化内容</p>', '', '4', '0', '1', '', null, null, null, '0');
INSERT INTO `article` VALUES ('21', '6666', '<p>这里写你的初始化内容</p>', '', '6', '0', '1', '', null, null, null, '0');

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `cid` smallint(6) NOT NULL AUTO_INCREMENT,
  `cname` char(10) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '娱乐3333', '娱乐', '娱乐', '100');
INSERT INTO `category` VALUES ('4', '体育', '123描述', '123关键字', '200');

-- ----------------------------
-- Table structure for `links`
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `lid` smallint(6) NOT NULL AUTO_INCREMENT,
  `lname` varchar(15) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '100',
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of links
-- ----------------------------

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('20181103130948', 'Users', '2018-11-03 21:14:08', '2018-11-03 21:14:08', '0');

-- ----------------------------
-- Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `tname` char(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES ('1', 'PHP');
INSERT INTO `tag` VALUES ('2', 'JS');
INSERT INTO `tag` VALUES ('3', 'HTML');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL DEFAULT '' COMMENT '用户名，登陆使用',
  `password` varchar(32) NOT NULL DEFAULT 'e10adc3949ba59abbe56e057f20f883e' COMMENT '用户密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e');

-- ----------------------------
-- Table structure for `webset`
-- ----------------------------
DROP TABLE IF EXISTS `webset`;
CREATE TABLE `webset` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of webset
-- ----------------------------
INSERT INTO `webset` VALUES ('1', '{\"webname\":\"\\u9a6c\\u5148\\u751f\\u7684\\u535a\\u5ba2\",\"copyright\":\"@copyright2019\",\"codelen\":\"3\",\"email\":\"ma@163.com\"}');
