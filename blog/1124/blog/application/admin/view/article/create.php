<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">
	    <!-- Loading Bootstrap -->
	    <link href="/static/admin/Flat/dist/css/vendor/bootstrap.min.css" rel="stylesheet">
	    <!-- Loading Flat UI -->
	    <link href="/static/admin/Flat/dist/css/flat-ui.css" rel="stylesheet">
	    <link href="/static/admin/Flat/docs/assets/css/demo.css" rel="stylesheet">
	    <link rel="shortcut icon" href="/static/admin/Flat/img/favicon.ico">
	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	    <!--[if lt IE 9]>
	      <script src="dist/js/vendor/html5shiv.js"></script>
	      <script src="dist/js/vendor/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<form method="post" action="/admin/article" enctype="multipart/form-data">
		<div class="alert alert-success">添加文章</div>
		<div class="form-group">
			<label for="exampleInputEmail1">文章标题</label>
			<input id="exampleInputEmail1" class="form-control" type="text" placeholder="" required="" name="title">
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">作者</label>
			<input id="exampleInputEmail1" class="form-control" type="text" placeholder="" required="" name="author">
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">所属分类</label>
			<select name="cid" class="form-control" required>
				<option value="">请选择</option>
				<?php foreach($categories as $category): ?>
					<option value="{$category.cid}">{$category.cname}</option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">缩略图</label>
			<input id="exampleInputEmail1" type="file" name="thumb">
		</div>
		
		
		<div id="">
			<label for="">文章标签</label>
			<br />
			<?php foreach($tags as $tag): ?>
				<label class="checkbox checkbox-inline" for="checkbox{$tag.tid}">
					<input id="checkbox{$tag.tid}" name="tid[]" class="custom-checkbox" type="checkbox" data-toggle="checkbox" value="{$tag.tid}" >
					<span class="icons">
					<span class="icon-unchecked"></span>
					<span class="icon-checked"></span>
					</span>
					{$tag.tname}
				</label>
			<?php endforeach ?>

		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">文章摘要</label>
			<textarea name="digest" rows="5" cols=""  class="form-control" placeholder="请输入文章关键字"></textarea>
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">文章关键字</label>
			<textarea name="keywords" rows="5" cols=""  class="form-control" placeholder="请输入文章关键字"></textarea>
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">文章描述</label>
			<textarea name="description" rows="5" cols=""  class="form-control" placeholder="请输入文章描述"></textarea>
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">文章正文</label>
			<script id="container" name="content" type="text/plain" style="height: 500px;">这里写你的初始化内容</script>
			<!-- 配置文件 -->
		    <script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
		    <!-- 编辑器源码文件 -->
		    <script type="text/javascript" src="/static/ueditor/ueditor.all.js"></script>
		    <!-- 实例化编辑器 -->
		    <script type="text/javascript">
		        var ue = UE.getEditor('container');
		    </script>
		</div>
		<button class="btn btn-primary btn-block" type="submit"> 确定 </button>
		</form>
	</body>
</html>
