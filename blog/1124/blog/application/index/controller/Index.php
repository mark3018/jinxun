<?php
namespace app\index\controller;
use app\common\model\Article; 
use think\Cache;
class Index
{
    public function index()
    {
    	//没有缓存读取数据库的数据，有缓存直接读取缓存
    	$data = Cache::remember('article',function(){
			return Article::where('is_recycle',0)->select();
		});
        return view('',compact('data'));
    }
}
