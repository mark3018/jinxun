<?php
think_auth_rule(规则表)
id   name(规则唯一标识)  
 1    category-index      
 2    category-save    
 3    tag-index


think_auth_group（用户组表）
id    title（用户组中文名称）   rules（规则id，用逗号隔开）
 1    分类组                   1,2
 2    标签组                   3            


think_auth_group_access（用户组明细表）
uid（用户id） group_id（用户组id）
 1            1
 2            2



users(用户表)
uid   username
1 	  category
2     tag



category用户(uid是1)
Category控制器
index方法
save方法



tag用户(uid是2)
Tag控制器
index方法



//从下往上捋

// 先知道用户是属于什么组，再看组里有什么权限