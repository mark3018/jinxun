<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use app\common\model\Article; 
use app\common\model\Category; 
use app\common\model\Tag; 
use app\common\model\Webset; 
class Common extends Controller
{
 	public function _initialize(){
 		//栏目
		$categories = Category::get_data();
        //标签云
        $tags = Tag::get_data();
 		$this->assign([
 			'categories'=>$categories,
 			'tags'=>$tags
 		]);
 	} 
}