<?php
namespace app\admin\controller;

use think\Controller;
use think\Session;
use think\auth\Auth;

class Common extends Controller
{

	public function _initialize(){
		//如果没有登录
		if(!Session::get('user')){
			//去登录
			return $this->redirect('login/login');
		}
		//如果不是超级管理员，需要检测权限
		if(session('user.username') != 'admin'){
			//获得控制器的名字
			$controller = request()->controller();
	        //获得方法的名字
	        $action = request()->action();
	        $auth = new Auth();
	        //不需要检测的控制器和方法
	        $notCheck = ['Index-index','Index-welcome'];
	        //控制器和方法组合的字符串
	        $controllerAction = $controller . '-' . $action;
	       	// p($controllerAction);
	        if(!in_array($controllerAction, $notCheck)){
	        	 //检测是否有权限
		        if(!$auth->check($controllerAction, session('user.id'))){
		            $this->error('你没有权限访问');
		        }
	        }
		}
		
       

	

	}

}

 ?>