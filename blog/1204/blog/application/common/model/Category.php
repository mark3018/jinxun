<?php
namespace app\common\model;
use think\Model;

class Category extends Model
{
    //
    public static function get_data(){
    	$categories = self::all();
        foreach ($categories as $category) {
            //每个栏目对应的文章总数
            $category->articleNums = Article::where('cid',$category->cid)->where('is_recycle',0)->count();
        }
        return $categories;
    }
    
}
