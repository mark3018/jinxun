<?php 
//1.定需求
// 博客

//2.建表
栏目表
cname  栏目名称
description  栏目描述SEO
keywords  关键字SEO
sort  排序

create table category(
	cid smallint primary key auto_increment,
	cname char(10) not null default '',
	description varchar(255) not null default '',
	keywords varchar(255) not null default '',
	sort smallint unsigned not null default 100
);


文章表
title 标题
content 内容
author 作者
pubtime 发布时间
click 阅读数
cid  所属栏目

create table article(
	aid int primary key auto_increment,
	title char(120) not null default '',
	content text,
	author varchar(10) not null default '',
	pubtime int not null default 0,
	click smallint unsigned not null default 0,
	cid smallint unsigned not null default 0
);


文章标签中间表
aid  文章id
tid  标签id
create table arc_tag(
	aid int unsigned not null default 0,
	tid int unsigned not null default 0
);


标签表
tname 名称
create table tag(
	tid int primary key auto_increment,
	tname char(15) not null default ''
);


用户表
username 用户名
password 密码
create table user(
	uid smallint primary key auto_increment,
	username char(15) not null default '',
	password char(32) not null default ''
);


友情链接表
lname 名称
link  链接地址
thumb 缩略图
sort 排序

create table links(
	lid smallint primary key auto_increment,
	lname varchar(15) not null default '',
	link varchar(255) not null default '',
	thumb varchar(255) not null default '',
	sort smallint unsigned not null default 100
);



//3.下载框架写代码



//4.边写一边测试

//5.内测

//6.上线完工

//7.后续的维护增加功能


 ?>