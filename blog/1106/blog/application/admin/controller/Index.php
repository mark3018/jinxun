<?php

namespace app\admin\controller;

use think\Request;

class Index extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        
        return view();
    }

    public function welcome(){
        echo 'welcome';
    }
}
