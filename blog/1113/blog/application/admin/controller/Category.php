<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use app\common\model\Category as CategoryModel; 

class Category extends Common
{
   
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        

        $data = CategoryModel::all();
        // foreach ($data as $v) {
        //     dump($v->toArray());
        // }
        return view('',compact('data'));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
        return view();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {

        $model = new CategoryModel;
        $result = $model->validate(true)->save($request->post());
        if(false === $result){
            // 验证失败 输出错误信息
            $this->error($model->getError());
        }
        $this->success('添加成功','/admin/category');

    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
        $data = CategoryModel::find($id);
        return view('',compact('data'));
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
        echo $id;
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //先判断栏目下面是否有文章：作业


        //如果栏目下面无文章直接删除
        CategoryModel::destroy($id);
        return ['status'=>true,'message'=>'删除栏目成功'];

    }
}
