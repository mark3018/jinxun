<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

//自定义p函数
function p($var){
	echo '<pre>' . print_r($var,true) . '</pre>';
}


//webset('webname')
//webset('codelen');
function webset($name){
	$content = app\common\model\Webset::getWebSet();
	return isset($content[$name]) ? $content[$name] : '';
}
