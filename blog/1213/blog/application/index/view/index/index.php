{extend name="base" /}
{block name="left"}
<main class="col-md-8">
						<article class="_carousel">
							<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
							    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
							  </ol>
							
							  <!-- Wrapper for slides -->
							  <div class="carousel-inner" role="listbox">
							    <div class="item active">
							      <img src="/static/home/images/1.jpg">
							    </div>
							    <div class="item">
							       <img src="/static/home/images/2.jpg">
							    </div>
							  </div>
							
							  <!-- Controls -->
							  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							  </a>
							  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							  </a>
							</div>
						</article>
						<?php foreach($articles as $article): ?>
						<article>
							<div class="_head">
								<h1>{$article.title}</h1>
								<span>
								作者：
								{$article.author}
								</span>
								•
								<!--pubdate表⽰示发布⽇日期-->
								<time pubdate="pubdate" datetime="<?php echo date('Y-m-d',$article->pubtime) ?>"><?php echo date('Y-m-d',$article->pubtime) ?></time>
								•
								分类：
								<a href="list.html"><?php echo $article->category->cname ?></a>
							</div>
							<div class="_digest">
							<img src="{$article.thumb}"  class="img-responsive"/>
								<p>
									{$article.digest}
								</p>
							</div>
							<div class="_more">
								<a href="{:url('Content/index',['aid'=>$article['aid']])}" class="btn btn-default">阅读全文</a>
							</div>
							<div class="_footer">
								<i class="glyphicon glyphicon-tags"></i>
								<?php foreach($article->tags as $tag): ?>
								<a href="">{$tag.tname}</a>、
								<?php endforeach ?>
								
							</div>
						</article>
						<?php endforeach ?>
					</main>
{/block}
