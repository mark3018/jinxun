<?php 
namespace app\lib\email;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


require __DIR__ . "/vendor/autoload.php";

//自定义Mail类
class Mail{
	public static $ErrorInfo;

	public static function send($address,$content){
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
			$mail->CharSet = 'UTF-8';
		    //Server settings
		    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.163.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'marvin2019@163.com';                 // SMTP username
		    $mail->Password = 'admin999';                           // SMTP password
		    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 994;                                    // TCP port to connect to

		    //Recipients
		    $mail->setFrom('marvin2019@163.com', '马老师');
		    //给谁发（收件人）
		    // $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
		    $mail->addAddress($address);               // Name is optional
		    // $mail->addReplyTo('info@example.com', 'Information');
		    // $mail->addCC('cc@example.com');
		    // $mail->addBCC('bcc@example.com');

		    //Attachments（附件）
		    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		    //Content（内容）
		    $mail->isHTML(true);                                  // Set email format to HTML
		    //主题
		    $mail->Subject = '**网站验证码';
		    //内容
		    $mail->Body    = $content;
		    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		    $mail->send();
		    // echo 'Message has been sent';
		    return true;
		} catch (Exception $e) {
		    self::$ErrorInfo = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
		    return false;
		}
	}
}


// Mail::send('123@163.com','abc');


 ?>