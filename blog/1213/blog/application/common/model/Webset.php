<?php

namespace app\common\model;

use think\Model;

class Webset extends Model
{
    //
	public static function getWebSet(){
		$model = self::find();
		$content = [];
		if($model){
			$content = json_decode($model->content,true);
		}
		return $content;
	}
}
