<?php
namespace app\admin\controller;

use think\Controller;
use think\Session;

class Common extends Controller
{

	public function _initialize(){
		//如果没有登录
		if(!Session::get('user')){
			//去登录
			return $this->redirect('login/login');
		}
	

	}

}

 ?>