<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use app\common\model\Category; 
use app\common\model\Tag; 
use app\common\model\ArcTag; 
use app\common\model\Article as ArticleModel; 

class Article extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //获取所有的栏目
        $categories = Category::all();
        //获取所有的标签
        $tags = Tag::all();
        return view('',compact('categories','tags'));
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        // dump(input('post.tid/a'));exit;
        //添加文章
        $model = new ArticleModel;
        $model->title = $request->post('title');
        $model->save();
       
        //添加文章的标签其实就是添加中间表
        foreach ($_POST['tid'] as $tid) {
            $arcTagModel = new ArcTag;
            $arcTagModel->aid = $model->aid;
            $arcTagModel->tid = $tid;
            $arcTagModel->save();
        }
       

        return $this->success('添加成功','/admin/article');
        
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
