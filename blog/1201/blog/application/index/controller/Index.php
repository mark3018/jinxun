<?php
namespace app\index\controller;
use app\common\model\Article; 
use app\common\model\Category; 
use app\common\model\Tag; 
use app\common\model\Webset; 

class Index extends Common
{
    public function index()
    {
    	

        $articles = Article::get_data();
	
        return view('',compact('articles'));
    }
}
