<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

Route::resource('admin/category','admin/Category');
Route::resource('admin/tag','admin/Tag');
Route::resource('admin/article','admin/Article');
//删除到回收站
Route::rule('admin/article/deleteToRecycle/:id','admin/Article/deleteToRecycle');
//从回收站恢复
Route::rule('admin/article/recover/:id','admin/Article/recover');
//回收站显示所有的文章
Route::rule('admin/article/recycle','admin/Article/recycle');