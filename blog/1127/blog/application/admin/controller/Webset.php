<?php
namespace app\admin\controller;
use think\Controller;
use think\Request;
use app\common\model\Webset as WebsetModel; 

class Webset extends Common
{
	public function index(){
		if(request()->isPost()){
			//如果能查询到那么将来的save就是修改，否则就是添加
			$model =  WebsetModel::find() ?: new WebsetModel;
			$model->content = json_encode(input('post.'));
			$model->save();
			
			return $this->success('配置成功');
		}
		return view('');
	}



}