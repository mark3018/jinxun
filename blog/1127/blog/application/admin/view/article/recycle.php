<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">
	    <!-- Loading Bootstrap -->
	    <link href="/static/admin/Flat/dist/css/vendor/bootstrap.min.css" rel="stylesheet">
	    <!-- Loading Flat UI -->
	    <link href="/static/admin/Flat/dist/css/flat-ui.css" rel="stylesheet">
	    <link href="/static/admin/Flat/docs/assets/css/demo.css" rel="stylesheet">
	    <link rel="shortcut icon" href="/static/admin/Flat/img/favicon.ico">
	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	    <!--[if lt IE 9]>
	      <script src="dist/js/vendor/html5shiv.js"></script>
	      <script src="dist/js/vendor/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<table class="table table-hover">
			<tr class="active">
			  <th width="30">aid</th>
			  <th>标题</th>
			  <th width="100">分类</th>
			  <th width="200">操作</th>
			</tr>
			<?php foreach($articles as $article): ?>
				<tr>
					<td>{$article.aid}</td>
					<td>{$article.title}</td>
					<td>{$article.category.cname}</td>
					<td>
						<a href="/admin/article/recover/{$article.aid}" class="btn btn-sm btn-warning">恢复</a>
						<a href="" class="btn btn-sm btn-danger">删除</a>
					</td>
				</tr>
			<?php endforeach ?>
		</table>
		<style type="text/css">
			ul.pagination li{
				background: black;
			}
		</style>
		{$articles->render()}
	</body>
</html>
