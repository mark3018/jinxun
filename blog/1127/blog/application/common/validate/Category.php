<?php 
namespace app\common\validate;
use think\Validate;
class Category extends Validate
{
	//验证的规则
    protected $rule = [
        'cname'  =>  'require|max:5',
        'keywords'=>'require'
    ];


    //验证不通过提示的信息
    protected $message = [
        'cname.require'  =>  '分类名称必填',
        'cname.max' =>  '分类名称不得超过5个字符',
        'keywords.require'=>'关键字必填'
    ];
    
    //验证场景
    protected $scene = [
        'save'   =>  ['cname','keywords'],
        'update'  =>  ['cname'],
    ]; 




}



 ?>