<?php
namespace app\index\controller;
use app\common\model\Article; 
use app\common\model\Category; 
use app\common\model\Webset; 
use think\Cache;
class Index
{
    public function index()
    {
    	//没有缓存读取数据库的数据，有缓存直接读取缓存
    	$articles = Cache::remember('article',function(){
			return Article::where('is_recycle',0)->select();
		});
		//栏目
		$categories = Category::all();



    	
        return view('',compact('articles','categories'));
    }
}
