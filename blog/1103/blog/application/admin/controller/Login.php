<?php

namespace app\admin\controller;

use think\Controller;
use app\common\model\Users;
use think\Request;
use think\Session;

class Login extends Controller
{
    //登录
    public function login(Request $request){
    	//如果是post提交
    	if($request->isPost()){
    		$post = input('post.');
    		$user = Users::where('username',$post['username'])->find();
    		if(!$user || $user['password'] != md5($post['password'])){
    			return	$this->error('用户名或者密码错误');
    		}

    		Session::set('user',$user->toArray());
    		return $this->success('登录成功','Index/index');

    	}
    	// $users = Users::select();
    	// foreach ($users as $user) {
    	// 	dump($user->toArray());
    	// }
    	// echo '<pre>';
    	// print_r($users);exit;

    	return view();
    }


    public function logout(){
    	echo 'logout';
    }
}
