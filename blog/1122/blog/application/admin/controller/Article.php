<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use app\common\model\Category; 
use app\common\model\Tag; 
use app\common\model\ArcTag; 
use app\common\model\Article as ArticleModel; 

class Article extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
        // $data = Db::table('article')
        // ->alias('a')
        // ->join('category c','a.cid = c.cid')
        // ->select();
        // dump($data);
        $articles = ArticleModel::where('is_recycle',0)->paginate(2);
        return view('',compact('articles'));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //获取所有的栏目
        $categories = Category::all();
        //获取所有的标签
        $tags = Tag::all();
        return view('',compact('categories','tags'));
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {


        $thumb = '';
        $file = request()->file('thumb');
        // dump($file);exit;
       // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
            if($info){
                // 成功上传后 获取上传信息
                $thumb = 'uploads/' . str_replace('\\', '/', $info->getSaveName());
                
            
            }else{
                // 上传失败获取错误信息
                echo $file->getError();
            }
        }


        // dump(input('post.tid/a'));exit;
        //添加文章
        $model = new ArticleModel;
        $model->title = $request->post('title');
        $model->thumb = $thumb;
        $model->content = $request->post('content');
        $model->save();
       
        //添加文章的标签其实就是添加中间表
        $tids = isset($_POST['tid']) ? $_POST['tid'] : [];
        foreach ($tids as $tid) {
            $arcTagModel = new ArcTag;
            $arcTagModel->aid = $model->aid;
            $arcTagModel->tid = $tid;
            $arcTagModel->save();
        }



       

        return $this->success('添加成功','/admin/article');
        
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //获取所有的栏目
        $categories = Category::all();
        //获取所有的标签
        $tags = Tag::all();
        //获取文章旧数据
        $article = ArticleModel::find($id);
        // dump($article);
        return view('',compact('categories','tags','article'));
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //

    }

    //删除到回收站
    public function deleteToRecycle($id){
        
    }
}
