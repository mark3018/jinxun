<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">
	    <!-- Loading Bootstrap -->
	    <link href="/static/admin/Flat/dist/css/vendor/bootstrap.min.css" rel="stylesheet">
	    <!-- Loading Flat UI -->
	    <link href="/static/admin/Flat/dist/css/flat-ui.css" rel="stylesheet">
	    <link href="/static/admin/Flat/docs/assets/css/demo.css" rel="stylesheet">
	    <link rel="shortcut icon" href="/static/admin/Flat/img/favicon.ico">
	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	    <!--[if lt IE 9]>
	      <script src="dist/js/vendor/html5shiv.js"></script>
	      <script src="dist/js/vendor/respond.min.js"></script>
	    <![endif]-->
	    <script type="text/javascript" src='/static/js/jquery.min.js'></script>
	</head>
	<body>
		<table class="table table-hover">
			<tr class="active">
			  <th width="30">cid</th>
			  <th>分类名称</th>
			  <th width="210">操作</th>
			</tr>
			{foreach $data as $v} 
				<tr>
					<td>{$v.cid}</td>
					<td>{$v.cname}</td>
					<td>
						<a href="/admin/category/{$v.cid}/edit" class="btn btn-sm btn-warning">编辑</a>
						<a href="javascript:del({$v.cid});" class="btn btn-sm btn-danger">删除</a>
					</td>
				</tr>
			{/foreach}
		</table>
		<script type="text/javascript">
			function del(id){
				if(confirm('确定删除吗')){
					$.ajax({
						url:'/admin/category/' + id,
						//因为资源路由要求删除必须使用DELETE请求方式，所以借助ajax设置为DELETE
						method:'DELETE',
						dataType:'json',
						success:function(response){
							// console.log(response);
							if(response.status){
								location.href='/admin/category';
							}
						}
					});
				}
			}
		</script>
	</body>
</html>
