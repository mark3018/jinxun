<?php 
//视图是一组查询记录，并不是真实存在的表
//视图可以放一些不太敏感的字段，比如姓名、年龄，如果涉及到类似金钱的字段，那可以不放在视图中

//创建视图
create view bankview as select name from bank;

//插入动作
insert into bankview set name='test';

//删除视图
drop view bankview;

//修改视图
alter view bankview as select bid,name from bank;

 ?>