<?php 
//myisam  查询性能强悍，表级锁，不支持事务
//innodb  支持事务，行级锁，外键约束

// 外键约束，保证数据的完整性，不能随意删除有文章的栏目
category 
cid      cname
1         PHP
2        Linux 


article   
aid      title                  cid
 1       php相关的文章           1
 2       laravel相关的文章       1







//查看创建表语句（可以看到索引、表的引擎）
show create table article;
//修改表的引擎
alter table article engine=innodb;





 ?>