<?php 
//
create table stu(
	sid tinyint,
	id tinyint(5) zerofill,
);
//2018年12月25日 19:41:51

create table user(
	id smallint,
	username varchar(20) not null default '',
	password char(32) not null default ''
);

//增加主键索引
alter table user add primary key(id);
//增加唯一索引
alter table user add unique username_key(username);


create table goods(
	id smallint,
	title varchar(120) not null default '',
	content text
);

//增加普通索引
alter table goods add index title_key(title);


//查看索引
show create table goods;
desc goods;

//删除索引
alter table goods drop index title_key;
//删除主键索引
alter table user drop primary key;


//关联表一定要加上索引
category表
cid     cname
 1      php
 2      python
 3      linux



 article表
 aid     title     cid
  1      文章PHP    1
  2      文章linux  3


select * from category c join article a on c.cid=a.cid;




//前提：一定是查询频繁的列
//算维度
//维度=不重复的条数/总条数

//假设10000条数据
//性别的维度 = 2 / 10000，性别的维度特别低
//用户名的维度 = 10000 / 10000 , 姓名的维度特别高


//前缀索引（对于字段内容比较长的情况，前缀索引占用空间小，速度更快，但是随着数据的改变，需要逐渐的调整）
//算前缀索引公式，黄金值是0.31
select count(distinct(left(title,1)))/count(*) from goods;
//加前缀索引
alter table goods add index title_key(title(1));




 ?>