<?php 
//bank
// create table bank(
// 	bid smallint primary key auto_increment,
// 	name char(15) not null default '',
// 	money decimal(5,2) not null default 0
// ) engine=innodb;


$dsn = 'mysql:host=127.0.0.1;dbname=student';
try{
	$pdo = new PDO($dsn,'root','root');
	$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	$pdo->exec("SET NAMES UTF8");
	//开启事务
	$pdo->exec("BEGIN");
	//维壮账户减去500元
	$pdo->exec("UPDATE bank SET money=money-500 WHERE name='维壮'");
	//张震化账户增加500元
	$pdo->exec("UPDATE bank SET money=money+500 WHERE name='张震化'");
	//确认提交
	$pdo->exec("COMMIT");

}catch(PDOException $e){
	//如果发生了错误回滚
	$pdo->exec("ROLLBACK");
	exit($e->getMessage());
}


 ?>