<?php 
//
create table article(
	aid int primary key auto_increment,
	title char(120) not null default '',
	content text,
	click smallint unsigned not null default 0,
	good smallint unsigned not null default 0
);

//增加组合索引
alter table article add index click_good_key(click,good);


 ?>