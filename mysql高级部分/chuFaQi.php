<?php 
//查看触发器
show triggers\G

//删除触发器
// drop trigger 触发器名称

//创建一个触发器，删除班级，让其自动删除班级对应的学生

//班级表
create table c(
	cid smallint primary key auto_increment,
	cname varchar(5) not null default ''
);

//学生表
create table s(
	sid int primary key auto_increment,
	sname char(15) not null default '',
	cid smallint unsigned not null default 0
);

\d $

create trigger after_delete_c after delete on c
for each row
begin
	delete from s where cid=old.cid;
end
$



 ?>