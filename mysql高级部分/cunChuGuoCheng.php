<?php 
//




grade
gid      gname
 1       1年级
 2       2年级


 student
 sid     sname   gid
  1      小米     1
  2      刘宝良   2


class School{
	//删除年级
	public static function deleteGrade($grade){
		//1.删除所有的对应的学生

		//2.删除对应的班级

	}	
}

School::deleteGrade('1年级');


//如果有多种应用程序，那么每个应用程序中都得封装一次，那么我们把封装放到更高的层面：mysql，所以我们使用mysql的存储过程


//更改结束符
\d $$


//创建存储过程
\d $
create procedure age(in arg tinyint)
begin
	declare age tinyint default 0;
	set age=arg;
	IF age<20 THEN
		select '年轻人';
	ELSEIF age<40 THEN
		select '青年人';
	END IF;
end
$


//查看存储过程
SHOW PROCEDURE STATUS\G

//调用存储过程
call age(50)$

//删除存储过程
DROP PROCEDURE age;



//in(向参数传入值)
//可以输出外部传入的值
create procedure in_test(in id int)
begin
	set id=100;
end
$

set @id=1$
call in_test(@id)$
select @id$


//out(给参数赋值，结束之后可以在外部访问到变量)
create procedure out_test(out id int)
begin
	set id=100;
end
$

set @id=1$
call out_test(@id)$
select @id$


//不可以输出外部传入的值
create procedure out_test2(out id int)
begin
	select id;
end
$
set @id=1$
call out_test2(@id)$


//inout(两者皆具)











 ?>