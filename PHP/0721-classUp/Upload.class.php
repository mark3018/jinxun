<?php

/**
 * 上传类
 * Class Upload
 */
class Upload
{
    //这里相当于系统默认的配置项
    //支持上传的类型
    public $allowType;
    //支持的文件大小
    public $size;
    //目录
    public $path;
    //上传文件的数量
    public $count;
    //用户提示上传的错误信息
    public $error;

    public function __construct($allowType = null, $size = null,$path = null)
    {
        //：后面的都为用户设置的配置项
        $this->allowType = is_null($allowType) ? ['jpg', 'png', 'gif'] : $allowType;
        $this->size = is_null($size) ? 2000000 : $size;
        $this->path = is_null($path) ? './Upload/' : $path;
    }

    /**
     * 上传总方法
     */
    public function manyUp()
    {
        //1.重组数组
        $newArr = $this->resetArr();
        //2.筛选
        foreach ($newArr['up'] as $k => $v) {
            if(!$this->filter($v)){
                return false;
            };
        }
        foreach($newArr['up'] as $k=>$v){
            //3.移动上传
            $this->up($v);
        }
        return true;
    }

    /**
     * 1.重组数组
     */
    private function resetArr()
    {
        $values = $_FILES['up'];
        $arr = [];
        $this->count = count($values['name']);
        foreach ($values['name'] as $k => $v) {
            if (strlen($v) == 0) {
                //说明你没有上传这个文件
                $this->count--;
            }
            $arr['up'][] = [
                'name' => $v,
                'type' => $values['type'][$k],
                'tmp_name' => $values['tmp_name'][$k],
                'error' => $values['error'][$k],
                'size' => $values['size'][$k],
            ];
        }
        $arr['count'] = $this->count;
        return $arr;
    }

    /**
     * 2.筛选
     */
    private function filter($v)
    {
        $type = strtolower(ltrim(strrchr($v['name'], '.'), '.'));
        switch ($v) {
            case $v['error'] !== 0:
                $this->error = '文件上传失败';
                return false;
            case !is_uploaded_file($v['tmp_name']):
                $this->error = '不是合法上传的文件';
                return false;
            case !in_array($type, $this->allowType):
                $this->error = '不支持此文件类型';
                return false;
            case $v['size'] > $this->size;
                $this->error = '超出文件限定大小';
                return false;
            default:
                return true;
        }
    }

    /**
     * 3.上传
     */
    private function up($v){
        //2.组一个上传到哪儿去的路径
        //创建目录
        is_dir($this->path) || mkdir($this->path,0777,true);
        //获取文件类型
        $type = pathinfo($v['name'])['extension'];
        //组一个完整的路径
        $dest = $this->path . 'jinxun_' . mt_rand(0,9999) . '.' . $type;
        //1.移动上传
        move_uploaded_file($v['tmp_name'],$dest);
    }
}
