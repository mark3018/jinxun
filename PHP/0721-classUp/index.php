<?php

/**
 * 单路口文件
 */

//载入函数库
include '../functions.php';

if(IS_POST){
//    实例化类时 给类传参就相当于设置配置项为用户的配置项
    $upload = new Upload(['jpg'],100000);
    $bool = $upload -> manyUp();
    if($bool){
        p('上传成功');
    }else{
        p($upload -> error);
    }
}

//载入视图
include './view.html';