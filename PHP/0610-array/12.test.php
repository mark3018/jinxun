<?php
include "../functions.php";

//创建递归函数完成多维数组键名大写转换

$arr = [
    'a'=>'A',
    'b'=>'B',
    'c'=>[
        'd'=>'D',
        'e'=>[
            'f'=>'F',
        ]
    ]
];
p($arr);

function change_case($arr,$default = CASE_UPPER){
    $newArr = array_change_key_case($arr,$default);
    foreach($newArr as $k=>$v){
        if(is_array($v)){
            $newArr[$k] = change_case($v,$default);
        }
    }
    return $newArr;
}

$newArr = change_case($arr);
p($newArr);