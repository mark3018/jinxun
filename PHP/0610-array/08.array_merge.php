<?php

include "../functions.php";

$arr1 = ['a','b','c'];
$arr2 = ['d','e','f'];

//索引数组的合并，进行一个追加
//$newArr = array_merge($arr1,$arr2);


$arr1 = ['a'=>'A','b'=>'B','c'=>'C'];
$arr2 = ['b'=>2,'c'=>3,'e'=>'E'];

//关联数组 键名一样的 会进行覆盖
$newArr = array_merge($arr1,$arr2);

p($newArr);