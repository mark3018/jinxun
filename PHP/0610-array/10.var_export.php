<?php
include "../functions.php";

$arr  = [
    [
        'title'=>'你的孤独虽败犹荣',
        'author'=>'刘同',
    ],
    [
        'title'=>'高手',
        'author'=>'万维钢',
    ],
    [
        'title'=>'你的孤独虽败犹荣',
        'author'=>'刘同',
    ],
    [
        'title'=>'高手',
        'author'=>'万维钢',
    ]
];

echo '<pre>';
var_dump($arr);
echo '</pre>';

p($arr);

echo '<pre>';
//打印出来的数据为合法的PHP代码  加上参数二  可以有返回值
//在没接触MySQl之前 我们将数据压入模拟数据库时(PHP文件) 我们都需要压入的是合法的PHP代码
$newArr = var_export($arr,true);
echo '</pre>';

p($newArr);