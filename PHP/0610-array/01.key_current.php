<?php

include "../functions.php";

$arr = [
    'a'=>'A',
    'b'=>'B',
    'c'=>'C',
];

p($arr);
echo '<hr>';

//返回数组中当前单元的键名
p(key($arr));
//向数组中的指针向前移动一位
next($arr);
p(key($arr));
next($arr);
p(key($arr));

echo '<hr>';
p(current($arr));
prev($arr);
p(current($arr));