<?php

include "../functions.php";

/**
 * 剪切文件夹
 * @param $source 需要复制的文件夹
 * @param $dec 目标文件夹
 * @return bool
 */
function cutDir($source, $dec)
{
    //检测目标目录是否存在 不存在创建
    is_dir($dec) || mkdir($dec,0777,true);
    return rename($source, $dec . '/' . basename($source));
}
// 需要复制的文件夹
$source = './source';
//目标文件夹
$dec = './dec';
if (cutDir($source, $dec)) {
    echo '剪切成功';
} else {
    echo '剪切失败';
};
