<?php

include "../functions.php";

/**
 * 目录剪切函数方法。
 */

/**
 * @param $source 要剪切谁
 * @param $dest 剪切到哪里去
 */
function cut($source,$dest){
    if(cp($source,$dest) && del($source)){
        p('剪切成功');
    }else{
        p('剪切失败');
    }
}

//复制目录
function cp($source,$dest){
//    p($source);
//    p($dest);
    //1.判断源是否是一个目录
    if(!is_dir($source)) return false;
    //2.创建目标目录
    is_dir($dest) || mkdir($dest,0777,true);
    //3.进行遍历源目录
    foreach(glob($source . '/*') as $v){
        $newDest = $dest . '/' . basename($v);
        is_dir($v) ? cp($v,$newDest) : copy($v,$newDest);
    }
    return true;
}

//删除
function del($source){
    if(!is_dir($source)) return false;
    foreach(glob($source . '/*') as $v){
        is_dir($v) ? del($v) : unlink($v);
    }
    return rmdir($source);
}

cut('./aa/b','./yang/b');