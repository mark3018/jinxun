<?php

/**
 * 函数库
 */
//头部
header('Content-type:text/html;charset=utf-8');


//p打印函数
function p($con){
    echo "<pre style='padding: 15px;background: #2E3E4E;border-radius: 46px;color: white;font-size: 18px;font-weight: 700'>";
    if(is_null($con)){
        var_dump($con);
    }elseif (is_bool($con)){
        var_dump($con);
    }else{
        print_r($con);
    }
    echo "</pre>";
}

//设定时区
date_default_timezone_set('PRC');


/**
 * 提示信息
 * @param $msg 提示信息
 * @param $url 跳转地址
 */
function message($msg,$url){
    $str = <<<str
<script>
alert("$msg");
location.href = "$url";
</script>
str;
    echo $str;die;
}


/**
 * 判断是否为post提交
 */
define('IS_POST',($_SERVER['REQUEST_METHOD'] == 'POST') ? TRUE : FALSE);