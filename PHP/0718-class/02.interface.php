<?php

include "../functions.php";

/**
 * 接口类
 * 接口类 说白了就是一个类的模板，一个类的规定，如果你属于这类，
 * 你就必须遵循我的规定，少一个都不行，但是具体方法怎么去写，你怎么去做，
 * 我不管 那是你的事
 * Interface Model
 */
interface Model
{
    //不能有构造方法
//    public function __construct()
//    {
//        p('我是自动的');
//    }

    //不能有普通方法
//    public function index(){
//        p('我是普通方法');
//    }

    public function query();

    public function exec();
}

interface Model2
{
    public function index();
}

class Son implements Model,Model2
{
    public function query()
    {
        p('查询数据库');
    }

    public function exec()
    {
        p('增删改');
    }

    public function index(){
        p('接口类可以有多个');
    }
}

$Son = new Son();
$Son->query();
$Son->exec();
$Son->index();


/**
 *
 * 抽象类与接口类
 * 如果要创建一个模型，这个模型将由一些紧密相关的对象采用，就可以使用抽象类。
 * 如果要创建由一些不相关对象采用的功能，就可以接口。
 * 如果必须从多个来源继承行为，就用接口类。
 * 如果知道所有类都会共享一个公共的行为实现，就用抽象类，并在其中实现该行为。
 *
 */