<?php

include "../functions.php";

/**
 * 抽象类
 * Class Transportation
 */
abstract class Transportation
{
    //抽象类当中构造方法可以运行
    public function __construct()
    {
        p('我是自动执行的');
    }

    //抽象类里可以有普通方法
    public function index()
    {
        p('我是普通方法');
    }

    //抽象方法不能有代码体，需要去子类中重写方法
    abstract public function sport();
}

class Car extends Transportation
{
    public function sport()
    {
        p('我是汽车');
    }

    public function index()
    {
        parent::index();
    }
}

class Plane extends Transportation{
    public function sport()
    {
        p('我是飞机');
    }
}

$Car = new Car();
$Car->sport();
$Car->index();

$Plane = new Plane();
$Plane -> sport();