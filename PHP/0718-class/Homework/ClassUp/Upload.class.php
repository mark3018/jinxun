<?php

/**
 * 上传类
 * Class Upload
 */
class Upload
{
    //给用户提示的信息
    public $error;
    //目录
    public $path;

    /**
     * 构造方法 用户配置项思路
     * Upload constructor.
     * @param null $path
     */
    public function __construct($path=null)
    {
        $this->path = is_null($path) ? './Upload/' : $path;
    }

    /**
     * 给类外部进行调用的一个方法
     * 作为一个总方法
     */
    public function manyUp()
    {
        //1.重组数组 获得新数组
        $newArr = $this->resetArr();
        //2.文件过滤
        foreach ($newArr as $v) {
            $this->filter($v);
        }
        //3.移动上传
        $this->up($v);
        return true;
    }

    /**
     * 1.进行重组数组
     * @return mixed
     */
    private function resetArr()
    {
        return $newArr;
    }

    /**
     * 2.文件过滤
     * @param $newArr
     */
    private function filter($v)
    {
        switch ($v) {
            case $v['error'] !== 0;
                $this->error = '文件上传失败';
                return false;
            //...其他情况自己完善

            default:
                return true;
        }
    }

    public function up()
    {
        return true;
    }
}

