<?php

/**
 * 首页控制器
 */
class IndexController extends CommonController{
    /**
     * 找见未找见的方法
     */
    public function __call($name, $arguments)
    {
        //用户提示  避免找不见的方法页面报错
//        p($name . '方法未找见，请回到首页');
        $this->success("sorry...【{$name}】方法未找见！！！",'index.php');
    }

    /**
     * 首页
     */
    public function index(){
        include "./view/index.html";
    }

    /**
     * 添加
     */
    public function add(){
//        $this->success('添加成功','index.php');
        include "./view/add.html";
    }

    /**
     * 编辑
     */
    public function edit(){
        include "./view/edit.html";
    }
}
