<?php

/**
 * 单入口文件
 */

include "../../functions.php";

//找见未找见的类
function __autoload($className){
    //p($className);//IndexController
    $path = "./controller/{$className}.class.php";
    include $path;
}

$IndexController = new IndexController();
//让调用的方法活起来
$action = isset($_GET['a']) ? $_GET['a'] : 'index';
$IndexController -> $action();