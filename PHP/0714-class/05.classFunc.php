<?php

include "../functions.php";

class Model{
    public $db = '数据库';
    public $jinxun = '金讯教育';

    public function index(){
        p('我是首页');
    }

    public function add($param1,$param2){
        return '参数1为：' . $param1 . '<hr>参数2为：' . $param2;
    }
}

$model = new Model();

//返回类中方法的数组
p(get_class_methods('Model'));

//通过类名类获得 返回类中属性的数组 键名是属性名  键值是属性值
p(get_class_vars('Model'));

//通过对象来获得 返回类中属性的数组 键名是属性名  键值是属性值
p(get_object_vars($model));

//调用回调函数，把一个数组参数作为回调函数的参数
//函数的方式
function func($a,$b){
    return $a * $b;
}
p(call_user_func_array('func',[2,5]));

//类的方式
p(call_user_func_array([$model,'add'],[111,666]));

//检测类中的方法是否存在  返回布尔值
p(method_exists($model,'add1'));