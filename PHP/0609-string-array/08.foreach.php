<?php

include "../functions.php";

/**
 * 数组的遍历  看到数组就想到遍历 想到foreach
 */

$arr = [
    'a'=>[
        'a'=>'A',
        'b'=>'B',
    ],
    'b'=>[
        'a'=>'A',
        'b'=>'B',
    ],
    'c'=>[
        'a'=>'A',
        'b'=>'B',
    ],
    'd'=>[
        'a'=>'A',
        'b'=>'B',
    ],
];

//$arr = [
//    'a'=>'A',
//    'b'=>'B',
//    'c'=>'C'
//];
p($arr);

foreach($arr as $k=>$v){
    //p($k);//键名
    //p($v);//键值
    foreach($v as $kk=>$vv){
        //p($kk);
        p($vv);
    }
}