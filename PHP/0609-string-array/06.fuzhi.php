<?php

include '../functions.php';

//以array()进行赋值(这种用法已经淘汰)
//$arr = array(1,2,3);


//[]类似于array()函数进行赋值
$arr  = [1,2,3];


//直接赋值
$arr[3] = 4;

$arr = [
    1,
    6=>2,
    3
];
p($arr);