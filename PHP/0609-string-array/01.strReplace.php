<?php

include "../functions.php";

/**
 * 字符串替换
 */
$str = 'iamsingerboy';
p($str);

//单字符串替换
$newStr = str_replace('i','8',$str);

//将字符串中多个字符进行替换一个字符
$newStr = str_replace(['i','y'],6,$str);

//将字符串中多个字符替换多个字符
$newStr = str_replace(['i','y'],['8','6'],$str);
p($newStr);