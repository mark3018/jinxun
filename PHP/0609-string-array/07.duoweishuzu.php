<?php

include '../functions.php';

/**
 * 二维数组
 */
$arr  = [
    'a'=>'A',
    'b'=>'B',
    'c'=>'C',
    'd'=>[
        'a'=>'E',
        'b'=>'F'
    ]
];

/**
 * 三维数组
 */
$arr  = [
    'a'=>'A',
    'b'=>'B',
    'c'=>'C',
    'd'=>[
        'a'=>'E',
        'b'=>[
            'F',
            'G'
        ]
    ]
];
p($arr);