<?php

//创建一个函数 检测文件是否为图片

include '../functions.php';

function is_img($file){
    //获取文件后缀
//    $file = strtolower($file);
//    $file = strrchr($file,'.');//字符串截取
//    $file = ltrim($file,'.');

    //一步到位
    $file = strtolower(ltrim(strrchr($file,'.'),'.'));//txt

    //有一个数组  放了很多文件后缀
    $arr = ['jpg','png','gif','bmp'];
    if(in_array($file,$arr)){
        echo "<script>alert('可以上传')</script>";
    }else{
        echo "<script>alert('请上传图片')</script>";
    }
}

$file = '1.JPG';
is_img($file);