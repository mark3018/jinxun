<?php

include '../functions.php';

//键名 => 键值
//Array
//(
//    [0] => a
//    [1] => b
//    [2] => c
//    [3] => d
//)

//索引数组 键名为数值类型
//$arr = ['a','b','c','d'];


//关联数组 键名为字符串类型
$arr = [
    'a'=>'A',
    'b'=>'B',
    'c'=>'C'
];

p($arr);