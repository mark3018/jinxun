<?php
//dst_im目标图像
//src_im被拷贝的源图像
//dst_x目标图像开始 x 坐标
//dst_y目标图像开始 y 坐标，x,y同为 0 则从左上角开始
//src_x拷贝图像开始 x 坐标
//src_y拷贝图像开始 y 坐标，x,y同为 0 则从左上角开始拷贝
//dst_w：目标图像的宽度。******
//dst_h：目标图像的高度。******
//src_w(从 src_x 开始)拷贝的宽度
//src_h(从 src_y 开始)拷贝的高度
//imagecopyresized ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y , int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )

//发送头部
header('Content-type:image/png');

//创建画布
$img = imagecreatetruecolor(300,300);

//取色
$color = imagecolorallocate($img,255,0,0);

//填充
imagefill($img,0,0,$color);

//缩略
$srcImg = imagecreatefromjpeg('3.jpeg');
$srcX = imagesx($srcImg);
$srcY = imagesy($srcImg);

imagecopyresized($img,$srcImg,0,0,0,0,300,300,$srcX,$srcY);

//输出
imagepng($img,'thumb.png');

//销毁图像资源
imagedestroy($img);
