<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="../jquery.min.js"></script>
    <script>
        $(function(){
            $('#btn').click(function(){
                var src = $('#pic').prop('src');
                //由于缓存问题会造成图片不切换，我们需要给src的值加上一个参数，让每次请求都不一样，加入随机数，这样每次刷新都是新的，就不存在缓存问题喇。
                $('#pic').prop('src',src + '?rand=' + Math.random());
            })
        })
    </script>
</head>
<body>
    验证码：<input type="text">
    <img src="01.code.php" id="pic" onclick="this.src = this.src + '?rand=' + Math.random()">
    <button id="btn">点我切换~</button>
</body>
</html>
