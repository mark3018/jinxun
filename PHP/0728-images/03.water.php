<?php
//dst_im 目标图像
//src_im 被拷贝的源图像
//dst_x 目标图像开始 x 坐标
//dst_y 目标图像开始 y 坐标，x,y同为 0 则从左上角开始
//src_x 拷贝图像开始 x 坐标
//src_y 拷贝图像开始 y 坐标，x,y同为 0 则从左上角开始拷贝
//src_w （从 src_x 开始）拷贝的宽度
//src_h （从 src_y 开始）拷贝的高度
//pct 图像合并程度,取值 0-100,当 pct=0 时,实际上什么也没做,反之完全合并.
//imagecopymerge( resource dst_im, resource src_im, int dst_x, int dst_y, int src_x, int src_y, int src_w, int src_h, int pct );

//发送头部
header('Content-type:image/png');

//获取目标文件 要加水印的图片
$dstImg = imagecreatefromjpeg('2.jpg');
$dstW = imagesx($dstImg);//1920
$dstH = imagesy($dstImg);//1080

//获取源文件 作为水印的图片
$srcImg = imagecreatefromjpeg('1.jpg');
$srcW = imagesx($srcImg);//700
$srcH = imagesy($srcImg);//525

$x = $dstW - $srcW - 50;
$y = $dstH - $srcH -50;

//加水印
imagecopymerge($dstImg,$srcImg,$x,$y,0,0,$srcW,$srcH,50);

imagepng($dstImg,'water.png');

imagedestroy($dstImg);
imagedestroy($srcImg);
