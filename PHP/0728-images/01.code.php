<?php

//发送头部
header('Content-type:image/png');

//创建画布
$img = imagecreatetruecolor(200,100);

//取色
$yellow = imagecolorallocate($img,255,216,101);

//填充
imagefill($img,0,0,$yellow);

/**
 * 验证码文字
 */
//种子
$code = 'zxcvbnmasdfghjklqwertyuiop1234567890';
//mt_rand比rand快4倍
for($i=0;$i<4;$i++){
    $text = $code[mt_rand(0,strlen($code)-1)];
    $x = (200/4)*$i+10;
    $y = 100/2 + mt_rand(-20,20);
    $color = imagecolorallocate($img,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
    imagettftext($img,40,mt_rand(-45,45),$x,$y,$color,'../0722-images/simsun.ttc',$text);
}

/**
 * 验证码干扰
 */
//加点
for($i=0;$i<1000;$i++){
    $color = imagecolorallocate($img,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
    imagesetpixel($img,mt_rand(0,200),mt_rand(0,100),$color);
}
//划线
for($i=0;$i<10;$i++){
    $color = imagecolorallocate($img,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
    imageline($img,mt_rand(0,200),mt_rand(0,100),mt_rand(0,200),mt_rand(0,100),$color);
}

//输出图像
imagepng($img);

//销毁图像资源
imagedestroy($img);