<?php

include "../functions.php";

/**
 * PHP创建图像
 */

//发送头部 告诉浏览器即将输出一个png类型的文件
//********如果图像未能显示，注释头部 查看报错********
header('Content-type:image/png');

//创建画布
$img = imagecreatetruecolor(500,500);

//取色
$color = imagecolorallocate($img,255,0,0);

//填充
imagefill($img,0,0,$color);

//空心矩形
$orange = imagecolorallocate($img,245,209,39);
imagerectangle($img,30,30,470,470,$orange);
//实心矩形
$blue = imagecolorallocate($img,4,62,249);
imagefilledrectangle($img,50,50,450,450,$blue);

//空心圆
$yellow = imagecolorallocate($img,255,216,101);
imageellipse($img,250,250,400,400,$yellow);
//实心圆
imagefilledellipse($img,250,250,300,300,$yellow);

//划线
$black = imagecolorallocate($img,0,0,0);
imageline($img,0,250,500,250,$black);
imageline($img,250,0,250,500,$black);

//画点 一个像素 很小很小
imagesetpixel($img,235,235,$black);

//写字
$green = imagecolorallocate($img,32,168,0);
imagettftext($img,60,-20,140,230,$green,'simsun.ttc','杨建军');


//输出图像 写了参数二  图片可以进行保存
imagepng($img,'jianjun.png');

//销毁图像资源
imagedestroy($img);