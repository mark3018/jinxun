<?php

include "../functions.php";

/**
 * PHP创建图像
 */

//发送头部 告诉浏览器即将输出一个png类型的文件
//********如果图像未能显示，注释头部 查看报错********
header('Content-type:image/png');

//创建画布
$img = imagecreatetruecolor(500,500);

//取色
$color = imagecolorallocate($img,255,0,0);

//填充
imagefill($img,0,0,$color);

//输出图像 写了参数二  图片可以进行保存
//imagepng($img,'red.png');
imagepng($img);

//销毁图像资源
imagedestroy($img);