<?php
/*函数库*/

define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
define('IS_POST', REQUEST_METHOD == 'POST' ? true : false);

/**
 * 错误提示 返回上一页
 * @param string $message
 */
function error($message='操作有误哦')
{
    $prevPage = $_SERVER['HTTP_REFERER'];
    echo <<<TPL
    <script>
      alert("{$message}");
      location.href = "{$prevPage}";
    </script>
TPL;
    exit();
}

/**
 * 成功提示 返回上一页
 * @param string $message
 * @param string $prevPage
 */

function success($message='操作成功哦', $prevPage = '')
{
    if(empty($prevPage)){
        $prevPage = $_SERVER['HTTP_REFERER'];
    }
    echo <<<TPL
    <script>
      alert("{$message}");
      location.href = "{$prevPage}";
    </script>
TPL;
    exit();
}

/**
 * 将数组数据写入数据文件
 * @param $data
 */
function insert($data)
{
    $dataFile = var_export($data, true);
    file_put_contents('./data/data.php', "<?php\r\nreturn {$dataFile} ?>");
}

/**
 * 上传文件
 * @param $filename form表单的name
 * @return bool|string 成功返回路径 失败返回false
 */
function upfile($filename){
    $image = $_FILES[$filename];
    //获取文件名
    $filename = basename($image['name']);
    //临时文件
    $tmpFile = $image['tmp_name'];
    //设置文件路径 images/date1(日期)/date2(时间搓)
    $date1 = date('Y-m-d');
    //检测文件夹是否存在
    $decDir = './images/'. $date1;
    if(!is_dir($decDir)){
        mkdir($decDir,0777,true);
    }
    //组装文件名 时间搓命名
    $filename = time().'.'.pathinfo($filename, PATHINFO_EXTENSION);
    //最后的文件目标路径
    $filePath = $decDir.'/'.$filename;
    //移动文件到指定目录 ^_^
    return rename($tmpFile,$filePath)?$filePath:false;
}













