<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>上传文件</title>
    <link rel="stylesheet" href="./html/bootstrap/css/bootstrap.min.css">
   </head>
<body>


<!--处理表单提交-->
<?php
include_once('./func/func.php');
$data = include_once './data/data.php';
if (IS_POST) {
    if($path = upfile('image')){
        $newData = array(
            'images' => $path
        );
        $data[] = $newData;
        insert($data);
        success();
    }else{
        error();
    };
}
?>

<!--拉取数据表-->
<div class="container">
    <div class="page-header">
        <h1>上传文件 <small>上传文件</small></h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">文件列表</div>
        <style>
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                vertical-align: middle;
            }
        </style>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>文件路径</th>
                <th>缩略图</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $str = '';
            foreach ($data as $k => $val) {
                $str .= <<<TPL
                        <tr>
                            
                            <td>{$val['images']}</td>                       
                            <td> <a href="{$val['images']}" target="_blank"><img style="width: 40px; height: 40px;" src="{$val['images']}"></a> </td>                       
                            <td><a href="del.php?id={$k}" onclick="return confirm('确认操作？此操作不可逆！')"><span class="glyphicon glyphicon-trash"></span> </a></td>
                        </tr>
TPL;
            }
            echo $str;
            ?>
            </tbody>
        </table>
    </div>
</div>

<!--表单-->
<div class="container ">
    <div class="well">
        <form action="index.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="upload">选择文件 <span class="glyphicon glyphicon-folder-open"></span> </label>
                <input type="file" id="upload"  name="image">
            </div>
            <button type="submit" class="btn btn-default">提交 <span class="glyphicon glyphicon-open"></span></button>
        </form>
    </div>
</div>
</body>
</html>



