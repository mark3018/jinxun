<?php

/*用户注册登录类*/

class userController
{
    //用户数据库
    private $db;
    //数据库的位置
    private $dbPath;

    function __construct()
    {
        $this->dbPath = './data/userData.php';
        $this->db = include $this->dbPath;
    }

    /*登录*/
    public function login()
    {
        if (isLogin()) {
            success('您已经登录', './index.php?c=index&a=index');
        }
        $retrunData = array();
        if (IS_POST) {
            //判断是否为空
            if (empty($username = $_POST['username']) || empty($password = $_POST['password'])) {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '用户或密码不能为空';
                ajaxReturn($retrunData);
            }
            //检测验证码
            if (!empty($code = $_POST['code'])) {
                if ($code != $_SESSION['code']) {
                    $retrunData['status'] = 0;
                    $retrunData['msg'] = '验证码输入有误';
                    ajaxReturn($retrunData);
                }
            } else {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '请输入验证码';
                ajaxReturn($retrunData);
            }

            //查找账号是否存在
            foreach ($this->db as $k => $v) {
                if ($v['username'] == $username && $v['password'] == md5($password)) {
                    //将用户名记录到session中
                    $_SESSION['user']['id'] = $k;
                    $_SESSION['user']['username'] = $username;
                    //检测是否记住登录状态
                    if ($remeber = $_POST['remeber'] == 1) {
                        setcookie(session_name(), session_id(), time() + 3600 * 7);
                    }
                    $retrunData['status'] = 'ok';
                    $retrunData['msg'] = '登录成功';
                    ajaxReturn($retrunData);
                }
            }
            $retrunData['status'] = 0;
            $retrunData['msg'] = '用户或密码不对哦';
            ajaxReturn($retrunData);
        }
        include "./view/user/login.html";
    }

    /*注册*/
    public function regedit()
    {
        if (IS_POST) {
            $retrunData = array();
            //判断是否为空
            if (empty($username = $_POST['username']) || empty($password = $_POST['password']) || empty($password2 = $_POST['password2'])) {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '用户或密码不能为空';
                ajaxReturn($retrunData);
            }
            //检测是否有重复名称
            foreach ($this->db as $v) {
                if ($v['username'] == $username) {
                    $retrunData['status'] = 0;
                    $retrunData['msg'] = '用户名已存在';
                    ajaxReturn($retrunData);
                }
            }
            //检测密码是否一致
            if ($password != $password2) {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '密码不一致';
                ajaxReturn($retrunData);
            }
            //移除多余的项目
            unset($_POST['password2']);
            $_POST['time'] = time();
            //加密密码
            $_POST['password'] = md5($password);
            //存入数据库中
            $this->db[] = $_POST;
            if (insert($this->db, $this->dbPath)) {
                $retrunData['status'] = 'ok';
                $retrunData['msg'] = '注册成功';
                ajaxReturn($retrunData);
            } else {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '注册失败';
                ajaxReturn($retrunData);
            };
            exit();
        }
        include "./view/user/regedit.html";
    }

    /*获取验证码*/
    public function code()
    {
        include_once './tools/code/codeClass.php';
        $codeClass = new codeClass();
        $codeClass->code();
    }

    /*退出登录*/
    public function outLogin()
    {
        $_SESSION = array();
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time() - 42000);
        }
        session_destroy();
        success('退出成功', './index.php');
    }
}