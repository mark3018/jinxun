<?php

/*文章管理类*/

class artController extends CommonClass
{

    private $db;
    private $dbPath;

    function __construct()
    {
        $this->dbPath = './data/artData.php';
        $this->db = include $this->dbPath;
    }

    /*首页*/
    public function index()
    {
        include "./view/art/index.html";
    }

    /*新增*/
    public function add()
    {
        if (IS_POST) {
            $retrunData = array();
            //判断是否为空
            if (empty($title = $_POST['title']) || empty($content = $_POST['content'])) {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '标题或内容不能为空哈';
                ajaxReturn($retrunData);
            }
            //检测是否有重复名称
            foreach ($this->db as $v) {
                if ($v['title'] == $title) {
                    $retrunData['status'] = 0;
                    $retrunData['msg'] = '文章标题已存在了';
                    ajaxReturn($retrunData);
                }
            }
            //记录时间
            $_POST['time'] = time();
            //存入数据库中
            $this->db[] = $_POST;
            if (insert($this->db, $this->dbPath)) {
                $lastKey = '';
                //获取上一个id值
                foreach ($this->db as $k => $v) {
                    $lastKey = $k;
                }
                $retrunData['status'] = 'ok';
                $_POST['time'] = date('Y-m-d H:i:s');
                $_POST['Key'] = $lastKey;
                $retrunData['data'] = $_POST;
                ajaxReturn($retrunData);
            } else {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '新增失败';
                ajaxReturn($retrunData);
            };
        }
    }

    /*查看*/
    public function see()
    {
        //获取文章详情
        $aid = $_GET['aid'];
        $data = $this->db[$aid];
        include "./view/art/see.html";
    }


    /*编辑*/
    public function edit()
    {
        if (IS_POST) {
            $retrunData = array();
            //文章id
            $aid = $_POST['aid'];
            //删除键值
            unset($_POST['aid']);
            $_POST['time'] = time();
            $this->db[$aid] = $_POST;
            if (insert($this->db, $this->dbPath)) {
                $retrunData['status'] = 'ok';
                $_POST['time'] = date('Y-m-d H:i:s');
                $_POST['Key'] = $aid;
                $retrunData['data'] = $_POST;
                ajaxReturn($retrunData);

            } else {
                $retrunData['status'] = 0;
                $retrunData['msg'] = '编辑失败';
                ajaxReturn($retrunData);

            }
        }
        //获取文章详情
        $aid = $_GET['aid'];
        $data = $this->db[$aid];
        ajaxReturn($data);
    }

    /*删除*/
    public function del()
    {
        $retrunData = array();
        //获取文章id
        $aid = $_GET['aid'];
        //移除
        unset($this->db[$aid]);
        //重新放入数据
        if (insert($this->db, $this->dbPath)) {
            $retrunData['status'] = 'ok';
            $retrunData['msg'] = '删除成功';
            ajaxReturn($retrunData);
        } else {
            $retrunData['status'] = 0;
            $retrunData['msg'] = '删除失败';
            ajaxReturn($retrunData);
        }
    }


}