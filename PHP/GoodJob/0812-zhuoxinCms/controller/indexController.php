<?php

/*首页*/

class indexController extends CommonClass
{

    function __construct()
    {
        if (!isLogin()) {
            error('请登录', './index.php?c=user&a=login');
        }
    }

    /*首页*/
    public function index()
    {
        include "./view/index/index.html";
    }
}