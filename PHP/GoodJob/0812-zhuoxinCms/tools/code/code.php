<?php
/*验证码方法*/
$height = 60;
$codeLen = 4;
$fontSize = 30;
$width = $codeLen * ($fontSize + 10);
//创建画布
$img = imagecreatetruecolor($width, $height);
/*画布颜色*/
$color = imagecolorallocate($img, 255, 255, 255);
//填充
imagefill($img, $width, $height, $color);
/*生成验证码*/
$codeStr = "abcdefghijklmnpqrstuvwxyz123456789";
$codes = '';
for ($i = 0; $i < $codeLen; $i++) {
    //随机取出验证码
    $code = $codeStr[mt_rand(0, strlen($codeStr) - 1)];
    $codes .= $code;
    $color = imagecolorallocate($img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
    imagettftext($img, $fontSize, mt_rand(-50, 50), $width / 4 * $i + 20, 40, $color, "./tools/code/1.ttf", $code);
}
/*保存验证码到session...*/
/*杂点*/
for ($i = 0; $i < 800; $i++) {
    $color = imagecolorallocate($img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
    imagesetpixel($img, mt_rand(0, $width), mt_rand(0, $height), $color);
}

/*杂线*/
for ($i = 0; $i < 20; $i++) {
    $color = imagecolorallocate($img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
    imageline($img, mt_rand(0, 200), mt_rand(0, 200), mt_rand(0, 200), mt_rand(0, 200), $color);
}
header("Content-type:image/png");
//输出
imagepng($img);
//销毁
imagedestroy($img);