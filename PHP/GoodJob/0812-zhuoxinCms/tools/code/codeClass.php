<?php

/*验证码类*/

//保存验证码 自行开启
class codeClass
{
    public $width; //宽度
    public $height; //高度
    public $codeLen; //验证码长度
    public $fontSize; //字体大小

    /**
     * code constructor
     * @param int $codeLen 验证码长度   默认4位
     * @param int $fontSize 验证码字体大小
     * @param int $height 画布高度     默认60
     */
    public function __construct($codeLen = 4, $fontSize = 30, $height = 60)
    {
        $this->width = $codeLen * ($fontSize + 10);
        $this->height = $height;
        $this->codeLen = $codeLen;
        $this->fontSize = $fontSize;
    }

    //获取验证码
    public function code()
    {
        $this->createCode();
    }


    //创建验证码
    public function createCode()
    {
        //创建画布
        $img = imagecreatetruecolor($this->width, $this->height);
        /*画布颜色*/
        $color = imagecolorallocate($img, 255, 255, 255);
        //填充
        imagefill($img, $this->width, $this->height, $color);
        /*生成验证码*/
        $codeStr = "abcdefghijklmnpqrstuvwxyz123456789";
        $codes = '';
        for ($i = 0; $i < $this->codeLen; $i++) {
            //随机取出验证码
            $code = $codeStr[mt_rand(0, strlen($codeStr) - 1)];
            $codes .= $code;
            $color = imagecolorallocate($img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            imagettftext($img, $this->fontSize, mt_rand(-50, 50), $this->width / 4 * $i + 20, 40, $color, "./tools/code/1.ttf", $code);
        }
        /*保存验证码到session...*/
        $_SESSION['code'] = $codes;
        /*杂点*/
        for ($i = 0; $i < 800; $i++) {
            $color = imagecolorallocate($img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            imagesetpixel($img, mt_rand(0, $this->width), mt_rand(0, $this->height), $color);
        }

        /*杂线*/
        for ($i = 0; $i < 20; $i++) {
            $color = imagecolorallocate($img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            imageline($img, mt_rand(0, 200), mt_rand(0, 200), mt_rand(0, 200), mt_rand(0, 200), $color);
        }
        header("Content-type:image/png");
        //输出
        imagepng($img);
        //销毁
        imagedestroy($img);
    }
}