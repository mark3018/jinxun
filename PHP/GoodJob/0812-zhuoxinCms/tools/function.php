<?php
/*函数库*/
/*自动加载类 类统一放入class文件夹下*/
function __autoload($className)
{
    include("./controller/$className.php");
}

//开启session
session_start();
//定义IS_POST常量
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
define('IS_POST', REQUEST_METHOD == 'POST' ? true : false);


/*验证是否登录*/
function isLogin()
{
    if (!empty($_SESSION['user'])) {
        return true;
    } else {
        return false;
    }
}

/**
 * 错误提示 返回上一页
 * @param string $message
 */
function error($message = '操作有误哦', $prevPage = '')
{
    if (empty($prevPage)) {
        $prevPage = $_SERVER['HTTP_REFERER'];
    }
    echo <<<TPL
    <script>
      alert("{$message}");
      location.href = "{$prevPage}";
    </script>
TPL;
    exit();
}

/**
 * 成功提示 返回上一页
 * @param string $message
 * @param string $prevPage
 */

function success($message = '操作成功哦', $prevPage = '')
{
    if (empty($prevPage)) {
        $prevPage = $_SERVER['HTTP_REFERER'];
    }
    echo <<<TPL
    <script>
      alert("{$message}");
      location.href = "{$prevPage}";
    </script>
TPL;
    exit();
}

/*返回json对象 用于ajax获取数据*/
function ajaxReturn($data)
{
    echo json_encode($data);
    exit();
}

/*将数据存入数据库*/
function insert($data, $filePath)
{
    $data = var_export($data, 1);
    $data = "<?php \n\r return " . $data . "?>";
    return file_put_contents($filePath, $data);
}


