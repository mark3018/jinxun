<?php

include "../functions.php";

/**
 * 重复匹配
 */

//$str = '1234b';
//使用原子只匹配字符串当中一个字符
//$preg = '/\d/';

//字符串当中每个字符都进行匹配了
//$preg = '/\d{5}/';

//$str = 'a';
//$preg = '/\w??/';
//p(preg_match($preg,$str));


/**
 * 匹配字符边界
 */
$str = 'zx1024.com';
// ^z代表以z开头  m$代表以m结束
$preg = '/^z.{8}?m$/';
//p(preg_match($preg,$str));


/**
 * 手机号验证
 */
$phone = '13866888866';
$preg = '/^1(39|38|35|88|98)\d{8}$/';
p(preg_match($preg,$phone));