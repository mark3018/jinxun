<?php

include "../functions.php";

/**
 * 模式修正符
 */


//i 不区分大小写
$str = 'abc';
$preg = '/ABC/i';
//p(preg_match($preg,$str));


//s 将字符串视为单行，换行符做普通字符看待，是“.”匹配任何字符
$str = '<div>
Hello</div><div>
World</div>
';
$preg = '/<div>.*?<\/div>/s';
//p(preg_match($preg,$str));

//e 5.5版本以后废弃了  我们需要使用preg_replace_callback将字符串作为表达式使用
$str = '密码为：admin888';
//$preg = '/admin888/e';
//p(preg_replace($preg,"md5('admin888')",$str));
//p(preg_match($preg,$str));

$preg = '/admin888/';
$newStr = preg_replace_callback($preg,'func',$str);
function func($v){
    return md5($v[0]);
}
p($newStr);
