<?php

include '../functions.php';
include "./02.resetArr.php";
/**
 * 多文件上传
 */

if(IS_POST){
    $count = manyUp();
    if($count != 0){
        p('已上传' . $count . '张图片');
    }else{
        p('上传失败');
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>多文件上传</title>
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        文件1：<input type="file" name="up[]" >
        <br>
        文件2：<input type="file" name="up[]" >
        <br>
        文件3：<input type="file" name="up[]" >
        <br>
        <input type="submit" value="提交">
    </form>
</body>
</html>
