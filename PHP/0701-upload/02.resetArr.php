<?php


/**
 * 重组数组
 */
function resetArr(){
    //1.获取$_FILES中up的值
    $values = $_FILES['up'];

    //3.需要数组 所以来一个空数组
    $arr = [];
    //5.获取数组数量(总数),跟文件数量无关,相当于获取存在几个上传框
    $count = count($values['name']);//3

    //2.重组数组
    foreach($values['name'] as $k=>$v){
        //6.获得实际文件的数量
       if(strlen($v) == 0){
           $count -- ;
       }
        //4.数组的追加
        $arr['up'][] = [
            'name'=> $v,
            'type'=> $values['type'][$k],
            'tmp_name'=> $values['tmp_name'][$k],
            'error'=> $values['error'][$k],
            'size'=> $values['size'][$k],
        ];
    }
   //7.获得上传文件的数量
    $arr['count'] = $count;
    return $arr;
}

/**
 * 多文件上传
 */
function manyUp(){
    $res = resetArr();

//    作业。。。完成上传

    return $res['count'];
}