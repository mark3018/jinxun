<?php

include '../functions.php';
/**
 * 单文件上传
 */

if(IS_POST){
//    p($_FILES);
    $img = $_FILES['up']['tmp_name'];
    //1.判断是否是一个合法的上传文件
    if(is_uploaded_file($img)){
        //3.组一个上传到哪儿去的路径
        $dir = 'images/' . date('Ymd') . '/';
        is_dir($dir) || mkdir($dir,0777,true);
        //获取文件类型
        // $fileName = zhangmin.jpg
        $fileName = $_FILES['up']['name'];
            //方式1
            //$type = pathinfo($fileName)['extension'];
            //方式2
            $type = strtolower(ltrim(strrchr($fileName,'.'),'.'));

        //到这里我们有 images/20180701/    jpg;
        //组一个完整的路径 也就是给文件加上文件名称
            //路径处理方式1 获取了原图片的名称进行处理
            $name = str_replace(strrchr($fileName,'.'),'',$fileName);
            $dest = $dir . 'small_' . $name . '.' .$type;
            //路径处理方式2 相当于自定义文件名称
            $dest = $dir . 'jinxun_' . mt_rand(0,9999) . '.' . $type;

        //2.移动上传
        $res = move_uploaded_file($img,$dest);
        if($res){
            p('上传成功');
        }else{
            p('上传失败');
        }
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        上传：<input type="file" name="up" >
        <br>
        <input type="submit" value="提交">
    </form>
</body>
</html>
