<?php


/**
 * 1.重组数组
 */
function resetArr(){
    //1.获取$_FILES中up的值
    $values = $_FILES['up'];

    //3.需要数组 所以来一个空数组
    $arr = [];
    //5.获取数组数量(总数),跟文件数量无关,相当于获取存在几个上传框
    $count = count($values['name']);//3

    //2.重组数组
    foreach($values['name'] as $k=>$v){
        //6.获得实际文件的数量
       if(strlen($v) == 0){
           $count -- ;
       }
        //4.数组的追加
        $arr['up'][] = [
            'name'=> $v,
            'type'=> $values['type'][$k],
            'tmp_name'=> $values['tmp_name'][$k],
            'error'=> $values['error'][$k],
            'size'=> $values['size'][$k],
        ];
    }
   //7.获得上传文件的数量
    $arr['count'] = $count;
    return $arr;
}

/**
 * 2.多文件上传
 */
function manyUp(){
    //1.重组数组后获得的新数组
    $resetArr = resetArr();
    //2.遍历 准备要进行上传工作
    foreach($resetArr['up'] as $k=>$v){
       //3.判断是否是一个合法的上传文件
        if(is_uploaded_file($v['tmp_name'])){
            //5.组一个上传到哪儿去的路径
                //创建目录
                $dir = 'images/' . date('Ymd') . '/';
                is_dir($dir) || mkdir($dir,0777,true);
                //获取文件类型
                $type = pathinfo($v['name'])['extension'];
                //组一个完整的路径
                $dest = $dir . 'jinxun_' . mt_rand(0,9999) . '.' . $type;
            //4.移动上传
            move_uploaded_file($v['tmp_name'],$dest);
        }
    }
    //返回一个上传文件的数量给前台页面 用于用户提示
    return $resetArr['count'];
}