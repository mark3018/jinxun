<?php

/**
 * 函数库
 */
//头部
header('Content-type:text/html;charset=utf-8');


//p打印函数
function p($con){
    echo "<pre style='padding: 15px;background: #2E3E4E;border-radius: 46px;color: white;font-size: 18px;font-weight: 700'>";
    if(is_null($con)){
        var_dump($con);
    }elseif (is_bool($con)){
        var_dump($con);
    }else{
        print_r($con);
    }
    echo "</pre>";
}

//设定时区
date_default_timezone_set('PRC');


/**
 * 判断是否为post提交
 */
define('IS_POST',($_SERVER['REQUEST_METHOD'] == 'POST') ? TRUE : FALSE);

/**
 * 成功函数
 * @param $msg 提示信息
 * @param $url 跳转地址
 */
function success($msg,$url){
    $str = <<<str
<script>
alert("$msg");
location.href = "$url";
</script>
str;
    echo $str;die;
}

/**
 * 失败函数
 * @param $msg 提示信息
 */
function error($msg){
    $str = <<<str
<script>
alert("$msg");
history.back();
</script>
str;
    echo $str;die;
}

/**
 * 写入数据
 * @param $data 数据
 * @param $filePath 文件路径
 */
function putData($data,$filePath){
    //合法化PHP代码
    $php = var_export($data,true);
    //写入文件
    file_put_contents($filePath,"<?php\r\nreturn {$php} ?>");
}


/**
 * 目录剪切函数方法。
 */
/**
 * @param $source 要剪切谁
 * @param $dest 剪切到哪里去
 */
function cut($source,$dest){
    if(cp($source,$dest) && del($source)){
        p('剪切成功');
    }else{
        p('剪切失败');
    }
}
//复制目录
function cp($source,$dest){
//    p($source);
//    p($dest);
    //1.判断源是否是一个目录
    if(!is_dir($source)) return false;
    //2.创建目标目录
    is_dir($dest) || mkdir($dest,0777,true);
    //3.进行遍历源目录
    foreach(glob($source . '/*') as $v){
        $newDest = $dest . '/' . basename($v);
        is_dir($v) ? cp($v,$newDest) : copy($v,$newDest);
    }
    return true;
}
//删除
function del($source){
    if(!is_dir($source)) return false;
    foreach(glob($source . '/*') as $v){
        is_dir($v) ? del($v) : unlink($v);
    }
    return rmdir($source);
}


/**
 * 找见未找见的类
 */
function __autoload($className){
    include "./{$className}.class.php";
}

