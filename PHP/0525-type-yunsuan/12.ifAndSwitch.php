<?php


//if else 比 switch使用更频繁

//当判断某种范围的时候我们更建议if语句
//当判断某个值得时候我们更建议用switch

//switch适合处理判断分支较多的情况下(代码可读性好)，而if适合处理判断分支较少的情况下(反之代码可读性差，容易出现漏判或重复判断)