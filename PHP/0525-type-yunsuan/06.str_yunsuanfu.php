<?php
/**
 * 链接运算符
 */
$a = 'www';
$b = '.xiongweiyang.com';
$url = $a . $b;
//echo $url;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <table border="1">
        <tr>
            <td>姓名：</td>
            <td>班级：</td>
        </tr>
        <tr>
            <td>张正华</td>
            <td>金讯PHP</td>
        </tr>
    </table>
    <hr>
    以下是PHP代码效果:
</body>
</html>

<?php
//连接赋值运算符
$str = '<table border="1">';
$str .= '<tr><td>姓名：</td><td>班级：</td></tr>';
$str .= '<tr><td>张正华</td><td>金讯PHP</td></tr>';
$str .= '</table>';
echo $str;
?>
