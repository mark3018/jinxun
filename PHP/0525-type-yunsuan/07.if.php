<?php
/**
 * 选择结构
 */

//单路分支
//if(3<2){
//    echo '你答对了~';
//}


//双路分支
//if(3>2){
//    echo 'ok';
//}else{
//    echo 'no';
//}

$a = 0;
if($a){
    echo "{$a}";
}else if($a == 1){
    echo $a;
}else{
    echo "{$a}是神马";
}
