<?php

/**
 * 比较运算符
 */
$a = 3;
$b = '3';
//比较值 不能比较类型
//if($a == $b){
//    echo 'yes';
//}else{
//    echo 'no';
//}


//既比较数值又比较类型
//if($a === $b){
//    echo 'yes';
//}else{
//    echo 'no';
//}

/**
 * 不等
 */
//只判断数值 不判断类型
//if($a != $b){
//    echo 'yes';
//}else{
//    echo 'no';
//}


if($a <> $b){
    echo 'yes';
}else{
    echo 'no';
}


//判断值与类型
//if($a !== $b){
//    echo 'yes';
//}else{
//    echo 'no';
//}