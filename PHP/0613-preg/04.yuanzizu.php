<?php

include "../functions.php";

/**
 * 原子组
 * 原子组一次性可以匹配多个字符
 * 而原子表或者原子一次只匹配一个字符
 */

$str = 'abcdefg';
//$preg = '/[a-d]/';//意味着abcd

$preg = '/(abc)/';
p(preg_match($preg,$str));