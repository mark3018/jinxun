<?php

/**
 * 文章管理控制器
 */
class ArcController extends CommonController
{
    private $db;
    public function __construct()
    {
        //为了不覆盖父类当中的构造方法
        parent::__construct();
        $this->db = include "./Databases/arc.php";
    }

    //首页
    public function index()
    {
        include "./View/Arc/index.html";
    }

    //添加
    public function add(){
        if(IS_POST){
            $_POST['time'] = time();
            $this->db[] = $_POST;
            $this->putData($this->db,'./Databases/arc.php');
            $this->success('添加成功','index.php?c=arc');
        }
        include "./View/Arc/add.html";
    }

    //显示文章内容
    public function show(){
        $aid = $_GET['aid'];
        $oldData = $this->db[$aid];
        include "./View/Arc/show.html";
    }

    //编辑
    public function edit(){
        $aid = $_GET['aid'];
        $oldData = $this->db[$aid];
        if(IS_POST){
            $_POST['time'] = $oldData['time'];
            $this->db[$aid] = $_POST;
            $this->putData($this->db,'./Databases/Arc.php');
            $this->success('编辑成功','index.php?c=arc');
        }
        include "./View/Arc/edit.html";
    }

    //删除
    public function del(){
        $aid = $_GET['aid'];
        unset($this->db[$aid]);
        $this->putData($this->db,'./Databases/Arc.php');
        $this->success('删除成功','index.php?c=arc');
    }
}