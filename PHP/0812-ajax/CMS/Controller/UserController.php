<?php

/**
 * 用户管理控制器
 */
class UserController extends CommonController
{
    //数据表
    private $db;

    public function __construct()
    {
        //加载user数据表
        $this->db = include "Databases/user.php";
    }

    //验证码
    public function code(){
        include "./Tools/Code.php";
        $code = new Code();
        $code -> make();
    }

    //登录
    public function login()
    {
        if (IS_POST) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            //请输入用户名或密码
            if (empty($username) || empty($password)) {
                $this->error('请输入用户名或密码');
            }

            //验证码的验证
            if($_POST['code'] !== $_SESSION['code']){
                $this->error('验证码错误！！！');
            }

            //表单提交的数据和数据库进行对比
            foreach ($this->db as $k => $v) {
                if ($username == $v['username'] && md5($password) == $v['password']) {
                    //七天免登陆
                    if (isset($_POST['auto'])) {
                        setcookie(session_name(), session_id(), time() + 7 * 24 * 3600, '/');
                    } else {
                        setcookie(session_name(), session_id(), 0, '/');
                    }

                    //登陆成功之前 将用户id和用户名存入session
                    //为什么来一个['admin'],这是session中专门存用户信息的
                    //以后可能session当中还会存购物车信息。
                    $_SESSION['admin']['uid'] = $k;
                    $_SESSION['admin']['username'] = $v['username'];

                    $this->success('登录成功', 'index.php?c=arc&a=index');
                }
            }
            $this->error('用户名或密码错误');
        }
        include "./View/User/login.html";
    }

    //注册
    public function register()
    {
        if (IS_POST) {
            $_POST['time'] = time();
            $username = $_POST['username'];
            $password = $_POST['password'];
            $confirm = $_POST['confirm'];

            //后台验证是否填写表单
            if (empty($username) || empty($password) || empty($confirm)) {
                $this->error('请填写完整表单');
            }

            //两次密码是否一致
            if ($password !== $confirm) {
                $this->error('两次密码不一致，请重新输入');
            }

            //验证用户是否已经注册
            foreach ($this->db as $v) {
                if ($username == $v['username']) {
                    $this->error('用户名已存在');
                }
            }

            //删除确认密码
            unset($_POST['confirm']);

            //加密
            $_POST['password'] = md5($password);

            //数据追加
            $this->db[] = $_POST;

            //压入数据
            $this->putData($this->db, './Databases/user.php');

            //提示信息
            $this->success('注册成功', 'index.php?c=user&a=login');
        }
        include "./View/User/register.html";
    }

    //退出
    public function out()
    {
        session_unset();
        session_destroy();
        $this->success('退出成功','index.php');
    }
}