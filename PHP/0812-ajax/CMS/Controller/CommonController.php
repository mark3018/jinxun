<?php

/**
 * 公共控制器  给其他控制器放公用的方法 相当于父类  其他控制器相当于子类 子类需要继承父类
 */
class CommonController
{
    public function __construct()
    {
        //判断是否登录
        if(!isset($_SESSION['admin'])){
            $this->success('请先登陆','index.php?c=user&a=login');
        }
    }

    /**
     * 成功方法
     * @param $msg 提示信息
     * @param $url 跳转地址
     */
    protected function success($msg, $url)
    {
        $str = <<<str
<script>
alert("$msg");
location.href = "$url";
</script>
str;
        echo $str;
        die;
    }

    /**
     * 失败方法
     * @param $msg 提示信息
     */
    protected function error($msg)
    {
        $str = <<<str
<script>
alert("$msg");
history.back();
</script>
str;
        echo $str;
        die;
    }

    /**
     * 压入数据
     */
    public function putData($data, $file)
    {
        //合法化代码
        $str = var_export($data, true);
        //压入数据库
        file_put_contents($file, "<?php\r\n return {$str} ?>");
    }
}