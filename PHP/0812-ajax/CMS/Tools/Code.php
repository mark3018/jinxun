<?php

/**
 * 验证码类
 */
class Code
{
    public $img;
    public $width;
    public $height;
    public $color;
    public $codeNum;
    public $size;
    public $seed;
    public $textCode;

    public function __construct($codeNum = null,$width = null, $height = null, $color = null,  $size = null, $seed = null)
    {
        $this->width = is_null($width) ? 200 : $width;
        $this->height = is_null($height) ? 100 : $height;
        $this->color = is_null($color) ? '#3CB9EC' : $color;
        $this->codeNum = is_null($codeNum) ? 4 : $codeNum;
        $this->size = is_null($size) ? 40 : $size;
        $this->seed = is_null($seed) ? 'zxcvbnmasdfghjklqwertyuiop1234567890' : $seed;
    }

    /**
     * 总方法
     */
    public function make()
    {
        //发送头部
        header('Content-Type:image/png');
        //创建画布
        $this->createBg();
        //写字
        $this->text();
        //制造干扰
        $this->makeTrouble();
        //输出图片
        imagepng($this->img);
        //销毁
        imagedestroy($this->img);
    }

    /**
     * 创建画布
     */
    private function createBg()
    {
        $this->img = imagecreatetruecolor($this->width, $this->height);
        //填充颜色
        //将十六进制颜色转为十进制
        $color = hexdec($this->color);
        imagefill($this->img, 0, 0, $color);
    }

    /**
     * 写字
     */
    private function text()
    {
        for ($i = 0; $i < $this->codeNum; $i++) {
            $x = ($this->width / $this->codeNum) * $i + 10;
            $y = ($this->height+$this->size)/2;
            $color = imagecolorallocate($this->img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            $text = $this->seed[mt_rand(0, strlen($this->seed) - 1)];
            $this->textCode .= $text;
            imagettftext($this->img, $this->size, mt_rand(-45, 45), $x, $y, $color, './Tools/simsun.ttc', $text);
        }
        //将验证码存入session当中
        //会有个视觉bug 打印出来的验证码比验证码图片慢一拍
        //原因：页面加载login.php时候，图片的加载跟其他标签加载时异步的，所以其他标签信息先加载，之后才会加载src图片，因为标签加载顺序不同步，验证码也不同步。
        $_SESSION['code'] = $this->textCode;
    }

    /**
     * 制造干扰
     */
    private function makeTrouble()
    {
        //画点
        for($i=0;$i<1000;$i++){
            $color = imagecolorallocate($this->img,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
            imagesetpixel($this->img,mt_rand(0,$this->width),mt_rand(0,$this->height),$color);
        }
        //划线
        for($i=0;$i<10;$i++){
            $color = imagecolorallocate($this->img,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
            imageline($this->img,mt_rand(0,$this->width),mt_rand(0,$this->height),mt_rand(0,$this->width),mt_rand(0,$this->height),$color);
        }
    }
}