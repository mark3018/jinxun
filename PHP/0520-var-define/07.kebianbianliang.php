<?php


$a = 'hello';//普通变量
$$a = 'world';//可变变量 相当于 $hello = world

//双引号可以解析变量 单引号不可以 花括号进行变量的分离
echo "$a ${$a}";//hello world
echo "<hr/>";
echo "$a $$a";//hello $hello 整体是一个字符串


echo "<hr/>";
//花括号进行变量分离、
echo "{$a}bc";//hellobc