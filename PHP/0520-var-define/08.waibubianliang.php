<?php
//后台代码
//post提交方式 隐式方式  比较安全
echo '<pre>';
//var_dump($_POST);

//地址栏提交  不太安全
//var_dump($_GET);

//可接受get或者post提交过来的数据
var_dump($_REQUEST);
echo '</pre>';

?>

<!--前台代码 给用户看到的-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
        <!-- 提交地址   提交方式-->
<form action=""    method="get">
        用户名：<input type="text" name="username">
        <br>
        密码:<input type="password" name="password">
        <br>
        <input type="submit" value="提交">
    </form>
</body>
</html>
