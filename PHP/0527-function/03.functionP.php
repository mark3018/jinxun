<?php

/**
 * 打印函数
 */
//之前接触的打印函数 echo / var_dump
//$a = 1;
//$b = 2;
//print_r($b);

/**
 * 不完整文档p打印函数
 * @param $con
 */
//function p($con){
//    echo "<pre style='padding: 15px;background: #2E3E4E;border-radius: 46px;color: white;font-size: 18px;font-weight: 700'>";
//    print_r($con);
//    echo "</pre>";
//}


//p(TRUE); //1
//$a = null;
//p($a); //无结果



/**
 * 完整的p打印函数
 */
function p($con){
    echo "<pre style='padding: 15px;background: #2E3E4E;border-radius: 46px;color: white;font-size: 18px;font-weight: 700'>";
    if(is_null($con)){
        var_dump($con);
    }elseif (is_bool($con)){
        var_dump($con);
    }else{
        print_r($con);
    }
    echo "</pre>";
}

//p(TRUE);
$a = null;
p($a);