<?php

//echo '哈哈';
//echo '<br>';
//echo '拉阿拉';
//echo '<br>';
//echo '123';

/**
 * 简单的函数
 */
//声明一个函数
//function p(){
//    //函数的代码提
//    echo 1;
//}
//函数的调用
//p();


/**
 * 带参数的函数
 */
        //参数 形参
//function p($var){
//    echo $var;
//}
//
////123 称为实参
//p(123);
//p('<br>');
//p('哈哈');


/**
 * 两个参数
 */
//function p($var1,$var2){
//    echo $var1 + $var2;
//}
//p(1,3);


/**
 * 默认值
 */
//function p($var1,$var2 = 10){
//    echo $var1 + $var2;
//}
////p(1,20);
//p(1);

//有默认值的参数 一定不能排在没默认值参数的前边
//function p($var1 = 20,$var2){
//    echo $var1 + $var2;
//}
//p(100);


/**
 * 返回值 需要变量来接收  或者直接打印
 */
//function p($var1,$var2){
//    //返回值
//    return $var1 + $var2;
//}
//$res = p(1,5);
//echo $res;

//当函数没有return的时候 返回值是一个NULL;
//function p($var1,$var2){
//    $var1 . '|' . $var2;
//}
//$res = p(1,3);
//var_dump($res);


/**
 * 函数的检测
 */
function p(){
    echo '我爱熊老师';
}
$bool = function_exists('p');
var_dump($bool);