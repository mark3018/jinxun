<?php

/**
 * 局部变量 函数体内的变量
 */
//function a(){
//    $var = '我爱熊老师';
//    echo $var;
//}
//a();
//echo $var;


/**
 * 全局变量
 */
//不太推荐
//$var = '我爱熊老师';
//function a(){
//    global $var;//设置全局
//    echo $var;
//}
//a();

//不太推荐
//function a(){
//    $GLOBALS['var'] = '我爱熊老师';
////    echo $GLOBALS['var'];
//}
//a();
//echo $GLOBALS['var'];

//比较推荐
//$var = '我爱熊老师';
//function a(&$var){
//    echo $var;
//}
//a($var);


//最为推荐  返回值的方式
$var1 = '我爱熊老师';
function a(&$var2){;
    return $var2;
}
$res = a($var1);
echo $res;