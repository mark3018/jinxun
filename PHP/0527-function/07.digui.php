<?php
//3的阶乘   3*2*1
function p($con){
    echo "<pre style='padding: 15px;background: #2E3E4E;border-radius: 46px;color: white;font-size: 18px;font-weight: 700'>";
    if(is_null($con)){
        var_dump($con);
    }elseif (is_bool($con)){
        var_dump($con);
    }else{
        print_r($con);
    }
    echo "</pre>";
}

/**
 * 递归 在不确定要执行多少次的时候用上递归 去进行自己调用自己
 * 阶乘的例子： 当参数大于1的时候 会一直调用自己这个函数 一直去走函数体
 * 直到else的情况，也就是参数不大于1了 我就执行完一遍函数体就返回出结构了
 * @param $num
 * @return mixed
 */
function facto($num){
    if($num>1){
        $res = $num * facto($num-1);
    }else{
        $res = $num;
    }
    return $res;
}

$res = facto(5);
p($res);

//3的阶乘
//第一次调用  facto(3);
//走进代码提               $res = 3 * facto(2); //3*2
//走这个参数 facto(2)      $res = 2 * facto(1); //2*1

//3*2*1

//5的阶乘


