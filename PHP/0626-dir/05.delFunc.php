<?php

include "../functions.php";

/**
 * 删除一个非空目录的函数
 * @param $path
 * @return bool
 */
function remove($path){
    if(!is_dir($path)) return false;
    foreach(glob($path . '/*') as $v){
//        p($v);
//        if(is_dir($v)){
//            remove($v);
//        }else{
//            unlink($v);
//        }
        is_dir($v) ? remove($v) : unlink($v);
    }
    return rmdir($path);
}

$path = './test';
$bool = remove($path);
if($bool){
    p('删除成功');
}else{
    p('删除失败');
}