<?php

include "../functions.php";

//磁盘总空间
$space = disk_total_space('f:');
$space = $space / pow(1024,3);
$space = round($space,2) . 'G';

//磁盘剩余空间
$space = disk_free_space('f:');
$space = $space / pow(1024,3);
$space = round($space,2) . 'G';


//测试给定字节大写转为单位
//根据测试给定字节的范围给上具体单位
$size = 67891234;
switch(true){
    case $size>=pow(1024,3):
        $units = [3,'G'];
        break;
    case $size>=pow(1024,2):
        $units = [2,'MB'];
    break;
    case $size>=pow(1024,1):
        $units = [1,'KB'];
    break;
    default:
        $units = [0,'B'];
        break;
}

$space = $size / pow(1024,$units[0]);
$space = round($space,2) . $units[1];
p($space);



