<?php

include "../functions.php";

//返回路径最后一部分
$file = './a/b/c';
$file = './a/b/c.php';
//p(basename($file));


//返回除去路径最后一部分的部分
//p(dirname($file));


//检测文件是否存在
//p(file_exists($file));

//目录是否存在
//$file = './b';
//p(is_dir($file));

//创建目录 参数二最高权限，参数三 递归创建
//p(mkdir($file,0777,true));

//移除目录 目录必须为空
//p(rmdir('./a/b'));


//重命名 文件或者文件夹都可以
//p(rename('./a','./aa'));


//拷贝文件 并且修改了文件名称  目录不可以拷贝
//p(copy('./01.disk.php','./aa/disk.php'));


//遍历出该目录下所有文件的路径
//p(glob('../*'));


//删除文件
//p(unlink('./aa/disk.php'));