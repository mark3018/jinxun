<?php

include '../functions.php';


//ceil 天花板  向上取整
//$num = '1.1';
//$num = '5.9';
//p(ceil($num));

//地板 向下取整
//p(floor($num));


//取最大值
//p(max(1,4,6,100));


//取最小值
//p(min(1,4,6,100));


//四舍五入
//p(round($num));


//随机数 若不传入参数 范围 1-getrandmax  也就是1-2147483647
//p(mt_rand());
p(getrandmax());

//p(mt_rand(1,3));

//p(mt_rand(0,1));

//if(mt_rand(1,10)>2){
//    echo "<script>alert('老婆洗碗')</script>";
//}else{
//    echo "<script>alert('老公洗碗')</script>";
//}


//指数
p(pow(3,4));