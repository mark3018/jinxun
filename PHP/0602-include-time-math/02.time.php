<?php

include '../functions.php';

//获取当前时区
//默认为UTC 协调世界时  世界统一时间、世界标准时间
//p(date_default_timezone_get());

//设置时区 设置为北京时间 差UTC八个小时
//date_default_timezone_set('PRC');
//p(date_default_timezone_get());

//时间戳  1970-1-1至当前时间的差
//p(time());

//转为年月日 时分秒的方式 格式化时间戳
//p(date('Y-m-d H:i:s',time()));

//打印结果 微秒数和时间戳
//p(microtime());

//浮点型  更精确！
//p(microtime(true));


//获取今天00:00的时间
//p(date('Y-m-d'));
//$startTime = strtotime(date('Y-m-d'));
//p(date('Y-m-d H:i:s',$startTime));


//获取明天00:00的时间
//$startTime = strtotime(date('Y-m-d'));
//$finalTime = $startTime + 24*60*60;
//p(date('Y-m-d H:i:s',$finalTime));

//获取准确的时间信息
//p(getdate());
p(getdate(214254325));