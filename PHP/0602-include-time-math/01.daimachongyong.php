<?php


/**
 * 将文件导入  就相当于把functions.php的代码粘贴到了该文件的上边
 */

//   ../代表往上找一层
//警告性的错误  不影响后面执行的代码 当此路径错误时  作为一个警告 不会影响下面的代码体
//include '../functions1.php';

//致命性的错误 不执行后面的代码了  当此路径错误时  致命性错误 不会执行下面的代码了
//require '../functions1.php';


//如果用include导入两次文件 相当于复制粘贴了两个p函数 重复定义 那么会报错
//如果第二次导入时用的是include_once 以避免函数的重复定义 变量被覆盖
include '../functions.php';
include_once "../functions.php";
include_once "../functions.php";

require_once "../functions.php";
p(1);