<?php

/**
 * 用户管理控制器
 */
class UserController{
    //数据表
    private $db;
    public function __construct()
    {
        //加载user数据表
        $this->db = include "Databases/user.php";
    }

    //登录
    public function login(){
        include "./View/User/login.html";
    }

    //注册
    public function register(){
        p($this->db);
        include "./View/User/register.html";
    }
}