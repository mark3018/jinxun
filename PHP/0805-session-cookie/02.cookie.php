<?php
include "../functions.php";

date_default_timezone_set('PRC');
//什么是cookie：本地存的小文件即为cookie，多个页面都可以去读取这个小文件，也就做到了变量共享。还是比较安全的，各个浏览器会进行加密，不用担心。
//什么是cookie传递：就是服务器将信息保存到本地，然后看你是否需要保存cookie。


//设置一个cookie
//默认是会话时间 会话时间就是浏览器一开一关 会话开始就是打开网页，会员结束就是关闭网页 cookie存在哪 浏览器加密存起来了
//setcookie('hi','Mr.Xiong');

//设置第三个参数 一定时间戳 如果不设置就是默认会话时间
//setcookie('hi','Mr.Xiong',time()+24*3600);

//设置第四个参数 设置作用路径  /代表www根目录
//setcookie('hi','Mr.Xiong',time()+24*3600,'/');


$goods = [
    'IphoneX'=>[
        'size'=>'128G',
        'price'=>'9688Rmb',
        'color'=>'black',
    ],
    'Mac'=>[
        'size'=>'256G',
        'price'=>'9888Rmb',
        'color'=>'white',
    ]
];
//序列化 第二个参数不能是数组
$goods = serialize($goods);
setcookie('goods',$goods);
p($_COOKIE);