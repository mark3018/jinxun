<?php

/**
 * 1.cookie保存在客户的浏览器中，session保存在服务器中。
 * session默认存在服务器的一个文件里(不是内存)，
 * Mac Mamp 文件放置于Applications/MAMP/tmp/php,
 * Windows PhpStudy   放置于 F:\phpStudy\tmp\tmp
 *
 * 2.cookie 不是很安全 ，别人可以分析存放在本地的 cookie 并且进行诈骗，考虑到安全我们会使用 session。
 *
 * 3.session 会在一定时间内保存在服务器中。当访问过多时，会占用服务器的性能，考虑到要减轻服务器的性能方便的问题，我们就用 cookie。
 *
 * 4.单个 cookie 保存的数据不超过4k,很多浏览器都限制了站点，FireFox允许每个域保存50个cookie。
 *
 * 5.总结：
 * session 是服务端保存的一个数据结构，用来跟棕用户的状态，这个数据可以保存在集群、数据库、文件中。
 * cookie 是客户端保存用户信息的一种机制，用来记录用户的一些信息，也是实现 session 的一种方式。
 *
 * 6.熊老师建议：
 * 将登陆信息等重要信息存放在 session;
 * 其他信息如果需要保留，放在 cookie 中。
 */