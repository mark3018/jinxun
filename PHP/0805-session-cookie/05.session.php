<?php
include "../functions.php";
//开启
session_start();

$_SESSION['uid'] = 2;
$_SESSION['username'] = 'jack';

//删除session变量
//unset($_SESSION['uid']);

//删除所有session变量，不删出session文件。
//$_SESSION = [];

//同时使用
//删除session变量信息
session_unset();
//删除文件
session_destroy();

p($_SESSION);