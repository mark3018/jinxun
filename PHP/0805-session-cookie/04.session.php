<?php

include "../functions.php";

//session 保存在服务端的 存重要的信息


//sessionId相当于一个身份证号
//1.我们每一个用户都会得到唯一的ID的文件仓库，这个文件是保存在服务器的，是独一无二，每个用户互不干扰。
//2.我们每一个用户都会有一把唯一的ID(钥匙),来找到并且打开那个文件仓库，取得数据。
//3.如果Id(钥匙)丢失了，那么数据也就没了。
//sessionId会保存在客户端和服务端
//因为第一次进来是读本地的cookie,然后获得session名字以后再去找session文件，这个文件里保存的是变量

//更改session配置，在开启之前更改
session_name('hi');
session_id('10');
//开启
session_start();

$_SESSION['uid'] = 1;
$_SESSION['username'] = 'admin';

p(session_name());
p(session_id());
p($_SESSION);