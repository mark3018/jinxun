<?php

include '../functions.php';

/**
 * 1.创建计算脚本时间的函数；
 */

//get_runtime('start');
//for($i=0;$i<=10000;$i++){
//    echo $i . '<br>';
//}
//get_runtime('end');

//逻辑：for循环开始前我存一个时间 然后执行for循环 执行完后再存一个时间  拿最后的时间去减去开始的时间 获得脚本运行的时间

function get_runtime($action){
    static $time;
    if ($action == 'start'){
        $time = microtime(true);//最开始的时间
    }else if($action == 'end'){
        return microtime(true) - $time;
    }else{
        die('请输入正确的口令，例如start或者end');
    }
}

get_runtime('start');
for($i=0;$i<=10000;$i++){
    echo $i . '<br>';
}
$cha = get_runtime('end');
p($cha);