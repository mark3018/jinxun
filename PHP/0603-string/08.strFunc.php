<?php

include "../functions.php";

$str = 'iamsingerboy';
$str = '我是中文汉字';
//p($str);

//从下标为几开始截取几个
//$newStr = substr($str,2,5);

//指定编码从下标为几开始截取几个
//$newStr = mb_substr($str,2,3,'UTF-8');
//p($newStr);


$str = '2018.06.03.jpg';
p($str);

//返回某字符串在字符串中开始至结束的字符 //截取字符
//$newStr = strchr($str,'.');//.06.03.jpg

//从右开始返回某字符串在字符串中开始至结束的字符 //截取字符
//$newStr = strrchr($str,'.');//.jpg
//$newStr = ' .jpg';
//$newStr = ltrim($newStr,' .');
//p($newStr);

//返回某字符串在字符串中第一次出现的位置  //获取位置
//$newStr = strpos($str,'.');

//返回某字符串在字符串中最后一次出现的位置  //获取位置
$newStr = strrpos($str,'.');
p($newStr);
