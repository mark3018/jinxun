<?php

class MsgController extends CommonController
{
    protected $db;

    public function __construct()
    {
        $this->db = include "./Databases/msg.php";
        parent::__construct();
    }

    public function index()
    {
        include './View/Msg/index.html';
    }

    //异步添加
    public function add(){
        $_POST['sendtime'] = date('Y-m-d H:i:s',time());
        $this->db[] = $_POST;
        $this->putData($this->db,'./Databases/msg.php');
        $_POST['id'] = max(array_keys($this->db));
        echo json_encode($_POST);die;
    }

    //异步删除
    public function del(){
        $id = $_POST['kk'];
        unset($this->db[$id]);
        $this->putData($this->db,'./Databases/msg.php');
        echo 1;die;
    }
}