<?php

/**
 * 留言板的删除方法
 */

//1.载入函数库
include "../help.php";

//2.获取地址栏参数 知道要删除哪一条数据
$id = $_GET['id'];

//3.载入数据库
$data = include "../Databases/data.php";

//4.销毁数据 unset()一般用于删除变量
unset($data[$id]);

//5.合法化PHp代码  6.写入文件当中
putData($data,'../Databases/data.php');

//7.//删除成功提示
success('删除成功','../index.php');

