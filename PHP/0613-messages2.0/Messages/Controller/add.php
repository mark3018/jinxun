<?php

/**
 * 留言板的添加方法
 */

//2.载入模拟数据库文件
$data = include "./Databases/data.php";

if(IS_POST){
    //8.进行后台手动验证
    if(empty($_POST['nickname'])){
        error('请填写昵称');
    }
    if(empty($_POST['content'])){
        error('请填写内容');
    }

    //3.进行给表单提交的数据增加添加时间数据 类似于数组直接赋值的方式
    //在我们模拟的数据库文件当中可以存格式化时间戳 也就是2018-06-12 20:44:58
    //但是在使用真实数据库的时候，我们更建议存时间戳，为什么？因为时间戳是数值类型，
    //而格式化时间戳 因为有符号 所以它是字符串类型。
    $_POST['time'] = time();
    //4.数组进行追加 而不是变量的覆盖
    $data[] = $_POST;
    //5.将数据转为合法的PHP代码 //6.将数据写入到文件当中
    putData($data,'./Databases/data.php');
    //7.提示信息
    success('添加成功','index.php');
}

//1.载入模板文件
include './View/index.html';