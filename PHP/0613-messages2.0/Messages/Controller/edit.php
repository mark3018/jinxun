<?php

/**
 * 留言板的编辑方法
 */

//1.载入函数库
include "../help.php";

//3.获取旧数据
$id = $_GET['id'];
$data = include "../Databases/data.php";
$oldData = $data[$id];

if(IS_POST){
    //4.获取添加时 时间
    $_POST['time'] = $oldData['time'];
    //5.进行数据的覆盖
    $data[$id] = $_POST;
    //6.合法化PHP代码 //7.写入文件
    putData($data,'../Databases/data.php');
    //8.提示信息
    success('编辑成功','../index.php');
}

//2.载入模板文件
include "../View/edit.html";