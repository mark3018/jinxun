<?php

include "../functions.php";

class Company
{
    public $name;
    public $address;

    public function boss()
    {
        p('我是老板我怕谁1');
    }

    public function index()
    {
        $this->ziyuan();
    }

    private function xiaomi()
    {
        p('小秘');
    }

    protected function ziyuan()
    {
        p('我是资源');
    }
}

class SonCompany extends Company
{
    public function index2()
    {
        $this->ziyuan();
    }
}

$tencent = new Company();
p($tencent->name = '腾讯');
p($tencent->address = '中国深圳');
$tencent->boss();
$tencent->index();

$tencentGame = new SonCompany();
$tencentGame->index2();

/**
 * public
 * 公有的
 *在类的外部能够访问 不受限制
 *在类的内部能够访问 不受限制
 * 在子类中能够访问 不受限制
 *
 *
 * private
 * 私有的
 * 在类的外部不能访问 受限制
 * 在类的内部能够访问 不受限制
 * 在子类中不能访问  受限制
 *
 *
 * protected
 * 受保护的
 * 在类的外部不能访问 受限制
 * 在类的内部能够访问 不受限制
 * 在子类中能够访问 不受限制
 */