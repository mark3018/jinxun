<?php

include '../functions.php';

//函数
function func(){
    //代码提
}

class Human{
    //属性  类里面的变量
    public $name;
    public $hair;
    public $sex;

    //方法 类里面的函数
    public function play(){
        echo '我可以玩耍<br>';
    }

    public function eat(){
        echo '我能吃<br>';
    }

    public function study(){
        echo '我可以学习<br>';
    }
}

//实例化类构建一个对象
$erkang = new Human();
//对象调用类里面的属性
p($erkang->name = '尔康');
p($erkang->hair = '半个光头加长发飘飘');
p($erkang->sex = '是个纯爷们');
//对象调用类当中的方法
$erkang->play();
$erkang->eat();
echo '<hr>';
$ziwei = new Human();
//对象调用类里面的属性
p($ziwei->name = '滋味');
p($ziwei->hair = '长发飘飘还有一点刘海');
p($ziwei->sex = '一个美丽的姑娘');
//对象调用类当中的方法
$ziwei->play();
$ziwei->eat();

