<?php

include "../functions.php";

class Controller{
    public function __construct()
    {
        p('我是自动执行的');
    }

    public function index(){
        p('首页');
    }

    public function add(){
        p('添加');
    }

    public function edit(){
        p('修改');
    }

    //析构方法
    //用于对象在内存中被销毁时自动执行的方法 不带任何参数
    public function __destruct()
    {
        p('我狗带了');
    }
}

$Controller = new Controller();
$Controller -> index();
$Controller -> add();
$Controller -> edit();

