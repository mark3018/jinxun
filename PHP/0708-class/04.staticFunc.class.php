<?php

include "../functions.php";

class Model
{
    public static $db = '连接数据库';

    public function index()
    {
        p('我是普通方法');
    }

    public static function query()
    {
        p('查询数据');
    }

    public static function insert($sql)
    {
        p($sql);
        self::query();
    }
}

$model = new Model();
$model->index();

p(Model::$db);
//Model::query();
Model::insert('select * from stu ');

class Son extends Model{
    public static function exec(){
        parent::query();
    }
}
Son::exec();

/**
 * 总结：
 * 普通方法/普通属性：
 * 对象 =  new 类名，
 * 在类的外部 对象->属性或方法,
 * 在类的内部 $this->属性或方法,
 * 在子类中 $this->父类的属性或方法。
 *
 *
 *  静态方法/静态属性:
 *  在类的外部 类名::方法/属性
 *  在类的内部 self::方法/属性
 *  在子类中 parent::父类的方法/属性
 */

/**
 * 1.static方法是类中的一个成员方法，属于整个类，即使不用创建对象也可以直接调用；
 * 2.静态方法效率上要实例化的方法要高，静态方法缺点是不自动销毁，而实例化的方法则可以销毁；
 * 3.静态方法和静态变量创建后始终使用一块内存，而实例化的方式会创建多个内存。
 */