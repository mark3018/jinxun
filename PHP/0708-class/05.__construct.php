<?php

include "../functions.php";

/**
 * 构造方法
 * Class Controller
 */
class Controller{
    //构造方法
    //只要一实例化就会自动执行的 使用场景：判断是否登录
    public function __construct()
    {
        p('我是自动执行的');
    }

    public function index(){
        p(1);
    }
}

class SonController extends Controller{
    public function __construct()
    {
        //不覆盖父类当中的构造方法
        parent::__construct();
        p('哈哈哈哈哈哈哈');
    }

    public function index(){
        //不覆盖父类当中的普通方法
        parent::index();
        p(2);
    }
}

$Son = new SonController();
$Son -> index();