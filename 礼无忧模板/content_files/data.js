(function() {
	try {
		var C = false,
		z = function(p) {
			try {
				C && console && console.log(p)
			} catch(g) {}
		};
		if (!window.__resource_server) {
			window.__resource_server = "http://xum.51zhanzhuang.cn"
		}
		if (!window.__server_domain) {
			window.__server_domain = "51zhanzhuang.cn"
		}
		var w = {};
		w.u_crc = "";
		w.u_allow_coo = 0;
		w.u_uid = "";
		w.u_create_time = "";
		w.ad_slot_id = "";
		w.page_client_type = 1;
		w.cookie_mapping_info_url = __resource_server + "/data/cmInfo.js";
		w.uid_domain = __server_domain;
		w.adInfoType = {
			s1: "PC广告位:",
			s2: "移动广告位:",
			w: "wh:"
		};
		w.page_client_type = (function() {
			function e() {
				var G = new Array("2.0 MMP", "240320", "AvantGo", "BlackBerry", "Blazer", "Cellphone", "Danger", "DoCoMo", "Elaine/3.0", "EudoraWeb", "hiptop", "IEMobile", "KYOCERA/WX310K", "KFTHWI", "KFAPWI", "PlayBook", "BB10", "LG/U990", "MIDP-2.0", "MMEF20", "MOT-V", "NetFront", "Newt", "Nintendo Wii", "Nitro", "Nokia", "Opera Mini", "Opera Mobi", "Palm", "Playstation Portable", "portalmmm", "Proxinet", "ProxiNet", "SHARP-TQ-GX10", "Small", "SonyEricsson", "Symbian OS", "SymbianOS", "TS21i-10", "UP.Browser", "UP.Link", "Windows CE", "WinWAP", "Android", "iPhone", "iPod", "iPad", "Windows Phone", "HTC");
				var F = new Array("Microsoft Pocket Internet Explorer");
				var p = navigator.userAgent.toString();
				for (var E = 0; E < G.length; E++) {
					if (p.indexOf(G[E]) >= 0) {
						return true
					}
				}
				var g = navigator.appName.toString();
				for (var E = 0; E < F.length; E++) {
					if (p.indexOf(F[E]) >= 0) {
						return true
					}
				}
				return false
			}
			return e() ? 2 : 1
		})();
		var x = function(e) {
			var g = new RegExp("(\\?|#|&)" + e + "=([^&]*)(&|#)", "i");
			var p = window.location.href.match(g);
			return p !== null ? decodeURIComponent(p[2]) : ""
		};
		w.ad_slot_id = x("sid");
		var b = {
			setCookie: function(g, F, e, E, H) {
				if (!e) {
					e = 365 * 10
				}
				var G = new Date();
				G.setTime(G.getTime() + (1000 * 60 * 60 * 24 * e));
				var p = g + "=" + F + ";expires=" + G.toUTCString();
				if (!H) {
					H = "/"
				}
				p += ";path=" + H;
				if (E) {
					p += ";domain=" + E
				}
				document.cookie = p
			},
			getCookie: function(p) {
				var g = p + "=";
				var e = document.cookie.split(";");
				for (var E = 0; E < e.length; E++) {
					var F = e[E];
					while (F.charAt(0) == " ") {
						F = F.substring(1)
					}
					if (F.indexOf(g) != -1) {
						return F.substring(g.length, F.length)
					}
				}
				return ""
			},
			delCookie: function(e, g, p) {
				this.setCookie(e, "", -1, g, p)
			},
			testCookie: function() {
				this.setCookie("_t", "1", 1, w.uid_domain);
				var e = this.getCookie("_t");
				if (e) {
					return true
				}
				return false
			}
		};
		function n(p) {
			var e = [];
			if (p) {
				for (var g in p) {
					e.push(g + "=" + encodeURIComponent(p[g]))
				}
			}
			return e.join("&")
		}
		function d(F) {
			var p = {};
			if (F) {
				var e = F.split("&");
				for (var g = 0; g < e.length; g++) {
					var E = e[g].split("=");
					p[E[0]] = decodeURIComponent(E[1])
				}
			}
			return p
		}
		var o = (function() {
			var F = "_QWERJAD_",
			e = "postMessage" in window;
			function G(J) {
				var H = [];
				if (J) {
					for (var I in J) {
						H.push(I + "=" + encodeURIComponent(J[I]))
					}
				}
				return H.join("&")
			}
			function g(L) {
				var J = {};
				if (L) {
					var H = L.split("&");
					for (var I = 0; I < H.length; I++) {
						var K = H[I].split("=");
						J[K[0]] = decodeURIComponent(K[1])
					}
				}
				return J
			}
			function E(K, I, H) {
				var J = "";
				if (arguments.length < 2) {
					J = "target error - target and name are both required"
				} else {
					if (typeof K != "object") {
						J = "target error - target itself must be window object"
					} else {
						if (typeof I != "string") {
							J = "target error - target name must be string type"
						}
					}
				}
				if (J) {
					throw new Error(J)
				}
				this.target = K;
				this.name = I;
				this.myName = H
			}
			if (e) {
				E.prototype.send = function(I, H) {
					this.target.postMessage(F + this.myName + "^^^" + (H ? H: "") + "^^^" + G(I), "*")
				}
			} else {
				E.prototype.send = function(I, H) {
					var J = window.navigator[F + this.name];
					if (typeof J == "function") {
						J(F + this.myName + "^^^" + (H ? H: "") + "^^^" + G(I), window)
					} else {
						throw new Error("target callback function is not defined")
					}
				}
			}
			function p(I, H) {
				this.targets = {};
				this.name = I;
				this.listenFunc = {};
				F = H || F;
				if (typeof F !== "string") {
					F = F.toString()
				}
				this.initListen()
			}
			p.prototype.addTarget = function(J, H) {
				var I = new E(J, H, this.name);
				this.targets[H] = I
			};
			p.prototype.initListen = function() {
				var I = this;
				var H = function(N) {
					if (typeof N == "object" && N.data) {
						N = N.data
					}
					if (N.indexOf(F) == 0) {
						N = N.slice(F.length);
						var L = N.indexOf("^^^");
						var K = N.slice(0, L);
						var N = N.slice(L + 3);
						var J = N.indexOf("^^^");
						var M = N.slice(0, J);
						var N = N.slice(J + 3);
						if (I.listenFunc[K + "_" + M]) {
							I.listenFunc[K + "_" + M](g(N))
						}
					}
				};
				if (e) {
					if ("addEventListener" in document) {
						window.addEventListener("message", H, false)
					} else {
						if ("attachEvent" in document) {
							window.attachEvent("onmessage", H)
						}
					}
				} else {
					window.navigator[F + this.name] = H
				}
			};
			p.prototype.listen = function(H) {
				var H = H || {
					targetName: "",
					action: "",
					callback: function(I) {}
				};
				this.listenFunc[H.targetName + "_" + H.action] = (H.callback)
			};
			p.prototype.clear = function() {
				this.listenFunc = {}
			};
			p.prototype.sendAll = function(J, I) {
				var H = this.targets,
				K;
				for (K in H) {
					if (H.hasOwnProperty(K)) {
						H[K].send(J, I)
					}
				}
			};
			p.prototype.sendMessage = function(H) {
				var H = H || {
					targetName: "",
					action: "",
					data: {}
				};
				this.targets[H.targetName].send(H.data, H.action)
			};
			return p
		})();
		var f = function(F, G, E, g, e, I) {
			var H = I;
			if (!H) {
				H = "__zxcv_" + Math.floor(2147483648 * Math.random()) + "_callback"
			}
			if (F == "jsonp") {
				window[H] = function() {
					if (g) {
						g(arguments[0])
					}
				}
			}
			var p = document.createElement("script");
			p.async = "async";
			p.src = E + (/\?/.test(E) ? "&": "?") + G + "=" + H;
			p.onload = p.onreadystatechange = function() {
				if (!p.readyState || /loaded|complete/.test(p.readyState)) {
					p.onload = p.onreadystatechange = null;
					if (F == "var") {
						if (g) {
							g(window[H])
						}
					}
					if (p.parentNode) {
						p.parentNode.removeChild(p)
					}
					window[H] = null
				}
			};
			p.onerror = function(J) {
				if (p.parentNode) {
					p.parentNode.removeChild(p)
				}
				window[H] = null;
				if (e) {
					e(J)
				}
			};
			document.body.appendChild(p)
		};
		try {
			window.localStorage.getItem("test")
		} catch(B) {
			try {
				if (/MSIE/.test(navigator.userAgent)) {
					if (!window.UserData) {
						window.UserData = function(p) {
							if (!p) {
								p = "user_data_default"
							}
							var g = document.createElement("input");
							g.type = "hidden";
							g.addBehavior("#default#userData");
							document.body.appendChild(g);
							g.save(p);
							var e = new Date();
							e.setDate(e.getDate() + 3650);
							g.expires = e.toUTCString();
							this.file_name = p;
							this.dom = g;
							return this
						};
						window.UserData.prototype = {
							setItem: function(g, e) {
								this.dom.setAttribute(g, e);
								this.dom.save(this.file_name)
							},
							getItem: function(e) {
								this.dom.load(this.file_name);
								return this.dom.getAttribute(e)
							},
							removeItem: function(e) {
								this.dom.removeAttribute(e);
								this.dom.save(this.file_name)
							},
							clear: function() {
								this.dom.load(this.file_name);
								var e = new Date();
								e = new Date(e.getTime() - 1);
								this.dom.expires = e.toUTCString();
								this.dom.save(this.file_name)
							}
						}
					}
					window.localStorage = new window.UserData("user_data_qwert")
				}
			} catch(B) {}
		}
		function r() {
			var e = new Date();
			var p = e.getMonth() + 1;
			var g = e.getDate();
			return e.getFullYear() + "" + (p > 9 ? p: "0" + p) + "" + (g > 9 ? g: "0" + g)
		}
		if (typeof window.atob == "undefined") {
			function q(E) {
				var p = "",
				I, K, F = "",
				H, G = "",
				J = 0;
				k = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
				do {
					I = k.indexOf(E.charAt(J++)), K = k.indexOf(E.charAt(J++)), H = k.indexOf(E.charAt(J++)), G = k.indexOf(E.charAt(J++)), I = I << 2 | K >> 4, K = (K & 15) << 4 | H >> 2, F = (H & 3) << 6 | G, p += String.fromCharCode(I), 64 != H && (p += String.fromCharCode(K)), 64 != G && (p += String.fromCharCode(F))
				} while ( J < E . length );
				return unescape(p)
			}
		}
		function m(p) {
			var g, G, F = "",
			E;
			p += "";
			g = 0;
			for (G = p.length; g < G; g++) {
				E = p.charCodeAt(g).toString(16),
				F += 2 > E.length ? "0" + E: E
			}
			return F
		}
		function a() {
			try {
				var F = document.getElementById;
				var p = document.createElement("canvas");
				p.setAttribute("width", 220);
				p.setAttribute("height", 30);
				var g = p.getContext("2d");
				g.textBaseline = "top";
				g.font = "14px 'Arial'";
				g.textBaseline = "alphabetic";
				g.fillStyle = "#f60";
				g.fillRect(125, 1, 62, 20);
				g.fillStyle = "#069";
				g.fillText("www.google.com<canvas>1.0", 2, 15);
				g.fillStyle = "rgba(102, 204, 0, 0.7)";
				g.fillText("www.google.com<canvas>1.0", 4, 17);
				p = p.toDataURL("image/png");
				g = q(p.replace("data:image/png;base64,", ""));
				var E = m(g.slice( - 16, -12));
				return E
			} catch(G) {}
			return "nocrc"
		}
		function c(e) {
			e = e || new Date();
			return e.getFullYear() + "-" + e.getMonth() + "-" + e.getDay() + "-" + e.getHours()
		}
		function u(e) {
			e = e || new Date();
			return e.getFullYear() + "-" + e.getMonth() + "-" + e.getDay()
		}
		function t() {
			var e = this;
			e.u_uid = "";
			e.u_allow_coo = 0;
			e.allow_storage = 0;
			e.allow_cookie = 0;
			function g() {
				e.testCoo();
				w.u_allow_coo = e.u_allow_coo;
				if (e.u_allow_coo == 1) {
					var E = e.getItem("uid");
					var p = e.getItem("utime");
					if (E && /^\d{32}$/.test(E) && p && /^\d{13}$/.test(p)) {
						w.u_uid = E;
						w.u_create_time = p
					} else {
						e.setItem("uid", "");
						e.setItem("utime", "")
					}
				}
			}
			g();
			return e
		}
		t.prototype.testCoo = function() {
			try {
				if (window.localStorage) {
					window.localStorage.setItem("test", "1");
					var g = window.localStorage.getItem("test");
					if (g == "1") {
						this.allow_storage = 1
					}
				}
			} catch(p) {}
			try {
				this.allow_cookie = b.testCookie() ? 1 : 0
			} catch(p) {}
			if (this.allow_storage == 1 || this.allow_cookie == 1) {
				this.u_allow_coo = 1
			}
		};
		t.prototype.getItem = function(g) {
			var E = "";
			try {
				if (this.allow_storage == 1) {
					var p = window.localStorage.getItem(g);
					if (p) {
						E = p
					}
				}
			} catch(F) {}
			if (!E) {
				try {
					var G = b.getCookie(g);
					if (G) {
						E = G
					}
				} catch(F) {}
			}
			return E
		};
		t.prototype.setItem = function(p, E, g) {
			if (E !== null && E !== undefined && E !== "") {
				E = E + ""
			}
			try {
				if (this.allow_storage == 1) {
					window.localStorage.setItem(p, E)
				}
			} catch(F) {}
			try {
				if (this.allow_cookie == 1) {
					if (g) {
						b.setCookie(p, E, 1, w.uid_domain, "/")
					} else {
						b.setCookie(p, E)
					}
				}
			} catch(F) {}
		};
		t.prototype.addAdInfo = function(e, E, p) {
			var g = this.getAdInfo(e, E, p);
			g.u_hour_count++;
			g.u_day_count++;
			g.u_all_count++;
			this.setItem(e + "_" + E, Math.floor(p / 1000) + "_" + g.u_hour_count + "_" + g.u_day_count + "_" + g.u_all_count, true)
		};
		t.prototype.getAdInfo = function(I, g, E, L) {
			var G = this.getItem(I + "_" + g);
			var N = 0;
			var H = 0;
			var F = 0;
			var K;
			if (G) {
				var P = G.split("_");
				K = new Date(P[0].length == 10 ? P[0] * 1000 : P[0] * 1);
				N = P[1];
				H = P[2];
				F = P[3]
			} else {
				K = 0
			}
			var p = c(K);
			var J = u(K);
			var e = new Date(E * 1);
			var O = c(e);
			var M = u(e);
			if (p != O) {
				N = 0
			}
			if (J != M) {
				H = 0
			}
			if (!L) {}
			return {
				u_hour_count: N,
				u_day_count: H,
				u_last_time: +K,
				u_all_count: F
			}
		};
		t.prototype.setPdsa = function(e) {
			this.setItem("pdsa_" + w.page_client_type, e.dsa)
		};
		t.prototype.getPdsa = function(e) {
			var E = this.getItem("pdsa_" + w.page_client_type);
			var g = {
				dsa: ""
			};
			if (E && E != "undefined" && E.indexOf("|") == -1) {
				g.dsa = E
			}
			return g
		};
		t.prototype.getSlotSn = function() {
			var e = this.getItem("ssn_" + w.page_client_type);
			var g = this.getItem("ssnt_" + w.page_client_type);
			var p = +new Date();
			if (!e || (g && (p - g) > (30 * 1000))) {
				e = -1
			}
			e++;
			this.setItem("ssn_" + w.page_client_type, e);
			this.setItem("ssnt_" + w.page_client_type, p);
			return e
		};
		t.prototype.resetSlotSn = function(e) {
			var g = this.getItem("ssn_" + w.page_client_type);
			if (g == e) {
				this.setItem("ssn_" + w.page_client_type, -1);
				this.setItem("ssnt_" + w.page_client_type, 0);
				return true
			} else {
				return false
			}
		};
		t.prototype.maxSA = function(p) {
			var g = this.setItem("ssns_" + w.page_client_type);
			var e = this.setItem("ssna_" + w.page_client_type);
			if (!g) {
				g = p.s
			}
			if (!e) {
				e = p.a
			}
			if ((p.s > g && ((p.s - g) < 10)) || (p.s < g && ((g - p.s) > 10))) {
				g = p.s
			}
			if ((p.a > e && ((p.a - e) < 10)) || (p.a < e && ((e - p.a) > 10))) {
				e = p.a
			}
			this.setItem("ssns_" + w.page_client_type, g);
			this.setItem("ssna_" + w.page_client_type, e);
			return {
				s: g,
				a: e
			}
		};
		t.prototype.getRemoteUid = function(g, p, e) {
			f("var", "ext", e,
			function(G) {
				var F = "cm" + g;
				var E = p;
				if (G) {
					E = G
				}
				C && console.log("other dsp uid[remote]:" + F);
				C && console.log(E);
				b.setCookie(F, E, 7, w.uid_domain)
			})
		};
		t.prototype.cookieMapping = function(p) {
			var e = this;
			if (b.testCookie()) {
				var g = p;
				C && console.log("my uid:" + g);
				f("jsonp", "cp", w.cookie_mapping_info_url + "?_ver=23" + r(),
				function(H) {
					C && console.log("cmInfo:");
					C && console.log(H);
					if (H) {
						for (var F in H) {
							var E = H[F];
							if (E) {
								E = E.replace("$[UID]", g);
								var G = b.getCookie("cm" + F);
								if (!G) {
									e.getRemoteUid(F, g, E)
								}
							}
						}
					}
				},
				null, "_cminfo_callback")
			}
		};
		
		var j = new t();
		var i = "_jad_parent",
		v = "_iframe_ad_data",
		D = "_iframe_uid_data";
		w.projectName = x("pn");
		var h = new o(v, w.projectName);
		h.addTarget(window.parent, i);
		h.listen({
			targetName: i,
			action: "set_u_uid",
			callback: function(p) {
				//j.cookieMapping(p.u_uid);
				var e = j.getItem("uid");
				var g = j.getItem("utime");
				if (!e || !g) {
					j.setItem("uid", p.u_uid);
					j.setItem("utime", p.server_time)
				}
			}
		});
		h.listen({
			targetName: i,
			action: "add_ad_info",
			callback: function(F) {
				C && console.log(F);
				var e = F.sid;
				var g = F.dsp;
				var E = F.whid;
				var p = F.server_time;
				j.addAdInfo("s" + w.page_client_type, e, p);
				if (g == 1 && F.sourceId) {
					j.addAdInfo("3s", F.sourceId, p)
				}
				if (g == 2 && F.schemeId) {
					j.addAdInfo("se", F.sourceId, p)
				}
			}
		});
		h.listen({
			targetName: i,
			action: "set_ssn_info",
			callback: function(e) {
				if (j.resetSlotSn(e.ssn) && e.dsa) {
					j.setPdsa(e)
				}
			}
		});
		h.listen({
			targetName: i,
			action: "add_ad_click",
			callback: function(F) {
				var e = F.sid,
				g = F.dsp,
				E = F.whid;
				var p = F.server_time;
				j.addAdInfo("sc" + w.page_client_type, e, p);
				if (g == 1 && F.sourceId) {
					j.addAdInfo("3sc", F.sourceId, p)
				}
				if (g == 2 && F.schemeId) {
					j.addAdInfo("sec", F.sourceId, p)
				}
			}
		});
		h.listen({
			targetName: i,
			action: "add_ad_request",
			callback: function(g) {
				var e = g.server_time;
				j.addAdInfo("c", w.page_client_type, e)
			}
		});
		var l = j.getAdInfo("s" + w.page_client_type, w.ad_slot_id, _server_time);
		var y = j.getPdsa(w.ad_slot_id);
		var s = j.getSlotSn();
		h.sendMessage({
			targetName: i,
			action: "a",
			data: {
				u_allow_coo: w.u_allow_coo,
				u_crc: w.u_crc,
				u_uid: w.u_uid,
				u_create_time: w.u_create_time,
				u_hour_count: l.u_hour_count,
				u_day_count: l.u_day_count,
				u_all_count: l.u_all_count,
				u_last_time: l.u_last_time,
				dsa: y.dsa,
				ssn: s
			}
		})
	} catch(B) {
		function A(p, g) {
			var E = document,
			G = window,
			F = encodeURIComponent;
			try {
				var I = [];
				I.push("name=" + F(p.name));
				I.push("msg=" + F(p.message));
				I.push("ref=" + F(E.referrer));
				I.push("ex=" + F(g));
				I.push("rnd=" + Math.floor(2147483648 * Math.random()))
			} catch(H) {}
		}
	}
})(window, document);
