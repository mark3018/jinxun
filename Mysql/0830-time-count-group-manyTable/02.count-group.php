<?php

//统计
select count(*) from stuu;


//取最小值
select min(age) from stuu;


//取最大值
select max(age) from stuu;


//求和
select sum(age) from  stuu;


//平均数
select avg(age) from stuu;



/**
 * 分组  分组是将一个数据集划分成若干个小区域，然后再针对若干个小区域进行数据处理。group by 可以对数据进行分组，但是分组后的数据并不能进行组内排序 说白了就是归类。
 */
select sex from stuu group by sex;
+------+
| sex  |
+------+
| 男     |
| 女    |
+------+

//分组并且获取对应的数量
select sex,count(*) from stuu group by sex;
+------+----------+
| sex  | count(*) |
+------+----------+
| 男     |        9 |
| 女    |        4 |
+------+----------+


//分组获取对应的数量并且进行筛选 获取性别女的
//where 在分组之前过滤数据
//having 是在分组之后过滤数据
select sex,count(*) from stuu group by sex having sex = '女';


//把所有的男女数据分为两块：
select * from stuu order by sex;
