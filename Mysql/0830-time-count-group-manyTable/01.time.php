<?php

//生日字段用 date类型  1990-08-30
//          datetime 1990-08-30  24:00:00



//获得当前日期
select curdate() as date;


//获取当前时间
 select curtime() as t;


//获取当前的日期与时间
select now() as n;


//获取具体年份
select year('2018-10-01') as y;


//查找所有90后的女同学
select * from stuu where sex='女' and year(birthday)>=1990;


//查找班级年龄最小的
 select * from stuu order by age asc limit 1;