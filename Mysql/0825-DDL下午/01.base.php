<?php

/**
 * char(20)  定长  20个人报名准备20张桌椅  速度更快
* varchar(20)  变长 有多少人来上课就准备多少张桌椅  更节省空间
*/

create table stu (sid int,sname char(5),password varchar(5));

insert into stu set sid=1,sname='宋佳佳',password=123456;

select * from stu;


//int后面的数值不是限定，而是结合前导零zerofill进行填充0
//需要注意的是，使用⼀一个宽度指示器不会影响字段的⼤大⼩小和它可以存储的值的范围
create table user4 (uid int(3) zerofill,uname char(5));


//unsigned 不能为负
 create table user(uid int unsigned,uname char(5));


//enum枚举类型 单选   set 多选
create table stu2 (sid int unsigned,sname char(5),sex enum('男','女'),hobby set('篮球','足球','羽毛球','地球'));
 insert into stu2 set sid=3,sname='小张',sex='男',hobby='篮球,足球';


//like是广泛的模糊匹配，字符串中没有分隔符
//find_in_set 是精确匹配,字段值以英文','分隔，
//find_in_set查询的结果要小于like查询的结果
select * from stu2 where find_in_set('羽毛球',hobby);
select * from stu2 where hobby like '%羽毛%';


//default 结合 not null一起使用
create table test(tid int default 1,content char(20) default '');
create table test2(tid int default 1 not null,content char(20) default '' not null);


//unique指定唯一标识 非重
create table user2 (uid int,uname char(10) unique);


//主键自增  一个表只能有一个主键 并且 primary key auto_increment一起使用
create table arc (aid int primary key auto_increment,content text);


//复制表结构
create table arc2 like arc;
//复制表数据
insert into arc2 select * from arc;
//创建表及复制数据 但是自增不会复制过去
create table arc3 select * from arc;