<?php
function p($var){
	echo '<pre>' . print_r($var) . '</pre>';
}

try{
	$dsn = 'mysql:host=127.0.0.1;dbname=jinxun';
	$user = 'root';
	$password = 'root';
	$pdo = new PDO($dsn,$user,$password);
	//把错误类型设置为异常类型
	$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	$pdo->query("SET NAMES UTF8");
	//1.实现预准备，返回预准备对象
	$sth = $pdo->prepare("SELECT * FROM article WHERE aid=?");
	//2.绑定参数
	$aid = 2;
	$sth->bindParam(1,$aid,PDO::PARAM_INT);
	//3.执行
	$sth->execute();
	$data = $sth->fetchAll(PDO::FETCH_ASSOC);
	p($data);




}catch(PDOException $e){
	echo $e->getMessage();
}


