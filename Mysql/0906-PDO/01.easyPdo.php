<?php

include "../functions.php";

//数据源：数据库类型，主机地址，数据库名称
$dsn = "mysql:host=localhost;dbname=jinxun";
//数据库用户名
$username = 'root';
//数据库密码
$password = 'root';
//实例化PDO
$pdo = new PDO($dsn,$username,$password);
//设置字符集
$pdo -> query('set names utf8');
//返回一个对象 query返回一个有结果集的数据
$obj = $pdo -> query("select * from stu where sname='叶新'");
//fetchAll 获取所有数据
//FETCH_ASSOC获得一个关联数组
$arr = $obj -> fetchAll(PDO::FETCH_ASSOC);
p($arr);