<?php

include "../functions.php";

//数据源：数据库类型，主机地址，数据库名称
$dsn = "mysql:host=localhost;dbname=jinxun";
//数据库用户名
$username = 'root';
//数据库密码
$password = 'root';
//实例化PDO
$pdo = new PDO($dsn,$username,$password);
//设置字符集
$pdo -> query('set names utf8');

//设置错误提示 沉默的错误 不显示错误信息
//$pdo -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_SILENT);

//警告性错误
$pdo ->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

//返回一个对象 query返回一个有结果集的数据
$obj = $pdo -> query('select * from stu1');
