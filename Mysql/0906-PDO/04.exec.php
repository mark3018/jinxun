<?php

include "../functions.php";

//query 执行查询语句 执行有结果集的操作
//exec 执行数据的增删改 执行无结果集的操作

try{
    $dsn = "mysql:host=localhost;dbname=jinxun";
    $username = 'root';
    $password = 'root';
    $pdo = new PDO($dsn,$username,$password);
    $pdo -> query('set names utf8');
    $pdo ->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

    //返回受影响的条数
    //$rows = $pdo -> exec("insert into stu set sname='李四',sex='男',age=18");

    //$rows = $pdo -> exec("update stu set sname='周星驰' where sid = 28");

    $rows = $pdo ->exec("delete from stu where sid = 28");
    p($rows);
}catch (Exception $e){
    echo "<h1 style='color: red;'>{$e->getMessage()}</h1>";
}