<?php

//查看创建表
show create table stu;


//修改表名
alter table stu rename stu2;


//修改字段类型同时更名
alter table stuu change sname stuname char(5) unique;


//修改字段类型并且改变位置  first / after 字段
alter table stuu modify stuname varchar(5) after sex;
alter table stuu modify stuname varchar(5) first;


//追加字段
alter table stuu add phone int unsigned after hobby;
alter table stuu add birthday date after age;


//删除字段
alter table stuu drop birthday;


//移除主键 先执行删除自增 再删除主键
alter table stuu modify sid int;
alter table stuu drop primary key;


//添加主键自增  先添加主键 再加上自增
alter table stuu add primary key (sid);
alter table stuu modify sid int auto_increment;