<?php

//1.随机获取6条学生数据。
select * from stu order by rand() limit 6;

//2.向stu表插入5条数据。
insert into stu () values (),(),(),(),();

//3.将stu表中所有的年龄+1。
update stu set age=age+1;

//4.将表中年龄最小的数据进行删除。
delete from stu order by age asc limit 1;