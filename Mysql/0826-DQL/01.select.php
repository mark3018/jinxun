<?php

//起别名
select * from stu;
select uname from stu;
select uname as u from stu;


//条件
select * from stu where age>=30;
select * from stu where sex='女' and age>20;
select * from stu where sex='男' or age>30;


//字符串链接
select concat(uname,sex) as a from stu;


//自身比较
select age,age>30 as compare from stu;
+------+---------+
| age  | compare |
+------+---------+
|   27 |       0 |
|   36 |       1 |
|   26 |       0 |
|   22 |       0 |
|   30 |       0 |
|   46 |       1 |
+------+---------+


//过滤
select distinct(sex) from stu;
+------+
| sex  |
+------+
| 男     |
| 女    |
+------+


//是否为空
select * from stu where age is null;
select * from stu where age is not null;


//if表达式
select age,if(age>25,'老','幼') from stu;
select age,ifnull(age,'空') from stu;


//排序
//降序
select * from stu order by age desc;
//升序  如果不给asc 默认是升序
select * from stu order by age asc;


//数据截取
select * from stu order by age limit 5;
//从位置为2开始截取  截取4条  位置从零开始
select * from stu order by age limit 2,4;


//范围
//获取年龄在26至30之间的数据 包括26和30
select * from stu where age between 26 and 30;
select * from stu where age not between 26 and 30;


//表达式具体的值
//获取年龄为26和27的数据
select * from stu where age in (26,27);
select * from stu where not age in (26,27);


//like匹配
//包含震字的数据
select * from stu where uname like '%震%';
//匹配以张字开头的数据
select * from stu where uname like '张%';
//匹配以新字结尾的数据
select * from stu where uname like '%新';


//截取字符串
//从左侧开始截取字符
select left(uname,2) as ltrim from stu;
//从右侧开始截取字符
select right(uname,2) as rtrim from stu;
//从中间取字符，参数2位起始字符 参数3为长度
select mid(uname,1,2) as mtrim from stu;


//随机获取数据
select * from stu order by rand();
//随机获取数据并截取
select * from stu order by rand() limit 6;