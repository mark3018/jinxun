<?php

/**
 * 打开Mysql
 */
//如果windows
//方式一
//win+r
//mysql -uroot -p;
//注意：如果显示mysql不是内部命令 进行设置环境变量

//方式二
//通过phpstudy打开mysql命令行 然后进行密码登录  默认密码root


/**
 * 退出
 */
quit/exit;
ctrl+c;


//显示所有的数据库
show databases;

//创建数据库(字符集 mac linux utf8/windows gbk)
create database jinxun charset gbk;

//使用数据库
use jinxun;

//查看所有数据表
show tables;

//删除数据库
drop database jinxun;
drop database if exists jinxun;

//创建数据表 有一个字段
 create table user (uid int);

//查看表结构
desc user;
+-------+---------+------+-----+---------+-------+
| Field | Type    | Null | Key | Default | Extra |
+-------+---------+------+-----+---------+-------+
| uid   | int(11) | YES  |     | NULL    |       |
+-------+---------+------+-----+---------+-------+

//删除数据表
drop table user;


//decimal类型 5代表总位数 2代表小数点后面位数 一般用于银行里的数据表
 create table bank (bid int,money decimal(5,2));
//压入数据
insert into bank set bid=1,money=255;
//查看bank表的数据
select * from bank;

