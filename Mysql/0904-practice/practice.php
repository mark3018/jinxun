<?php
//1.向stu表中插入两条数据；
insert into stu (sname,age) values (),();


//2.将stu表中id=10的数据：性别改为女，年龄改为20；
update stu set sex='女',age=20 where sid=10;


//3.将年龄小于30岁的同学年龄+1；
update stu set age=age+1 where age<30;


//4.删除年龄最小的同学；
delete from stu order by age asc limit 1;


//5.学生表中随机获取五条数据；
select * from stu order by rand() limit 5;


//6.给stu表添加一个time字段；
alter table stu add time int default 0 not null;


//7.将sname字段 修改成varchar(10)；
alter table stu modify sname varchar(10);


//8.删除学生表的主键；
alter table stu modify sid int;
alter table stu drop primary key;


//9.给stu表添加主键自增
alter table stu add primary key(sid);
alter table stu modify sid int auto_increment;


//10.查找姓张同学的总数；
select * from stu where sname like '张%';
select count(*) from stu where sname like '张%';
+----------+
| count(*) |
+----------+
|        2 |
+----------+


//11.检索出文章的标题和标签id；
select * from arc as a join arc_tag as at on a.aid = at.aid;
select a.title,at.tid from arc as a join arc_tag as at on a.aid = at.aid;



//12.检索出'你的孤独虽败犹荣'对应的标签名；
select * from arc as a join arc_tag as at on a.aid = at.aid;
//三表建立联系
select * from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid;
//排除掉不要的字段
select a.title,t.tname from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid;
//加一个条件
select a.title,t.tname from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid where a.title = '你的孤独虽败犹荣';
+------------------+------------+
| title            | tname      |
+------------------+------------+
| 你的孤独虽败犹荣               | 刘同          |
| 你的孤独虽败犹荣               | 青春励志         |
| 你的孤独虽败犹荣               | 好看的小说        |
+------------------+------------+


//13.检索出与'你的孤独虽败犹荣'拥有一样标签的文章；
//获取'你的孤独虽败犹荣'对应的标签id
select at.tid from arc as a join arc_tag as at on a.aid = at.aid where a.title = '你的孤独虽败犹荣';
+------+
| tid  |
+------+
|    1 |
|    3 |
|    8 |
+------+

//需要将1,3,8替换成以上的sql语句
select * from arc as a join arc_tag as at on a.aid = at.aid where at.tid in (1,3,8);
+-----+------------------+---------+------+------+
| aid | title            | content | aid  | tid  |
+-----+------------------+---------+------+------+
|   1 | 你的孤独虽败犹荣               | NULL    |    1 |    1 |
|   1 | 你的孤独虽败犹荣               | NULL    |    1 |    3 |
|   1 | 你的孤独虽败犹荣               | NULL    |    1 |    8 |
|   2 | 蓝猫淘气三千问               | NULL    |    2 |    3 |
|   2 | 蓝猫淘气三千问               | NULL    |    2 |    8 |
|   3 | 三国演义                 | NULL    |    3 |    8 |
+-----+------------------+---------+------+------+
//自连接  和以上结果一样
select * from arc as a join arc_tag as at on a.aid = at.aid where at.tid in (select at.tid from arc as a join arc_tag as at on a.aid = at.aid where a.title = '你的孤独虽败犹荣');

//排除掉自己、排除掉不必要的字段
select a.title from arc as a join arc_tag as at on a.aid = at.aid where at.tid in (select at.tid from arc as a join arc_tag as at on a.aid = at.aid where a.title = '你的孤独虽败犹荣') and a.title != '你的孤独虽败犹荣';
+----------------+
| title          |
+----------------+
| 蓝猫淘气三千问             |
| 蓝猫淘气三千问             |
| 三国演义               |
+----------------+

//过滤  (最终结果)
select distinct a.title from arc as a join arc_tag as at on a.aid = at.aid where at.tid in (select at.tid from arc as a join arc_tag as at on a.aid = at.aid where a.title = '你的孤独虽败犹荣') and a.title != '你的孤独虽败犹荣';
+----------------+
| title          |
+----------------+
| 蓝猫淘气三千问             |
| 三国演义               |
+----------------+


//14.检索出每篇文章对应的标签；
select * from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid;
select a.title,t.tname from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid;


15.检索出每个标签对应文章的数量；
//三表关联
select * from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid;
//分组
select * from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid group by t.tname;
//取出标签名和数量
select t.tname,count(*) from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid group by t.tname;
+------------+----------+
| tname      | count(*) |
+------------+----------+
| 古典小说        |        1 |
| 好看的小说        |        3 |
| 科普书籍         |        1 |
| 刘同          |        1 |
| 罗贯中          |        1 |
| 青春励志         |        2 |
| 四大名著          |        1 |
| 幼儿读本          |        1 |
+------------+----------+


16.检索出标签最多的文章；
//三表关联
select * from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid;
//分组
select * from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid group by a.title;
//统计
select a.title,count(*) from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid group by a.title;
//排序并截取
select a.title,count(*) as num from arc as a join arc_tag as at on a.aid = at.aid join tag as t on at.tid = t.tid group by a.title order by num desc limit 1;
+----------+-----+
| title    | num |
+----------+-----+
| 三国演义         |   5 |
+----------+-----+

//两表关联也可以做到
select a.title,count(*) as num from arc as a join arc_tag as at on a.aid = at.aid group by a.title order by num desc limit 1;

17.目前有学生表、班级表，用关联把没有学生的班级获取出来。
select c.cname from stu as s right join class as c on s.cid = c.cid where s.sid is null;
+-------+
| cname |
+-------+
| 五班      |
| 六班      |
+-------+