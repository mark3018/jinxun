<?php
namespace A;
function p(){
	echo 'A-p';
}

//使用命名空间的三种方式
// 1.非限定名称
// p();
// 2.限定名称(类比相对目录理解)
// B\p();
// 3.完全限定名称
// \A\B\p();

namespace A\B;
function p(){
	echo 'B-p';
}

// 3.完全限定名称
// \A\p();


 ?>