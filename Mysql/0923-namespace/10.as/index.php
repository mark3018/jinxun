<?php 
namespace Jinxun;
include './Model.php';
//命名导入，因为都叫Model,所以起一个别名
use \Mark\Model as MarkModel;
class Model{
	public function __construct(){
		echo '静态调用是不会执行的，只有实例化New才会执行';
	}
	public static function connect(){
		echo 'jx-connect';
	}


	public static function index()
	{
		//实例化Ma空间下面的Model类
		$obj = new MarkModel;
		$obj->run();
	}


	public static function display(){
		$obj = new MarkModel;
		$obj->run();
	}


}


Model::display();


 ?>