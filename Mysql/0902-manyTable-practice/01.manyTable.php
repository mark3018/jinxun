<?php


//笛卡尔积 无意义的数据  做法是没有意义的
select * from stu,class;

//正确方法
select * from stu,class where stu.cid = class.cid;
//加上别名
select * from stu as s,class as c where s.cid = c.cid;
select s.sname,c.cname from stu as s,class as c where s.cid = c.cid;


/**
 * 内部关联  检索某个表与其他表相匹配的数据
 * 说白了  就是两张表匹配的数据检索出来
 */
select * from stu inner join class on stu.cid = class.cid;
//缩写
select * from stu join class on stu.cid = class.cid;
//别名
select * from stu as s join class as c on s.cid = c.cid;
select s.sname,c.cname from stu as s join class as c on s.cid = c.cid;
//获取张震化所在的班级
select * from stu as s join class as c on s.cid = c.cid where s.sname = '张震化';
select s.sname,c.cname from stu as s join class as c on s.cid = c.cid where s.sname = '张震化';


/**
 * 左关联  显示左表所有的记录及右表匹配的记录
 *         左边所有数据拿出来  右边拿出匹配的数据
 */
select * from stu left join class on stu.cid = class.cid;
select * from stu as s left join class as c on s.cid = c.cid;


/**
 * 右关联  显示右表所有的记录及左表匹配的记录
 *          右边所有数据拿出来  左边拿出匹配的数据
 */
select * from stu right join class on stu.cid = class.cid;


/**
 * 自连接  得到与王帅在同一个班级的同学名
 */
//1.找cid
select * from stu where sname='王帅';
select cid from stu where sname='王帅';
+------+
| cid  |
+------+
|    2 |
+------+

//2.用上自连接 接着处理学生表  不用操作班级表
select * from stu where cid in (select cid from stu where sname='王帅');
select sname from stu where cid in (select cid from stu where sname='王帅');
select sname from stu where cid = (select cid from stu where sname = '王帅');
+--------+
| sname  |
+--------+
| 王帅      |
| 楚珊      |
| 姚红军      |
+--------+

//3.得到与王帅在一个班级的同学名 排除掉王帅自己
select sname from stu where cid in (select cid from stu where sname='王帅') and sname != '王帅';
+--------+
| sname  |
+--------+
| 楚珊      |
| 姚红军      |
+--------+


/**
 * 内连接  得到与王帅在同一个班级的同学名
 */
select s1.sname,s1.cid,s2.cid,s2.sname from stu as s1 join stu as s2 on s1.cid = s2.cid;
select s1.sname,s1.cid,s2.cid,s2.sname from stu as s1 join stu as s2 on s1.cid = s2.cid where s2.sname='王帅';
select * from stu as s1 join stu as s2 on s1.cid = s2.cid where s2.sname='王帅';
select s1.sname from stu as s1 join stu as s2 on s1.cid = s2.cid where s2.sname='王帅';
+--------+
| sname  |
+--------+
| 王帅      |
| 楚珊      |
| 姚红军      |
+--------+

//得到与王帅在一个班级的同学名 排除掉王帅自己
select s1.sname from stu as s1 join stu as s2 on s1.cid = s2.cid where s2.sname='王帅' and s1.sname !='王帅';
+--------+
| sname  |
+--------+
| 楚珊      |
| 姚红军      |
+--------+


