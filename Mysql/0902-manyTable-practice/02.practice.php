<?php

//1.向stu表中插入两条数据；
insert into stu (sname,age) values (),();


//2.将stu表中id=10的数据：性别改为女，年龄改为20；
update stu set sex='女',age=20 where sid=10;


//3.将年龄小于30岁的同学年龄+1；
update stu set age=age+1 where age<30;


//4.删除年龄最小的同学；
delete from stu order by age asc limit 1;


//5.学生表中随机获取五条数据；
select * from stu order by rand() limit 5;
