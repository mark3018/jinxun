<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script type="text/javascript" src="./static/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#sendCaptcha').click(function(){
				var email = $.trim($('[name=email]').val());
				//邮箱不能为空
				if(email == ''){
					alert('邮箱不能为空');
					return;
				}
				//邮箱格式必须正确
				//123456@qq.com
				//123456@163.com
				//mark-123@126.com
				//mark_123@126.com
				//mark@sina.com.cn
				var reg = /^[0-9a-z][0-9a-z-_]+[0-9a-z]@[0-9a-z]+\.(com|cn|net|com\.cn)$/i;
				if(!reg.test(email)){
					alert('邮箱格式不正确');
					return;
				}

				//发送验证码邮件了
				$.ajax({
					url:'./index.php?a=sendCaptcha',
					data:{email},
					dataType:'json',
					type:'post',
					success:function(response){
						if(response.errno != 0){
							alert(response.errmsg);
						}else{
							alert('发送成功，请稍后注意查收！有可能在垃圾箱');
						}
					}
				});

				var _this = $(this);
				//把按钮禁用
				_this.attr('disabled',true);
				var countNum = 60;
				_this.html(countNum + '秒后再发送');
				//启动定时器
				var timer = setInterval(function(){
					countNum--;
					if(countNum <= 0){
						clearInterval(timer);
						_this.html('获取验证码');
						_this.attr('disabled',false);
					}else{
						_this.html(countNum + '秒后再发送');
					}
					
				},1000);


			})
		})
	</script>
</head>
<body>
	<form action="?a=reg" method="post">
		邮箱： <input type="email" name="email"> <button type="button" id="sendCaptcha">获取验证码</button>
		<br>
		密码： <input type="password" name="password" id="">
		<br>
		邮箱验证码：<input type="text" name="captcha" id="">
		<br>
		<input type="submit" value="注册">
	</form>
</body>
</html>