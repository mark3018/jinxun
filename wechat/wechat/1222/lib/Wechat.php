<?php 
class Wechat{
	//验证服务器用的token
	private static $token = 'fuzayixie';
	//appid和appsecret
	private static $appid = 'wxa65ca9858dea7f3c';
	private static $appsecret = 'adad3c41fc55d6134ae0ce415541f7ac';
	

	public static function valid(){
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];
		$token = self::$token;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		if( $tmpStr == $signature ){
			echo $_GET["echostr"];
		}
	} 


	public static function getWechatObj(){
		//微信服务器返回给业务服务器的xml字符串
		$xml = file_get_contents('php://input');
// 		$xml = <<<str
// <xml><ToUserName><![CDATA[gh_3f02ef8afa17]]></ToUserName>
// <FromUserName><![CDATA[oQrB-5ntB_KznEmVPdffPxt5AVg4]]></FromUserName>
// <CreateTime>1545313251</CreateTime>
// <MsgType><![CDATA[event]]></MsgType>
// <Event><![CDATA[CLICK]]></Event>
// <EventKey><![CDATA[jrgq]]></EventKey>
// </xml>
// str;
		if($xml){
			// out($xml);
			//把xml变成更加容易操作的类型，因为我们想拿其中类似信息的内容
			return simplexml_load_string($xml,'simpleXMLElement',LIBXML_NOCDATA);

			// out($obj->Content);
		}
	} 

	//判断用户是否是订阅
	public static function isSubscribe(){
		$obj = self::getWechatObj();
		if($obj->MsgType == 'event'){
			if($obj->Event == 'subscribe'){
				return true;
			}
		}
		return false;
	}


	//回复文本消息
	public static function replyMsg($msg){
		$obj = self::getWechatObj();
		//刚才是⽤户发给我们，现在是我们发给⽤户，所以反⼀下
		//我们变成发送者FromUserName，⽤户变为接收者ToUserName
		$FromUserName = $obj->ToUserName;
		$ToUserName = $obj->FromUserName;
		$CreateTime = time();
		$MsgType = 'text';
		$Content = $msg;
		//3.组合要回复的模板
		$template = <<<str
<xml>
<ToUserName><![CDATA[{$ToUserName}]]></ToUserName>
<FromUserName><![CDATA[{$FromUserName}]]></FromUserName>
<CreateTime>{$CreateTime}</CreateTime>
<MsgType><![CDATA[{$MsgType}]]></MsgType>
<Content><![CDATA[{$Content}]]></Content>
</xml>
str;
		echo $template;exit;
	}


	//回复图文消息
	public static function replyNews($data){
		$obj = self::getWechatObj();
		//刚才是⽤户发给我们，现在是我们发给⽤户，所以反⼀下
		//我们变成发送者FromUserName，⽤户变为接收者ToUserName
		$FromUserName = $obj->ToUserName;
		$ToUserName = $obj->FromUserName;
		$CreateTime = time();
		$MsgType = 'news';
		//设置回复内容
		$template = "<xml>
		<ToUserName><![CDATA[{$ToUserName}]]></ToUserName>
		<FromUserName><![CDATA[{$FromUserName}]]></FromUserName>
		<CreateTime>{$CreateTime}</CreateTime>
		<MsgType><![CDATA[{$MsgType}]]></MsgType>
		<ArticleCount>".count($data)."</ArticleCount>
		<Articles>";

		//循环数据，组合xml
		foreach ($data as $v) {
			$template .= "<item>
			<Title><![CDATA[{$v['title']}]]></Title>
			<Description><![CDATA[{$v['description']}]]></Description>
			<PicUrl><![CDATA[{$v['picurl']}]]></PicUrl>
			<Url><![CDATA[{$v['url']}]]></Url>
			</item>";
		}
		$template .= "</Articles></xml>";
		exit($template);
	}


	//获得用户输入的内容
	public static function getContent(){
		$obj = self::getWechatObj();
		return trim($obj->Content);
	}

	//获取用户点击按钮的Key
	public static function getKey(){
		$obj = self::getWechatObj();
		return $obj->EventKey;
	} 


	public static function getAccessToken(){
		//保存access_token的缓存文件
		$atFile = './cache/at.php';
		//如果存access_token的文件存在并且没有过期
		if(is_file($atFile) && filemtime($atFile) + 7200 > time()){
			$atArr = include $atFile;
			return $atArr['access_token'];
		}
		//请求地址
		$url = "https://api.weixin.qq.com/cgi-bin/token";
		//获取access_token填写client_credential
		$grant_type = 'client_credential';
		//第三⽅⽤户唯⼀凭证
		$appid = self::$appid;
		//第三⽅⽤户唯⼀凭证密钥，即appsecret
		$secret = self::$appsecret;
		//最终地址
		$url .= "?grant_type={$grant_type}&appid={$appid}&secret={$secret}";
		//请求微信服务器要回复access_token
		$json = file_get_contents($url);
		//把返回的json转为数组
		$arr = json_decode($json,true);

		file_put_contents($atFile, "<?php return " . var_export($arr,true) . ";");
		return $arr['access_token'];
	}

	//创建菜单
	public static function createMenu($data){
		$curl = new Curl\Curl();
		$url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . self::getAccessToken();
		$result = $curl->post($url, $data);
		return json_decode($result->response,true);
	}


	public static function getMenu(){
		$url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" . self::getAccessToken();
		$result = (new Curl\Curl)->get($url);
		//把json转为数组
		$menuArr = json_decode($result->response,true);
		return $menuArr;
	}

	//获取有效的jsapi_ticket（通过接⼝调⽤）
	public static function getJsApiTicket(){
		$url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='.self::getAccessToken().'&type=jsapi';
		$result = (new Curl\Curl)->get($url);
		$data = json_decode($result->response,true);
		return $data['ticket']; 
	}

}


 ?>