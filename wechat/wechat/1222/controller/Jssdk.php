<?php 
class Jssdk{
	public function index(){
		$noncestr = substr(md5(microtime()), 0,20);
		//jsapi_ticket
		$jsApiTicktet = Wechat::getJsApiTicket();
		//当前时间戳
		$time = time();
		//当前地址
		$url = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('\\', '/',$_SERVER['REQUEST_URI']);
		$str = "jsapi_ticket={$jsApiTicktet}&noncestr={$noncestr}&timestamp={$time}&url={$url}";
		//sha1加密
		$signature = sha1($str); 

		$data = [
			//appid
			'appId'=>'wxa65ca9858dea7f3c',
			//时间戳
			'timestamp'=>$time,
			//生成签名的随机字符串
			'nonceStr'=>$noncestr,
			//签名字符串	
			'signature'=>$signature
		];
		
		include "./view/jssdk/index.php";
	}
}


 ?>