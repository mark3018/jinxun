<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
	<script type="text/javascript">
		wx.config({
			debug: true, // 开启调试模式,调⽤的所有api的返回值会在客户端alert出来，若要查看传⼊的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			appId: '<?php echo $data['appId'] ?>', // 必填，公众号的唯⼀标识
			timestamp: <?php echo $data['timestamp'] ?>, // 必填，⽣成签名的时间戳
			nonceStr: '<?php echo $data['nonceStr'] ?>', // 必填，⽣成签名的随机串
			signature: '<?php echo $data['signature'] ?>',// 必填，签名，⻅附录1
			jsApiList: ['chooseImage'] // 必填，需要使⽤的JS接⼝列表，所有JS接⼝列表⻅附录2
		});
		function choose(){
			wx.chooseImage({
				count: 1, // 默认9
				sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
				sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
				success: function (res) {
					var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
					console.log(localIds);
				}
			});
		}
	</script>
</head>
<body>
	我是一个测试
	<br>
	<a href="javascript:choose();">选择照片</a>
</body>
</html>