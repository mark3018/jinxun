<?php 
include "./helper.php";
require "./Wechat.php";

//调用微信服务器的ip
$accessToken = Wechat::getAccessToken();
$url = "https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=";
$url .= $accessToken;
//调取接⼝
$data = file_get_contents($url);
$data = json_decode($data);
//输出ip地址，微信服务器很多ip地址
foreach ($data->ip_list as $ip) {
	echo $ip . '<br/>';
}
exit;


//微信与业务服务器验证
Wechat::valid();

//如果用户是订阅
if(Wechat::isSubscribe()){
	//给用户回复订阅之后的信息
	Wechat::replyMsg('欢迎你订阅我！');
	
}

//关键词回复
if(Wechat::getContent() == '华为'){
	//模拟数据
	$data = [
		[
			'title'=>'华为事件后，这些美国公司比中国还急',
			'description'=>'前不久的华为事件不仅牵动着中国民众的注意力，也让大洋彼岸的不少美国企业心惊肉跳。虽然美国政府希望借此对华为的发展施压，但是这种出格的做法在美国国内也引起越来越大的争议。',
			//图⽚地址
			'picurl' => 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1545144050860&di=f0bad2ac798302e0530f5dd1675343af&imgtype=0&src=http%3A%2F%2Fytsports.cn%2Fstatic_new%2Fstatic%2Fnewsimage%2F20150726%2F14378853613679.jpg',
			//跳转地址
			'url' => 'https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&rsv_idx=1&tn=baidu&wd=%E5%8D%8E%E4%B8%BA&oq=%25E5%258D%258E%25E4%25B8%25BA&rsv_pq=88e1442200049116&rsv_t=eb73Sjz41huVMJzrF7cH2BAZrmLMvYit%2BMJRPCKufOO3e4v7bVQol7Nr4v8&rqlang=cn&rsv_enter=0',
		],
		[
			'title'=>'华为事件后，这些美国公司比中国还急',
			'description'=>'前不久的华为事件不仅牵动着中国民众的注意力，也让大洋彼岸的不少美国企业心惊肉跳。虽然美国政府希望借此对华为的发展施压，但是这种出格的做法在美国国内也引起越来越大的争议。',
			//图⽚地址
			'picurl' => 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1545144050860&di=f0bad2ac798302e0530f5dd1675343af&imgtype=0&src=http%3A%2F%2Fytsports.cn%2Fstatic_new%2Fstatic%2Fnewsimage%2F20150726%2F14378853613679.jpg',
			//跳转地址
			'url' => 'https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&rsv_idx=1&tn=baidu&wd=%E5%8D%8E%E4%B8%BA&oq=%25E5%258D%258E%25E4%25B8%25BA&rsv_pq=88e1442200049116&rsv_t=eb73Sjz41huVMJzrF7cH2BAZrmLMvYit%2BMJRPCKufOO3e4v7bVQol7Nr4v8&rqlang=cn&rsv_enter=0',
		],
	];
	Wechat::replyNews($data);
}


//默认回复
Wechat::replyMsg('您好，您的消息已收到，我们会尽快回复您');




 ?>