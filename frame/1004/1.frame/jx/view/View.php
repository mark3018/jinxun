<?php 
namespace jx\view;
//表类
class View{
	public function __call($name,$args){
		//要执行Base对象中的$name方法，并且传递$args参数
		call_user_func_array([new Base,$name], $args);
	}

	//当调用不存在的静态方法会自动触发，$name就是方法名，$args就是调用方法传入的参数，如果没有参数那默认就是空数组
	public static function __callStatic($name,$args){
		call_user_func_array([new Base,$name], $args);
	}
}


 ?>