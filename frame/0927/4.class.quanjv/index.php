<?php 
namespace JX;
include './Model.php';
//类不会像函数那样：先在当前空间寻找，没有再去全局空间寻找
//它只会在当前空间寻找，如果没有，就报错了
// Model::run();
//调用全局空间的Model里面的run方法
\Model::run();


 ?>