<?php 
class App{
	public static function run(){
		//在当前类注册一个自动载入方法
		spl_autoload_register([__CLASS__,'auto']);
		$arc = new \JX\Arc;	
		$arc->run();

	}


	//在当前类注册一个拥有自动载入功能的方法
	//当前方法其实就和__autoload功能是一样的
	public static function auto($className){
		$className = str_replace('\\', '/', $className);
		// echo  $className . '.php';exit;
		include $className . '.php';
	}

}
App::run();


 ?>