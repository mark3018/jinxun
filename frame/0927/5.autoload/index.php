<?php 
//当使用未找到的类，会自动触发次方法，并且把类名传入
function __autoload($className){
	include $className . '.php';
}

// $obj = new Arc;
// $obj->show();


//静态调用一个未找到的类，也会触发__autoload
// Arc::delete();


//继承一个未找到的类，也会触发__autoload
class A extends Arc{
	public function test(){
		echo 'test';
	}
}
(new A)->test();






 ?>