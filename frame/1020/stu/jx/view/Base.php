<?php
namespace jx\view;
//基类
class Base{
	private $var = [];
	private $htmlPath;

	public function make(){
		//组合模板路径
		$this->htmlPath = "../app/".MODULE."/view/".CONTROLLER."/".ACTION.".php";
		return $this;
	}


	public function with($data = []){
		$this->var = $data;
		return $this;
	}

	
	public function __toString(){
		extract($this->var);
		include $this->htmlPath;
		return '';
	}
}