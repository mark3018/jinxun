<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="./static/bootstrap-3.3.7-dist/css/bootstrap.min.css">
</head>
<body>
	<div class="container" style="margin-top: 20px;">
		<div class="panel panel-info">
		  <div class="panel-heading">
		    <h3 class="panel-title">学生列表</h3>
		  </div>
		  <div class="panel-body">
		   		<table class="table table-hover">
				  	<tr>
				  		<th>id</th>
				  		<th>头像</th>
				  		<th>姓名</th>
				  		<th>性别</th>
				  		<th>班级</th>
				  		<th>操作</th>
				  	</tr>
					  	<?php foreach($students as $student): ?>
					  	<tr>
					  		<td><?php echo $student['sid'] ?></td>
					  		<td><?php echo $student['profile'] ?></td>
					  		<td><?php echo $student['sname'] ?></td>
					  		<td><?php echo $student['sex'] ?></td>
					  		<td><?php echo $student['gname'] ?></td>
					  		<td><a href="<?php echo url('show') ?>&sid=<?php echo $student['sid'] ?>" class="btn btn-info btn-sm">查看</a></td>
					  	</tr>
				  <?php endforeach ?>
				</table>
		  </div>
		</div>
	</div>
</body>
</html>