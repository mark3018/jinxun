<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="./static/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<script type="text/javascript" src="./static/js/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			//获得图片src地址
			var src = $('#captcha').attr('src');
			//点击图片
			$('#captcha,#kbq').click(function(){
				//设置图片地址，让图片地址随机
				$('#captcha').attr('src',src + '&mt=' + Math.random());
			})
		})
	</script>
</head>
<body>
<form action="" method="post">
	<div class="container" style="width: 300px;margin-top: 50px;">
  <div class="form-group">
    <label for="exampleInputEmail1">用户名</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="username">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">密码</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">验证码</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name="captcha" placeholder="">
  
    <img id="captcha" src="<?php echo url('captcha') ?>">
    <a id="kbq" href="javascript:;">看不清，换一张</a>
  </div>

  <div class="checkbox">
    <label>
      <input type="checkbox"> 7天之内记住我
    </label>
  </div>
  <button type="submit" class="btn btn-block btn-info">登录</button>

</form>
</div>
</body>
</html>