<?php 
namespace app\home\controller;
//命名导入
use jx\view\View;
use jx\core\Controller;
//入口控制器
class EntryController extends Controller{
	//默认方法
	public function index(){
		$name = 'Mark';
		//dd是自定义函数
		// dd($name);

		//?s=home/category/index
		// echo url('home/category/index');
		// echo url('category/index');
		// echo url('add');exit;




		return View::make()->with(compact('name'));
	}


	public function add(){
		if(IS_POST){
			// dd('是POST提交');
			//默认回退
			// $this->message('提交成功');
			 //提示完之后按照用户设置的地址跳转
			 $this->setRedirect(url('index'))->message('提交成功');

		}

		return View::make();
	}

	
	

}


 ?>
