<?php 
namespace app\home\controller;
use jx\core\Controller;
use system\model\Article;
use system\model\Category;
use jx\view\View;

class ArcController extends Controller{
	
	public function show(){
		// $data = Article::get();
		$data = Article::where("aid>3")->get();
		return View::with(compact('data'))->make();
	}
}


 ?>