<?php 
#目的
//1.提升编程思维能力
//2.明白框架原理以及怎么使用框架


#要求
//1.框架运行的流程要明白
//2.多加注释
//3.能默打框架（难度比较大，尽量尝试）


#需要用到的知识
//1.PHP
//2.MYSQL
//3.composer依赖管理工具(需要提前配置)，下载A软件需要依赖B软件


#框架要实现的功能
//1.MVC设计模式
//2.数据库的ORM操作
//3.加载使用第三方的类库（软件包）验证码


#实现的步骤
//1.构建自己的框架
//2.再使用自己的框架做一个小项目（学生管理系统）

#构建的步骤
//建立框架的结构目录



//1.在jx/core建立Boot.php和functions.php
//2.通过命令行进入到项目目录，composer init
//3.更改composer.json，设置自动载入
//4.在命令行执行composer dump，于是就生成了一个vendor
//5.在public目录建立index.php，载入autoload.php
//6.调用Boot类里面的run方法，让框架的启动类执行
//7.在Boot类里面的run方法初始化框架
//8.让应用app执行（app/home/controller）建立了Homecontroller.php，在里面建立了index方法。





 