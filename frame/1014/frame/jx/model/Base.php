<?php 
namespace jx\model;
use PDO;
class Base{
	private $table;
	private static $pdo = NULL;
	//某些时候没有where条件
	private $where = '';


	public function __construct($config,$table){
		$this->table = $table;
		$this->connect($config);
	}


	private function connect($config){
		//如果不为空，那么证明已经实例化过PDO，连接过Mysql了
		if(!is_null(self::$pdo)) return;
		try{
			$dsn ="mysql:host={$config['db_host']};dbname={$config['db_name']}";
			$user = $config['db_user'];
			$password = $config['db_password'];
			$pdo = new PDO($dsn,$user,$password);
			//设置错误类型
			$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			//设置编码
			$pdo->query("SET NAMES " . $config['db_charset']);
			//把pdo对象交给静态属性
			self::$pdo = $pdo;
		}catch(PDOException $e){
			exit($e->getMessage());
		}

		



	}

	public function where($where){
		$this->where = " WHERE {$where}";
		return $this;
	}

	//获得全部数据
	public function get(){
		$sql = "SELECT * FROM {$this->table} {$this->where}";
		return $this->q($sql);
	}


	public function q($sql){
		try{
			$result = self::$pdo->query($sql);
			$data = $result->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}catch(PDOException $e){
			exit($e->getMessage());
		}
	}
}


 ?>