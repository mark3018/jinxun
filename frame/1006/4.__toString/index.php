<?php 
class Base{
	//要给模板传入的变量
	private $var = [];
	//模板视图的路径
	private $htmlPath;

	public function make(){
		$this->htmlPath = './show.php';
		return $this;
	}

	public function with($data){
		$this->var = $data;
		return $this;
	}

	//当输出对象的时候会自动触发，此方法必须return 一个字符串
	//有了这个方法，with和make就可以随意的调换顺序
	public function __toString(){
		//拆解变量
		extract($this->var);
		//载入视图
		include $this->htmlPath;
		//此方法必须return 一个字符串
		return '';
	}
}

$name = 'Mark';
echo (new Base)->make()->with(compact('name'));


 ?>