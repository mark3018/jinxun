<?php 
include "Base.php";
class View{
	public function __call($name,$params){
		call_user_func_array([new Base,$name], $params);
	}

	public static function __callStatic($name,$params){
		//因为未来传递多少个参数不确定，所以使用call_user_func_array形式
		call_user_func_array([new Base,$name], $params);
	}
}


View::make(1,2,3);

 ?>