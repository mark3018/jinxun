<?php 
class Base{
	//类的内部的多个方法可以共用此属性
	private $var;

	//载入视图
	public function make(){
		//把数组键名变成变量名
		//把数组的键值变成变量值
		//相当于$n = 'Mark'; $s = '男';
		extract($this->var);
		include './show.php';
	}

	//给视图分配变量
	public function with($data){
		$this->var = $data;
		return $this;
	}
}


 ?>