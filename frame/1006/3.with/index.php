<?php 
include "Base.php";
class View{

	public static function __callStatic($name,$params){
		//因为未来传递多少个参数不确定，所以使用call_user_func_array形式
		return call_user_func_array([new Base,$name], $params);
	}
}


$name = 'Mark';
$sex = '男';
//可以把单个或者多个变量变成数组（把变量名作为键名，把变量值作为键值）
// $var = compact('name','sex');
// print_r($var);
View::with(compact('name','sex'))->make();

 ?>