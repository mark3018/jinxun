<?php 
include "Base.php";
class View{

	public static function __callStatic($name,$params){
		//因为未来传递多少个参数不确定，所以使用call_user_func_array形式
		return call_user_func_array([new Base,$name], $params);
	}
}


$name = 'Mark';
$sex = '男';
View::with(['n'=>$name,'s'=>$sex])->make();

 ?>