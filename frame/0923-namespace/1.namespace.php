<?php
namespace A;
function p($var){
	echo '<pre>' . print_r($var,true) . '</pre>';
}
// p(11111);
\B\p(4444);

namespace B;
function p($var){
	echo '<pre style="background:grey">' . print_r($var,true) . '</pre>';
}
// p(222222);
//在B空间调用A空间的p函数
// \A\p(3333);