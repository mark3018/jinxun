<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include "../app/admin/view/public/header.php" ?>
<div class="container">
	<div class="row">
		 <?php include "../app/admin/view/public/left.php" ?>
		 <div class="col-md-9">
		
		 	<div class="panel panel-default" style="margin-top: 5px;">
			  <div class="panel-heading">
			    <h3 class="panel-title">素材列表</h3>
			  </div>
			  <div class="panel-body">
			    	<table class="table table-hover">
			    		<tr>
			    			<td>ID</td>
			    			<td>图片</td>
			    			<td>地址</td>
			    			<td>创建时间</td>
			    			<td>操作</td>
			    		</tr>
			    		<?php foreach($attachments as $attachment): ?>
				    		<tr>
				    			<td><?php echo $attachment['aid'] ?></td>
				    			<td><img width="100" src="<?php echo $attachment['path'] ?>"></td>
				    			<td><?php echo $attachment['path'] ?></td>
				    			<td><?php echo date('Y-m-d H:i:s',$attachment['createtime']) ?></td>
				    			<td>
				    				<a class="btn btn-danger btn-xs" href="javascript:if(confirm('确认删除吗')) location.href='<?php echo url('delete') ?>&aid=<?php echo $attachment['aid'] ?>';">删除</a>
				    			</td>
				    		</tr>
			    		<?php endforeach ?>
			    		
			    	</table>
			  </div>
			</div>
		 </div>
	</div>
</div>
</body>
</html>