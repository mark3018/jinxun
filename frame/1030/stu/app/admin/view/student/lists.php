<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include "../app/admin/view/public/header.php" ?>
<div class="container">
	<div class="row">
		 <?php include "../app/admin/view/public/left.php" ?>
		 <div class="col-md-9">
		 	<a href="<?php echo url('store') ?>" class="btn btn-info">添加学生</a>
		 	<div class="panel panel-default" style="margin-top: 5px;">
			  <div class="panel-heading">
			    <h3 class="panel-title">学生列表</h3>
			  </div>
			  <div class="panel-body">
			    	<table class="table table-hover">
			    		<tr>
			    			<td>ID</td>
			    			<td>名字</td>
			    			<td>班级</td>
			    			<td>操作</td>
			    		</tr>
			    		<?php foreach ($students as $student): ?>
				    		<tr>
				    			<td><?php echo $student['sid'] ?></td>
				    			<td><?php echo $student['sname'] ?></td>
				    			<td><?php echo $student['gname'] ?></td>
				    			<td>
				    				<a href="<?php echo url('update') ?>&sid=<?php echo $student['sid'] ?>" class="btn btn-primary btn-xs">编辑</a>
				    				<a href="javascript:if(confirm('确定吗')) location.href='<?php echo url('delete') ?>&sid=<?php echo $student['sid'] ?>';" class="btn btn-danger btn-xs">删除</a>
				    			</td>
				    		</tr>
			    		<?php endforeach ?>
			    	</table>
			  </div>
			</div>
		 </div>
	</div>
</div>
</body>
</html>