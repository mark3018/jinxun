<?php 
namespace app\admin\controller;
use jx\view\View;
use jx\core\Controller;
use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;
use system\model\User;

class LoginController extends Controller{
	//登录
	public function login(){
		
		
		if(IS_POST){
			//1.先比对验证码
			$captcha = post('captcha');
			if(strtolower($captcha) != $_SESSION['captcha']){
				return $this->setRedirect(url('login'))->message('验证码错误');
			}
			//2.判断用户名是否存在
			$username = post('username');
			$user = User::where("username='{$username}'")->pluck();
			if(!$user){
				return $this->setRedirect(url('login'))->message('用户名不存在');
			}

			//3.密码是否正确
			if($user['password'] != md5(post('password'))){
				return $this->setRedirect(url('login'))->message('密码错误');
			}

			//记录登录的状态
			// dd($user);
			$_SESSION['user'] = [
				'username' => $user['username'],
				'uid' => $user['uid']
			];
			//7天免登录
			if(isset($_POST['autologin'])){//用户勾选了7天免登录
				setcookie(session_name(),session_id(),time() + 3600 * 24 * 7,'/');
			}else{
				setcookie(session_name(),session_id(),0,'/');
			}
			
			return $this->setRedirect(url('entry/index'))->message('登录成功');

		}
		return View::make();
	}

	//退出登录
	public function logout(){
		session_unset();
		session_destroy();
		return $this->setRedirect(url('login'))->message('退出成功');
	}


	//显示验证码
	public function captcha(){
		header('Content-type: image/jpeg');
		
		$phraseBuilder = new PhraseBuilder(4);
		$builder = new CaptchaBuilder(null, $phraseBuilder);

		$builder->build();
		//存入session为了比对
		$_SESSION['captcha'] = strtolower($builder->getPhrase());
		$builder->output();
	}
}



 ?>