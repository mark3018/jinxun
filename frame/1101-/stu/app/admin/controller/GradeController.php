<?php 
namespace app\admin\controller;
use jx\view\View;
use jx\core\Controller;
use jx\model\Model;

class GradeController extends CommonController{
	//班级列表
	public function lists(){
		$sql = "SELECT * FROM grade";
		$grades = Model::q($sql);
		//判断是否有班级数据
		if(!$grades){
			return $this->setRedirect(url('store'))->message('请先添加班级');
		}
		return View::make()->with(['grades'=>$grades]);
	}

	//添加班级
	public function store(){
		if(IS_POST){
			$gname = post('gname');
			$sql = "INSERT INTO grade SET gname='$gname'";
			Model::e($sql);
			return $this->setRedirect(url('store'))->message('添加成功');
		}
		return View::make();
	}

	//修改
	public function update(){
		$gid = (int)$_GET['gid'];
		if(IS_POST){
			$gname = post('gname');
			$sql = "UPDATE grade SET gname='{$gname}' WHERE gid={$gid}";
			Model::e($sql);
			return $this->setRedirect(url('lists'))->message('修改成功');
		}

		
		$sql = "SELECT * FROM grade WHERE gid={$gid}";
		$grade = Model::q($sql);
		return View::make()->with(compact('grade'));
	}

	//删除
	public function delete(){
		$gid = (int)$_GET['gid'];
		//先判断班级下面是否有学生
		$sql = "SELECT * FROM student WHERE gid={$gid}";
		if(Model::q($sql)){
			return $this->message('请先删该班级下面的学生');
		}

		//删除班级
		$sql = "DELETE FROM grade WHERE gid=" . $gid;
		Model::e($sql);
		return $this->message('删除成功');
	}
}

 ?>