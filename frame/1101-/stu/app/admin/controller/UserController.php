<?php 
namespace app\admin\controller;
use jx\view\View;
use jx\core\Controller;
use jx\model\Model;

class UserController extends CommonController{

	//修改密码
	public function changePassword(){
		if(IS_POST){
			$oldPassword = post('old_password');
			$password = trim(post('password'));
			$confimationPassword = post('confirmation_password');
			//1.先判断新密码是否够6位
			if(strlen($password) < 6){
				return $this->message('新密码不得少于6位');
			}
			//2.再判断两次密码是否一致

			if($password != $confimationPassword){
				return $this->message('两次密码不一致');
			}
			//3.判断旧密码是否正确
			$sql = "SELECT * FROM user WHERE uid={$_SESSION['user']['uid']}";
			$data  = Model::q($sql);
			if(md5($oldPassword) != $data[0]['password']){
				return $this->message('旧密码错误');
			}

			//4.修改密码
			$sql = "UPDATE user SET password='" . md5($password) . "' WHERE uid={$_SESSION['user']['uid']}";
			Model::e($sql);
			return $this->setRedirect(url('login/logout'))->message('修改成功');

		}
		return View::make();
	}
}




 ?>