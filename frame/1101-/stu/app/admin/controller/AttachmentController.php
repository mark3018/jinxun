<?php 
namespace app\admin\controller;
use jx\view\View;
use jx\core\Controller;
use jx\model\Model;

class AttachmentController extends CommonController{
	public function lists(){
		$attachments = Model::q("SELECT * FROM attachment");
		return View::make()->with(compact('attachments'));
	}


	public function delete(){
		$aid = intval($_GET['aid']);
		$attachment = Model::q("SELECT * FROM attachment WHERE aid=$aid");
		//删除文件
		is_file($attachment[0]['path']) && unlink($attachment[0]['path']);
		//删除数据库数据
		Model::e("DELETE FROM attachment WHERE aid={$aid}");
		return $this->setRedirect(url('lists'))->message('删除成功'); 
	}
}


 ?>