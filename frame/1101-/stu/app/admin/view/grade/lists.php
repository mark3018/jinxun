<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include "../app/admin/view/public/header.php" ?>
<div class="container">
	<div class="row">
		 <?php include "../app/admin/view/public/left.php" ?>
		 <div class="col-md-9">
		 	<a href="<?php echo url('store') ?>" class="btn btn-info">添加班级</a>
		 	<div class="panel panel-default" style="margin-top: 5px;">
			  <div class="panel-heading">
			    <h3 class="panel-title">班级列表</h3>
			  </div>
			  <div class="panel-body">
			    	<table class="table table-hover">
			    		<tr>
			    			<td>ID</td>
			    			<td>班级名称</td>
			    			<td>操作</td>
			    		</tr>
			    		<?php foreach($grades as $grade): ?>
				    		<tr>
				    			<td><?php echo $grade['gid'] ?></td>
				    			<td><?php echo $grade['gname'] ?></td>
				    			<td>
				    				<a href="<?php echo url('update') ?>&gid=<?php echo $grade['gid'] ?>" class="btn btn-success btn-xs">编辑</a>
				    				<a href="javascript:if(confirm('确定删除吗')) location.href='<?php echo url('delete'); ?>&gid=<?php echo $grade['gid'] ?>';" class="btn btn-danger btn-xs">删除</a>
				    			</td>
				    		</tr>
			    		<?php endforeach ?>
			    	</table>
			  </div>
			</div>
		 </div>
	</div>
</div>
</body>
</html>