<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include "../app/admin/view/public/header.php" ?>
<div class="container">
	<div class="row">
		 <?php include "../app/admin/view/public/left.php" ?>
		 <div class="col-md-9">
		 
		 	<div class="panel panel-default" style="margin-top: 5px;">
			  <div class="panel-heading">
			    <h3 class="panel-title">编辑学生</h3>
			  </div>
			  <div class="panel-body">
			    	<form action="" enctype="multipart/form-data" method="post">
					  <div class="form-group">
					    <label for="exampleInputEmail1">名字</label>
					    <input type="text" class="form-control" value="<?php echo $student['sname'] ?>" name="sname">
					  </div>
					   <div class="form-group">
					    <label for="exampleInputEmail1">所属班级</label>
					   <select class="form-control" name="gid">
					   		<option>请选择班级</option>
					   		<?php foreach($grades as $grade): ?>
						  		<option <?php if($student['gid']==$grade['gid']): ?>selected<?php endif ?> value="<?php echo $grade['gid'] ?>"><?php echo $grade['gname'] ?></option>
						  	<?php endforeach ?>
						</select>
					  </div>

					   <div class="form-group">
					    <label for="exampleInputEmail1">头像</label>
					    <input type="file" class="form-control" id="exampleInputEmail1" name="profile">
					    <!-- 保存点击素材之后的素材地址 -->
					    <input type="hidden" name="profileHidden" value="<?php echo $student['profile'] ?>">
					    <br>
				
					    <?php foreach($attachments as $attachment): ?>
					    	<img class="img-thumbnail attachment" width="100" src="<?php echo $attachment['path'] ?>"  <?php if($student['profile'] == $attachment['path'] ): ?>style="border:1px solid red"<?php endif ?> >
					    <?php endforeach ?>
					    	<img class="img-thumbnail" width="100"  src="./attachment/default.jpg" <?php if($student['profile'] == './attachment/default.jpg' ): ?>style="border:1px solid red"<?php endif ?>>
					    <script type="text/javascript">
					    	$(function(){
					    		//点击素材
					    		$('.attachment').click(function(){
					    			//选中效果
					    			$(this).css('border','1px solid red').siblings().css('border','1px solid #ddd');
					    			//获得选中素材的地址
					    			var src = $(this).attr('src');
					    			//放到隐藏域
					    			$('[name=profileHidden]').val(src);
					    		})
					    	})
					    </script>
					  </div>
					    <div class="form-group">
					    <label for="exampleInputEmail1">性别</label>
					    <br>
					   <label class="radio-inline">
						  <input type="radio" name="sex" id="inlineRadio1" value="男" <?php if($student['sex'] == '男'): ?> checked <?php endif ?> > 男
						</label>
						<label class="radio-inline">
						  <input type="radio" name="sex" id="inlineRadio2" value="女" <?php if($student['sex'] == '女'): ?> checked <?php endif ?> > 女
						</label>
					
					  </div>
					  	<div class="form-group">
						    <label for="exampleInputEmail1">生日</label>
						    <input type="date" class="form-control" id="exampleInputEmail1" name="birthday" value="<?php echo $student['birthday'] ?>">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">自我介绍</label>
						   <textarea class="form-control" rows="5" name="introduce"><?php echo $student['introduce'] ?></textarea>
						  </div>
					 	<input type="submit" class="btn btn-primary" value="编辑学生">
					  	<a href="<?php echo url('lists'); ?>" class="btn btn-success">返回列表</a>
					</form>
			 
			</div>
		 </div>
	</div>
</div>
</body>
</html>