<?php
namespace jx\core;
class Controller{


	private $url = 'window.history.back()';


	//设置重定向（设置跳转地址）
	protected function setRedirect($url){
		$this->url = "location.href='{$url}'";
		return $this;
	}

	//消息提示
	protected function message($msg){
		include "./view/message.php";
		exit;
	}

}