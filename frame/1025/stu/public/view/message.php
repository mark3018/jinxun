<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="./static/bootstrap-3.3.7-dist/css/bootstrap.min.css">
</head>
<body>
	<div class="jumbotron">
  <h1><?php echo $msg ?></h1>
  <p>
  		<a href="javascript:<?php echo $this->url; ?>" class="btn btn-warning btn-sm">如果还未跳转，请点击我跳转！</a>
  </p>
  <script type="text/javascript">
  		// 在1.5秒之后跳转
  		setTimeout(function(){
  			<?php echo $this->url; ?>
  		},1500);
  </script>
</div>
</body>
</html>