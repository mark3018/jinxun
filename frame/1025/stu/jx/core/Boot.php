<?php 
namespace jx\core;
class Boot{
	public static function run(){
		//初始化框架
		self::init();
		//执行应用
		self::appRun();
	}

	//执行应用
	private static function appRun(){
		if(isset($_GET['s'])){
			$info = explode('/', $_GET['s']);
			//某些情况下可能get参数会被不小心写成大写，所以统一strtolower转为小写
			//模块
			$m = strtolower($info[0]);
			//控制器
			$c = strtolower($info[1]);
			//方法
			$a = strtolower($info[2]);

		}else{//默认访问首页get参数s就不存在
			$m = 'home';
			$c = 'entry';
			$a = 'index';
		}

		//定义三个常量是为了在视图类View中组合模板视图的路径
		define('MODULE',$m);
		define('CONTROLLER',$c);
		define('ACTION',$a);

		//控制器名
		$controller = ucfirst($c);
		//组合类名
		$class = "\app\\{$m}\controller\\{$controller}Controller";
		$obj = new $class;
		echo $obj->$a();
		
		
	}

	//初始化框架
	private static function init(){
		//开启session
		session_id() || session_start();
		//设置默认时区
		date_default_timezone_set('PRC');
		//设置是否是POST提交的常量
		define('IS_POST',$_SERVER['REQUEST_METHOD'] == 'POST' ? true : false);
		//定义完整url地址常量
		// p($_SERVER);
		define('__ROOT__','http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']);
	}

}


 ?>