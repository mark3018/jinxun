<?php 
namespace jx\core;
class Boot{
	public static function run(){
		//初始化框架
		self::init();
		//执行应用
		self::appRun();
	}

	private static function appRun(){
		$obj = new \app\home\controller\HomeController;
		$obj->index();
	}

	//初始化框架
	private static function init(){
		//开启session
		session_id() || session_start();
		//设置默认时区
		date_default_timezone_set('PRC');
		//设置是否是POST提交的常量
		define('IS_POST',$_SERVER['REQUEST_METHOD'] == 'POST' ? true : false);
	}

}


 ?>