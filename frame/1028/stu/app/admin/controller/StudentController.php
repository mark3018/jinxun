<?php 
namespace app\admin\controller;
use jx\view\View;
use jx\core\Controller;
use jx\model\Model;

class StudentController extends Controller{
	//学生列表
	public function lists(){
		return View::make();
	}


	//添加学生
	public function store(){
		if(IS_POST){
			//1.用户不上传也不选择那么就是默认头像
			if($_FILES['profile']['error'] == 4){
				$profilePath = './attachment/default.jpg';
			}else{
				//3.用户可以自由上传头像
				$profilePath = $this->upload();
			}
			//2.用户可以选择头像，不上传
			
			$sql = "INSERT INTO student SET sname='{$_POST['sname']}',profile='{$profilePath}',sex='{$_POST['sex']}',birthday='{$_POST['birthday']}',introduce='{$_POST['introduce']}',gid={$_POST['gid']}";
			Model::e($sql);
			return $this->setRedirect(url('lists'))->message('添加成功');
		}


		//把所有的班级调出来
		$grades = Model::q("SELECT * FROM grade");
		//把附件表的所有数据调出来，为了用户可以选择之前上传过的图片附件
		$attachments = Model::q("SELECT * FROM attachment");

		return View::make()->with(compact('grades','attachments'));
	}


	//上传
	private function upload(){
		//自己组合附件目录
		$dir = './attachment/profile/' . date('ymd');
		is_dir($dir) || mkdir($dir,0777,true);
		$storage = new \Upload\Storage\FileSystem($dir);
		//profile是file表单name的值
		$file = new \Upload\File('profile', $storage);

		// Optionally you can rename the file on upload
		$new_filename = uniqid();
		$file->setName($new_filename);

		// Validate file upload
		// MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
		$file->addValidations(array(
		    // Ensure file is of type "image/png"
		    new \Upload\Validation\Mimetype(array('image/png', 'image/gif','image/jpeg')),

		    //You can also add multi mimetype validation
		    //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

		    // Ensure file is no larger than 5M (use "B", "K", M", or "G")
		    new \Upload\Validation\Size('2M')
		));

		// Access data about the file that has been uploaded
		$data = array(
		    'name'       => $file->getNameWithExtension(),
		    'extension'  => $file->getExtension(),
		    'mime'       => $file->getMimetype(),
		    'size'       => $file->getSize(),
		    'md5'        => $file->getMd5(),
		    'dimensions' => $file->getDimensions()
		);

		// Try to upload file
		try {
		    // Success!
		    $file->upload();
		    //组合带有文件名的完整路径，将来存入到数据库的
		    $fullPath = $dir . '/' . $data['name'];
		    //把文件路径保存到附件表
		    Model::e("INSERT INTO attachment SET path='$fullPath',createtime=" . time());
		    return $fullPath;

		} catch (\Exception $e) {
		    // Fail!
		    $errors = $file->getErrors();
		}
	}
}


 ?>