<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include "../app/admin/view/public/header.php" ?>
<div class="container">
	<div class="row">
		 <?php include "../app/admin/view/public/left.php" ?>
		 <div class="col-md-9">
		 
		 	<div class="panel panel-default" style="margin-top: 5px;">
			  <div class="panel-heading">
			    <h3 class="panel-title">添加学生</h3>
			  </div>
			  <div class="panel-body">
			    	<form action="" enctype="multipart/form-data" method="post">
					  <div class="form-group">
					    <label for="exampleInputEmail1">名字</label>
					    <input type="text" class="form-control" id="exampleInputEmail1" name="sname">
					  </div>
					   <div class="form-group">
					    <label for="exampleInputEmail1">所属班级</label>
					   <select class="form-control" name="gid">
					   		<option>请选择班级</option>
					   		<?php foreach($grades as $grade): ?>
						  		<option value="<?php echo $grade['gid'] ?>"><?php echo $grade['gname'] ?></option>
						  	<?php endforeach ?>
						</select>
					  </div>

					   <div class="form-group">
					    <label for="exampleInputEmail1">头像</label>
					    <input type="file" class="form-control" id="exampleInputEmail1" name="profile">
					    <br>
				
					    <?php foreach($attachments as $attachment): ?>
					    <img class="img-thumbnail attachment" width="100" src="<?php echo $attachment['path'] ?>">
					    <?php endforeach ?>
					    <script type="text/javascript">
					    	$(function(){
					    		$('.attachment').click(function(){
					    			$(this).css('border','1px solid red').siblings().css('border','1px solid #ddd');
					    		})
					    	})
					    </script>
					  </div>
					    <div class="form-group">
					    <label for="exampleInputEmail1">性别</label>
					    <br>
					   <label class="radio-inline">
						  <input type="radio" name="sex" id="inlineRadio1" value="男" checked> 男
						</label>
						<label class="radio-inline">
						  <input type="radio" name="sex" id="inlineRadio2" value="女"> 女
						</label>
					
					  </div>
					  	<div class="form-group">
						    <label for="exampleInputEmail1">生日</label>
						    <input type="date" class="form-control" id="exampleInputEmail1" name="birthday">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">自我介绍</label>
						   <textarea class="form-control" rows="5" name="introduce"></textarea>
						  </div>
					 	<input type="submit" class="btn btn-primary" value="添加学生">
					  	<a href="<?php echo url('lists'); ?>" class="btn btn-success">返回列表</a>
					</form>
			 
			</div>
		 </div>
	</div>
</div>
</body>
</html>