<?php 
namespace app\home\controller;
use jx\view\View;
use jx\core\Controller;
use system\model\Student;
class EntryController extends Controller{
	//学生列表
	public function index(){
		//获得学生和班级的关联数据
		$students = Student::getStudentGrade();
		return View::make()->with(compact('students'));
	}


	//显示具体的学生信息
	public function show(){
		$sid = isset($_GET['sid']) ? (int)$_GET['sid'] : 1;
		$student = Student::find($sid);
		return View::make()->with(compact('student'));
	}
}


 ?>