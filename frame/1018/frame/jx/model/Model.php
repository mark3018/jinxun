<?php 
namespace jx\model;
class Model{
	private static $config;


	public static function __callStatic($name,$args){
		//获得当前表名
		$table = get_called_class();
		$table = strtolower(ltrim(strrchr($table, '\\'),'\\'));
		return call_user_func_array([new Base(self::$config,$table),$name], $args);
	}


	//加载配置项
	public static function setConfig($config){
		self::$config = $config;
	}
}

 ?>