<?php 
namespace app\home\controller;
use jx\core\Controller;
use system\model\Article;
use system\model\Category;
use jx\view\View;

class ArcController extends Controller{
	
	public function show(){
		// $data = Article::get();
		$data = Article::where("aid>3")->get();
		return View::with(compact('data'))->make();
	}


	//阅读某一篇文章
	public function read(){
		$aid = isset($_GET['aid']) ? (int)$_GET['aid'] : 1;
		// $data = Article::where("aid={$aid}")->pluck();
		// $data = Article::findOne($aid);
		$data = Article::find($aid);
		p($data);
	}


	public function del(){
		$aid = isset($_GET['aid']) ? (int)$_GET['aid'] : 1;
		$afRows = Article::where("aid={$aid}")->destory();

		$this->message('删除了' . $afRows . '条！');
	}
}


 ?>