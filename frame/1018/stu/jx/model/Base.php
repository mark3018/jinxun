<?php 
namespace jx\model;
use PDO;
class Base{
	private $table;
	private static $pdo = NULL;
	//某些时候没有where条件
	private $where = '';


	public function __construct($config,$table){
		$this->table = $table;
		$this->connect($config);
	}


	private function connect($config){
		//如果不为空，那么证明已经实例化过PDO，连接过Mysql了
		if(!is_null(self::$pdo)) return;
		try{
			$dsn ="mysql:host={$config['db_host']};dbname={$config['db_name']}";
			$user = $config['db_user'];
			$password = $config['db_password'];
			$pdo = new PDO($dsn,$user,$password);
			//设置错误类型
			$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			//设置编码
			$pdo->query("SET NAMES " . $config['db_charset']);
			//把pdo对象交给静态属性
			self::$pdo = $pdo;
		}catch(PDOException $e){
			exit($e->getMessage());
		}


	}




	public function where($where){
		$this->where = " WHERE {$where}";
		return $this;
	}

	//获得全部数据
	public function get(){
		$sql = "SELECT * FROM {$this->table} {$this->where}";
		// echo $sql;exit;
		return $this->q($sql);
	}


	//获取一维数组的操作
	public function pluck(){
		$data = $this->get();
		return current($data);
	}


	public function find($pri){
		//获得表的主键字段名称
		$priField = $this->getPri();
		$this->where("{$priField}={$pri}");
		$sql = "SELECT * FROM {$this->table} {$this->where}";
		$data = $this->q($sql);
		return current($data);
	}


	public function findOne($pri){
		return $this->find($pri);
	}


	//获得一张表的主键
	private function getPri(){
		$data = $this->q("DESC {$this->table}");
		$priField = '';
		foreach ($data as $v) {
			if($v['Key'] == 'PRI'){
				$priField = $v['Field'];
				break;
			}
		}
		return $priField;
	}


	public function destory(){
		if(!empty($this->where)){
			$sql = "DELETE FROM {$this->table} {$this->where}";
			return $this->e($sql);
		}
		
	}

	//执行有结果集的操作
	public function q($sql){
		try{
			$result = self::$pdo->query($sql);
			$data = $result->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}catch(PDOException $e){
			exit($e->getMessage());
		}
	}

	//执行无结果集操作
	public function e($sql){
		try{
			$affectedRows = self::$pdo->exec($sql);
			//如果有自增id
			if($lastInertId = self::$pdo->lastInsertId()){
				//返回自增id
				return $lastInertId;
			}
			//否则返回受影响的条数
			return $affectedRows;

		}catch(PDOException $e){
			exit($e->getMessage());
		}
		
	}
}


 ?>