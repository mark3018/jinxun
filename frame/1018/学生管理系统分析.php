
学生管理系统

功能需求：

前台
1.显示学生列表
2.点击某个具体的学生可以看到具体的详细信息


后台
1.登录功能（7天免登录）
2.验证码功能
3.班级管理
4.学生管理
5.素材管理
6.后台用户密码修改



需要什么表：

学生表 student
sid 主键
sname 学生姓名
profile 头像
sex 性别
birthday 生日
introduce 介绍
gid 关联字段，知道学生属于哪个班级

create table student(
	sid smallint primary key auto_increment,
	sname varchar(10) not null default '',
	profile varchar(255) not null default '',
	sex enum('男','女') not null default '男',
	birthday date,
	introduce varchar(255) not null default '',
	gid smallint unsigned not null default 0
);





班级表 grade
gid 主键
gname 班级名称

create table grade(
	gid smallint primary key auto_increment,
	gname varchar(10) not null default ''
);



用户表 user
uid
username 用户名
password 密码

create table user(
	uid smallint primary key auto_increment,
	username char(20) not null default '',
	password char(32) not null default ''
);


附件表 attachment
aid
path 路径
createtime 创建时间 

create table attachment(
	aid int primary key auto_increment,
	path varchar(255) not null default '' comment '附件地址',
	createtime int unsigned not null default 0
);








